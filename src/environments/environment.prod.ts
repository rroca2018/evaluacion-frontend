export const environment = {
  production: true,

  // URLS BACKENDS SERVIDOR 
  backendUrl: 'http://192.168.137.1:8089/balotario',
  backendUrlProj: 'http://192.168.137.1:8089/balotario',

  basicAuthorization: 'angularapp:12345',

  // CAPTCHA
  cantCharacters: 6
};
