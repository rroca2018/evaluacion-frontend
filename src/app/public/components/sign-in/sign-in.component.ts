import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { Session } from '@shared/auth/Session';
import { Constante } from 'app/protected/modules/applications/modules/balotario/dto/Constante';

@Component({
  selector: 'public-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  authForm: FormGroup; 

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.authForm = this.formBuilder.group({
      username: [],// Validators.required], 87654321
      password: []// Validators.required]
    });
  }

  onSubmit() {
    debugger;
    //if (this.authForm.valid) {
      if(this.authForm.value.username !=null && this.authForm.value.password!=null){
      this.authService.login(
        this.authForm.value
      ).subscribe(
        (session) => {
         // debugger;
          Session.start(session);
          console.log(Session.identity);
          if (Session.identity.puesto == Constante.ADMINISTRADOR) {
            this.router.navigate(['/principal/administrador']);
          } else {
            //inicio del cuestionario

            localStorage.removeItem('jStorage');
            localStorage.removeItem('result');
            localStorage.removeItem('jStorage_update');
            localStorage.removeItem('resultCalificacion');
            localStorage.removeItem('resultPorcentaje');
            localStorage.removeItem('calification');
            localStorage.removeItem('reloaded');
            localStorage.removeItem('totalAttempt');
            localStorage.removeItem('numberQuestion');
            localStorage.removeItem('iteration');
            localStorage.removeItem('init');
            localStorage.removeItem('chr');
            localStorage.removeItem('question');
            localStorage.removeItem('#nI');
            localStorage.removeItem('cod_bal');
            

            this.router.navigate(['/principal/persona']);
          }
        },
        () => {
          debugger;
          console.log("errores en el login : " + this.authForm);
          this.authForm.get('username').setErrors({ resp: 'Usuario o contraseña incorrecto.' });
         
        }
      );
    } else {
      this.authForm.get('username').setErrors({ resp: 'Usuario o contraseña incorrecto.' });
      Object.values(this.authForm.controls).forEach(c => c.markAsTouched());
    }
  }

}
