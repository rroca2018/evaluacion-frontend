import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { InfoMessageComponent } from '@shared/components/info-message/info-message.component';
import { MENSAJES } from 'app/common';
import { filter } from 'rxjs/operators';
import { AuthService } from 'app/protected/services/auth.service';
import { UsuarioRequest } from 'app/protected/modules/applications/modules/balotario/dto/request/UsuarioRequest';
import { Session } from '@shared/auth/Session';
import { Router } from '@angular/router';


@Component({
  selector: 'app-cambiar-contrasena',
  templateUrl: './cambiar-contrasena.component.html',
  styleUrls: ['./cambiar-contrasena.component.scss']
})
export class CambiarContrasenaComponent implements OnInit {

  dialogRefMessage: MatDialogRef<any>;
  usuarioForm: FormGroup; 
  hideAnterior = true;
  hideNueva = true;
  hideConfirmar = true;
  mensaje: string;



  constructor( private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CambiarContrasenaComponent>,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private router: Router,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.formCambiarContrasenia();
    console.log("cambiar contraseña");
    
  }




   public openDialogMensaje(message: string, alerta: boolean, confirmacion: boolean, valor: any): void {
      const dialogRef = this.dialog.open(InfoMessageComponent, {
        disableClose: true,
        data: {
          message: message,
          alerta: alerta,
          confirmacion: confirmacion,
          valor: valor
        }
      });
      dialogRef.afterClosed().subscribe((ok: number) => {
        if (ok == 0) {
  
        }
      });
    }

    public openDialogMensajeConfirm(message: string, confirmacion: boolean): void {

      this.dialogRefMessage = this.dialog.open(InfoMessageComponent, {
        //width: '400px',
        disableClose: true,
        data: { message: message, confirmacion: confirmacion }
      });
    }


  formCambiarContrasenia() {
    this.usuarioForm = this.formBuilder.group({
      contraseniaAnteriorFrm: [''],
      nuevaContraseniaFrm: [''],
      confirmarContraseniaFrm: ['']
    
    });


  }


  async actualizarContrasenia(){
    if (this.validarDatosObligatorios() == false) {
      this.openDialogMensaje(MENSAJES.USUARIO.CAMPOS_OBLIGATORIOS, true, false, null);
    }else if(this.validarLongitudNuevaContrasenia() == false){
      this.openDialogMensaje(MENSAJES.USUARIO.CLAVE_MINIMO_OCHO_CARACTERES, true, false, null);
    }else if(this.validarIgualdadNuevaContrasenia() == false){
      this.openDialogMensaje(MENSAJES.USUARIO.NUEVA_CONTRASENIA_NO_COINCIDE, true, false, null);
    }else{


   let validoClaveVigente : boolean = true;
      await this.authService.verificarClaveVigente(this.usuarioForm.value.contraseniaAnteriorFrm).then(res =>{
        if(res.codResultado == 0){
         this.openDialogMensaje(MENSAJES.USUARIO.CLAVE_NO_VIGENTE, true, false, null);
         validoClaveVigente = false;
        }
      })
      if(validoClaveVigente==true){
        this.openDialogMensajeConfirm(MENSAJES.USUARIO.MODIFICAR_CONTRASENIA, true);

        this.dialogRefMessage.afterClosed()
          .pipe(filter(verdadero => !!verdadero))
          .subscribe(async() => {   
            let errorActualziar: any;
            let user = new UsuarioRequest();
            user.idCodigo = Session.identity.id_usuario;
            user.clave = this.usuarioForm.value.nuevaContraseniaFrm;
            let actualiza : boolean = true;
           
            await this.authService.actualizarContrasenia(user).then((wsResponse : any)=> {
              debugger;
              if(wsResponse.error == "-1"){
                actualiza = false;    
                this.openDialogMensaje(wsResponse.mensaje, true, false, null);
              }
              
             },  error => {
            
              console.log(error);
              actualiza = false;    
              this.mensaje = MENSAJES.ERROR_SERVICIO;
               console.error(this.mensaje);  

               //this.openDialogMensaje(wsResponse., true, false, null);
             }      
            );
          if(actualiza){
            this.dialogRef.close(true);
            this.snackBar.open("Contraseña se modificó correctamente!");
            Session.stop();
            this.router.navigate(['/anonimo/iniciar-sesion']);
          }
          
            
        });


      }  

    }

  
     
  }



  validarDatosObligatorios(){
    let valido: boolean = true;

    if(this.usuarioForm.value.contraseniaAnteriorFrm.trim()    == "" || 
        this.usuarioForm.value.nuevaContraseniaFrm.trim()      == "" || 
        this.usuarioForm.value.confirmarContraseniaFrm.trim()  == ""){
          valido = false;
    } 
    return valido;
   }

   validarLongitudNuevaContrasenia(){
    let valido: boolean = true;
   
    if(this.usuarioForm.value.nuevaContraseniaFrm.trim()  != " "  &&  this.usuarioForm.value.nuevaContraseniaFrm.trim().length  < 8){
      valido = false;
    }else if(this.usuarioForm.value.confirmarContraseniaFrm.trim()  != " "  &&  this.usuarioForm.value.confirmarContraseniaFrm.trim().length  < 8){
          valido = false;
    }
    return valido;
   }

   validarIgualdadNuevaContrasenia(){

    let valido: boolean = true;
   
    if(this.usuarioForm.value.nuevaContraseniaFrm.trim()  != " "  &&  this.usuarioForm.value.confirmarContraseniaFrm.trim() != " "){
      if(this.usuarioForm.value.nuevaContraseniaFrm.trim() !=  this.usuarioForm.value.confirmarContraseniaFrm.trim()){
        valido = false;
      }
     }
    return valido;
   }
   
}
