import { Component, OnInit, ViewChild, HostListener, OnDestroy } from '@angular/core';
import { MatDrawer } from '@angular/material';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { Session } from '@shared/auth/Session';
import { of } from 'rxjs';

import { delay } from 'rxjs/operators';
import { Constante } from './modules/applications/modules/balotario/dto/Constante';

@Component({
  selector: 'app-protected',
  templateUrl: './protected.component.html',
  styleUrls: ['./protected.component.scss']
})
export class ProtectedComponent implements OnInit, OnDestroy {

  loading: boolean;
  subscriptorRouter: Subscription;
  menu_ocultar: boolean;
  menu_mostrar_estilo: boolean;
  menu_ocultar_estilo: boolean;

  @ViewChild('drawer') drawer: MatDrawer;

  constructor(
    private router: Router
  ) {

    if (Session.identity.puesto == Constante.ADMINISTRADOR) {
      this.menu_ocultar = false;
      this.menu_mostrar_estilo = true;
      this.menu_ocultar_estilo = false;
    } else {
      this.menu_ocultar = true;
      this.menu_mostrar_estilo = false;
      this.menu_ocultar_estilo = true;
    }
  }

  ngOnInit() {
    this.onResize(window.innerWidth);
    this.subscriptorRouter = this.router.events.subscribe(
      (event) => {
        if (event instanceof NavigationStart) {
          this.loading = true;
        }
        if (event instanceof NavigationEnd) {
          this.loading = false;
        }
      });
  }

  ngOnDestroy() {
    this.subscriptorRouter.unsubscribe();
  }

  @HostListener('window:resize', ['$event.target.innerWidth'])
  onResize(innerWidth: number) {
    of(true).pipe(delay(2000));
    if (typeof this.drawer !== 'undefined') {
      if (innerWidth <= 1280 && this.drawer.opened) {
        this.drawer.close();
      } else if (innerWidth >= 1280 && !this.drawer.opened) {
        this.drawer.open();
      }
    }
  }



}
