import { Component, OnInit, AfterViewChecked, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Session } from '@shared/auth/Session';
import { Router } from '@angular/router';
import { AuthService } from 'app/protected/services/auth.service';
import { Cabecera } from 'app/protected/entities/cabecera';
import { Subscription } from 'rxjs';
import { CambiarContrasenaComponent } from 'app/public/components/cambiar-contrasena/cambiar-contrasena.component';
import { MatDialogRef, MatDialog } from '@angular/material';

@Component({
  selector: 'protected-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, AfterViewChecked, OnDestroy {

  mostrarImagenHombre: boolean;
  mostrarImagenMujer: boolean;

  cabecera: Cabecera = {
    titulo: '',
    icono: ''
  };

  esperarCabecera: Subscription;

  constructor(
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private authService: AuthService,
    private dialog: MatDialog
  ) { 
    
    if(this.user.genero == "M"){
      console.log("MASCULINO");
      this.mostrarImagenHombre = false;
      this.mostrarImagenMujer = true;
    }else{
      
      console.log("FEMENINO");
      this.mostrarImagenHombre = true;
      this.mostrarImagenMujer = false;
    }
  }

  ngOnInit() {
    this.esperarCabecera = this.authService.cabecera.subscribe(cabecera => this.cabecera = cabecera);
  }

  ngOnDestroy() {
    this.esperarCabecera.unsubscribe();
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  logout(): void {
    // TODO: POR REFACTORIZAR
    /*
      envio el idUsuario que se va desconectar al websocket para que actualice
      su estado como offline para los demas usuarios conectados.
    */
    //this.chatService.cerrarSesionSocket(Session.identity.id_usuario);

    Session.stop();
    this.router.navigate(['/anonimo/iniciar-sesion']);
  }


   
  cambiarContrasena(): void{
    const dialogReg: MatDialogRef<CambiarContrasenaComponent> = this.dialog.open(CambiarContrasenaComponent, {
      panelClass: 'dialog-no-padding',
      width: '30%',
      //height: '60%',
      data: { 
      }
    });

    dialogReg.afterClosed().subscribe((valor: any) => {     
      if(valor) {
       
      } 
    });

  }


  get user() {
    return Session.identity;
  }

}
