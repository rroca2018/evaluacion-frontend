import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReporteComponent } from './components/reporte/reporte.component';
import { AuthRouting } from '@shared/auth/auth-routing';
import { ResultadoComponent } from './components/resultado/resultado.component';
import { UsuarioComponent } from './components/administrador/usuario/usuario.component';
import { ConfiguracionBalotariosComponent } from './components/administrador/configuracion-balotarios/configuracion-balotarios.component';
import { VisualizacionResultadosComponent } from './components/administrador/visualizacion-resultados/visualizacion-resultados.component';
import { PreguntasRespuestasComponent } from './components/administrador/preguntas-respuestas/preguntas-respuestas.component';
import { ConfiguracionGeneralComponent } from './components/administrador/configuracion-general/configuracion-general.component';

const routes: Routes = [
  { path: 'reporte', component: ReporteComponent, canActivate: [AuthRouting] },
  { path: 'resultado', component: ResultadoComponent, canActivate: [AuthRouting] },
  { path: 'bandeja-usuario', component: UsuarioComponent, canActivate: [AuthRouting]},
  { path: 'configuracion-balotarios', component: ConfiguracionBalotariosComponent, canActivate: [AuthRouting]},
  { path: 'visualizacion-resultados', component: VisualizacionResultadosComponent, canActivate: [AuthRouting]},
  { path: 'preguntas-respuestas', component: PreguntasRespuestasComponent, canActivate: [AuthRouting]},
  { path: 'datos-maestros', component: ConfiguracionGeneralComponent, canActivate: [AuthRouting]}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BalotarioRoutingModule { }
