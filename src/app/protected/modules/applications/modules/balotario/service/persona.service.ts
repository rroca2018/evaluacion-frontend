

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { PersonaRequest } from '../dto/request/PersonaRequest';
import { WsResponse } from '../dto/WsResponse';
import { VerificarRequest } from '../dto/request/verificarRequest';

@Injectable()
export class PersonaService {
  


  constructor(
    private http: HttpClient
  ) { }
/*
  crearPersona(persona: PersonaRequest) {
    return this.http.post<PersonaRequest>(`${environment.backendUrlProj}/persona/registrarPersona`, persona, { reportProgress: true });
  }
*/
  crearPersona(persona: PersonaRequest): Promise<PersonaRequest> {
    return this.http.post<PersonaRequest>(`${environment.backendUrlProj}/persona/registrarPersona`, persona, { reportProgress: true }).toPromise();
  }


  listarPersonas(): Promise<any> {
    return this.http.get<any>(`${environment.backendUrlProj}/persona/listarPersonas`,{ reportProgress: true }).toPromise(); 
  }

  obtenerPersona(idUsuario: string) {
    return this.http.get<any>(`${environment.backendUrlProj}/persona/obtenerPersona/${idUsuario}`,{ reportProgress: true }); 
  }
  

  actualizarPersona(persona: PersonaRequest) : Promise<PersonaRequest>{
    return this.http.post<PersonaRequest>(`${environment.backendUrlProj}/persona/actualizarPersona`, persona, { reportProgress: true }).toPromise();
  }


  verificarUsuario(usuario: String): Promise<WsResponse>{
    return this.http.post<WsResponse>(`${environment.backendUrlProj}/persona/verificarUsuario`, usuario, { reportProgress: true }).toPromise();
  }


  verificarCodTrabajador(verificarRequest: VerificarRequest): Promise<WsResponse>{
    return this.http.post<WsResponse>(`${environment.backendUrlProj}/persona/verificarCodigoTrabajador`, verificarRequest, { reportProgress: true }).toPromise();
  }

  verificarNumDocumento(verificarRequest: VerificarRequest): Promise<WsResponse> {
    return this.http.post<WsResponse>(`${environment.backendUrlProj}/persona/verificarNumeroDocumento`, verificarRequest, { reportProgress: true }).toPromise();
  }

  buscarPersonas(textoBuscar: String): any {
    return this.http.post<any>(`${environment.backendUrlProj}/persona/buscarPersonas`, textoBuscar); 
  }

  downloadFile() { 
    const REQUEST_URI = `${environment.backendUrlProj}/archivo/descargarArchivo`;
    return this.http.get(REQUEST_URI, {
      responseType: 'arraybuffer'
    })
  }

/*
  roles(opcionUrl: string): Promise<Rol[]> {
    return this.http.get<Rol[]>(`${this.urlBase}/roles?rutaOpcion=${opcionUrl}`).toPromise();
  }*/

}