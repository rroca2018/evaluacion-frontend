import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'environments/environment';
import { HttpClient, HttpErrorResponse, HttpEventType, HttpEvent } from '@angular/common/http';
import { ArchivoRequest } from '../dto/request/ArchivoRequest';
import { map, catchError } from 'rxjs/operators';
import * as fileSaver from 'file-saver';

@Injectable()
export class ExcelService {
  


  constructor(
    private http: HttpClient
  ) { }


  cargarExcelPartidaTxtError(nombreListado: string): void {
    const formData = new FormData();
    formData.append('nombreListado',nombreListado);
    this.http
      .post(`${environment.backendUrlProj}/excelFile/verTxtErrores`, formData, {responseType: 'blob'})
      .subscribe((b) => {
        fileSaver.saveAs(b, 'error_excel.txt');
      });
  }
    

  cargarExcel(request: ArchivoRequest): Observable<any> {

    const formData = new FormData();
    formData.append('file', request.archivo);

  /*formData.append('nomArchivo', request.nomArchivo);
    formData.append('idTipoArchivo', request.tipoDoc); */

    return this.http.post<any>(`${environment.backendUrlProj}/archivo/validarCargaExcelPersonas`, formData,{
      reportProgress: true,
      observe: 'events'
    }).pipe(
        map(event => this.getEventMessage(event, formData)),
        catchError(this.handleError)
      );
    }


    private getEventMessage(event: HttpEvent<any>, formData) {

        switch (event.type) {
          case HttpEventType.UploadProgress:
            return this.fileUploadProgress(event);
    
          case HttpEventType.Response:
            return this.apiResponse(event);
    
          default:
            return `evento_${event.type}`;
        }
      }


      private fileUploadProgress(event) {
        const percentDone = Math.round(100 * event.loaded / event.total);
        return { status: 'progress', message: percentDone/2};
      }
    
      private apiResponse(event) {
        return event.body;
      }
    
      private handleError(error: HttpErrorResponse) {
        
        if (error.error instanceof ErrorEvent) {
          // A client-side or network error occurred. Handle it accordingly.
          console.error('An error occurred:', error.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError('Algo malo sucedio. Por favor, inténtelo de nuevo más tarde.');
      }
    



}