

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { WsItemBeanResponse } from '../dto/ItemBean';


@Injectable()
export class ItemService {

  constructor(
    private http: HttpClient
  ) { }


  listadoTipoDocumento(): Observable<WsItemBeanResponse> {
    return this.http.get<WsItemBeanResponse>(`${environment.backendUrlProj}/persona/listarTipoDocumento`); 
  }

  listadoCategoria(): Observable<WsItemBeanResponse> {
    return this.http.get<WsItemBeanResponse>(`${environment.backendUrlProj}/persona/listarCategoria`); 
  }

  listadoCargo(): Observable<WsItemBeanResponse> {
    return this.http.get<WsItemBeanResponse>(`${environment.backendUrlProj}/persona/listarCargo`); 
  }

  listadoRol(): Observable<WsItemBeanResponse> {
    return this.http.get<WsItemBeanResponse>(`${environment.backendUrlProj}/persona/listarRol`); 
  }

  listadoEstado(): Observable<WsItemBeanResponse> {
    return this.http.get<WsItemBeanResponse>(`${environment.backendUrlProj}/persona/listarEstado`); 
  }

  listadoArea(idCategoria: number): Observable<WsItemBeanResponse> {
    return this.http.get<WsItemBeanResponse>(`${environment.backendUrlProj}/persona/listarArea/${idCategoria}`); 
  }

  listadoLocal(idCategoria: number, idArea: number): Observable<WsItemBeanResponse> {
    return this.http.get<WsItemBeanResponse>(`${environment.backendUrlProj}/persona/listarLocal/${idCategoria}/${idArea}`); 
  }

  listarYears(): Observable<WsItemBeanResponse> {
    return this.http.get<WsItemBeanResponse>(`${environment.backendUrlProj}/balotario/listarYears`); 
  }
  
  comboBalotarioYear(balotarioYear: number): Observable<WsItemBeanResponse> {
    return this.http.get<WsItemBeanResponse>(`${environment.backendUrlProj}/balotario/listarComboBalotario/${balotarioYear}`); 
  }

  listadoTema(): Observable<WsItemBeanResponse> {
    return this.http.get<WsItemBeanResponse>(`${environment.backendUrlProj}/preguntasRespuestas/listarTema`); 
  }
}