import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { WsResponse } from '../dto/WsResponse';
import { BehaviorSubject } from 'rxjs';
import {map, tap, catchError} from 'rxjs/operators';
import { environment } from 'environments/environment';
import { Resultado } from '../dto/Resultado';
import { WsResponsePersonaItem, PersonaItemResponse } from '../dto/response/PersonaItemResponse';
import { VerificarBalotarioRequest } from '../dto/request/VerificarBalotarioRequest';
import { BalotarioRequest } from '../dto/request/balotarioRequest';
import { ValidarExamenRealizadoRequest } from '../dto/request/ValidarExamenRealizadoRequest';
import { FiltroRequest } from '../dto/request/FiltroRequest';
import { PreguntaRequest } from '../dto/request/PreguntaRequest';
import { ArchivoRequest } from '../dto/request/ArchivoRequest';

@Injectable()
export class PreguntasRespuestasService {

  HOST: string = environment.backendUrlProj;

  constructor(
    private http: HttpClient
  ) { }


  listarPreguntasRespuestas(): Promise<any> {
    return this.http.get<any>(`${this.HOST}/preguntasRespuestas/listarPreguntasRespuestas`,{ reportProgress: true}).toPromise(); 
  }

  registrarPregunta(pregunta: PreguntaRequest) {
    return this.http.post<PreguntaRequest>(`${environment.backendUrlProj}/preguntasRespuestas/registrarPreguntaRespuesta`, pregunta, { reportProgress: true }).toPromise();
  }

  obtenerPregunta(codBalotario: number){
    return this.http.get<any>(`${this.HOST}/preguntasRespuestas/obtenerPregunta/${codBalotario}`,{ reportProgress: true }); 
  }

  buscarPregunta(textoBuscar: String): any {
    return this.http.post<any>(`${environment.backendUrlProj}/preguntasRespuestas/buscarPregunta`, textoBuscar); 
  }

  obtenerRespuestas(codPregunta: number): Promise<any>{
    return this.http.get<any>(`${this.HOST}/preguntasRespuestas/obtenerRespuestas/${codPregunta}`,{ reportProgress: true }).toPromise();
  }

  obtenerQtRespuestasGlobal(): Promise<any> {
    return this.http.get<any>(`${this.HOST}/preguntasRespuestas/obtenerQtRespuestasGlobal`,{ reportProgress: true}).toPromise(); 
  }

  downloadFile() { 
    const REQUEST_URI = `${environment.backendUrlProj}/archivo/descargarArchivoPregunta`;
    return this.http.get(REQUEST_URI, {
      responseType: 'arraybuffer'
    })
  }

  cargarExcel(request: ArchivoRequest): Observable<any> {

    const formData = new FormData();
    formData.append('file', request.archivo);

  /*formData.append('nomArchivo', request.nomArchivo);
    formData.append('idTipoArchivo', request.tipoDoc); */

    return this.http.post<any>(`${environment.backendUrlProj}/archivo/validarCargaExcelPreguntas`, formData,{
      reportProgress: true,
      observe: 'events'
    }).pipe(
        map(event => this.getEventMessage(event, formData)),
        catchError(this.handleError)
      );
    }


    private getEventMessage(event: HttpEvent<any>, formData) {

        switch (event.type) {
          case HttpEventType.UploadProgress:
            return this.fileUploadProgress(event);
    
          case HttpEventType.Response:
            return this.apiResponse(event);
    
          default:
            return `evento_${event.type}`;
        }
      }


      private fileUploadProgress(event) {
        const percentDone = Math.round(100 * event.loaded / event.total);
        return { status: 'progress', message: percentDone/2};
      }
    
      private apiResponse(event) {
        return event.body;
      }
    
      private handleError(error: HttpErrorResponse) {
        
        if (error.error instanceof ErrorEvent) {
          // A client-side or network error occurred. Handle it accordingly.
          console.error('An error occurred:', error.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError('Algo malo sucedio. Por favor, inténtelo de nuevo más tarde.');
      }
    
}
