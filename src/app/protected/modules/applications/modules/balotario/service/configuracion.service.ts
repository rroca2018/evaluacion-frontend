

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { PersonaRequest } from '../dto/request/PersonaRequest';
import { WsResponse } from '../dto/WsResponse';
import { VerificarRequest } from '../dto/request/verificarRequest';
import { WsItemBeanResponse } from '../dto/ItemBean';
import { WsItemBeanGeneralResponse } from '../dto/ItemBeanGeneral';
import { Categoria } from '../dto/request/Categoria';
import { Area } from '../dto/request/Area';
import { Local } from '../dto/request/Local';
import { Cargo } from '../dto/request/Cargo';
import { Tema } from '../dto/request/Tema';

@Injectable()
export class ConfiguracionService {
  
  constructor(
    private http: HttpClient
  ) { }

  

  listadoCategoria(): Observable<WsItemBeanGeneralResponse> {
    return this.http.get<WsItemBeanGeneralResponse>(`${environment.backendUrlProj}/configuracion/listarCategoria`); 
  }

  listadoCargo(): Observable<WsItemBeanGeneralResponse> {
    return this.http.get<WsItemBeanGeneralResponse>(`${environment.backendUrlProj}/configuracion/listarCargo`); 
  }

  listadoArea(): Observable<WsItemBeanGeneralResponse> {
    return this.http.get<WsItemBeanGeneralResponse>(`${environment.backendUrlProj}/configuracion/listarArea`); 
  }

  listadoLocal(): Observable<WsItemBeanGeneralResponse> {
    return this.http.get<WsItemBeanGeneralResponse>(`${environment.backendUrlProj}/configuracion/listarLocal`); 
  }

  listadoTema(): Observable<WsItemBeanGeneralResponse> {
    return this.http.get<WsItemBeanGeneralResponse>(`${environment.backendUrlProj}/configuracion/listarTema`); 
  }

  grabarCategoria(categoria: Categoria) {
    return this.http.post<Categoria>(`${environment.backendUrlProj}/configuracion/grabarCategoria`, categoria, { reportProgress: true }).toPromise();
  }

  obtenerCategoria(codCategoria: number){
    return this.http.get<any>(`${environment.backendUrlProj}/configuracion/obtenerCategoria/${codCategoria}`,{ reportProgress: true });
  }

  grabarArea(area: Area) {
    return this.http.post<Categoria>(`${environment.backendUrlProj}/configuracion/grabarArea`, area, { reportProgress: true }).toPromise();
  }
  
  obtenerArea(codArea: number){
    return this.http.get<any>(`${environment.backendUrlProj}/configuracion/obtenerArea/${codArea}`,{ reportProgress: true });
  }

  grabarLocal(local: Local) {
    return this.http.post<Categoria>(`${environment.backendUrlProj}/configuracion/grabarLocal`, local, { reportProgress: true }).toPromise();
  }
  
  obtenerLocal(codLocal: number){
    return this.http.get<any>(`${environment.backendUrlProj}/configuracion/obtenerLocal/${codLocal}`,{ reportProgress: true });
  }

  grabarCargo(cargo: Cargo) {
    return this.http.post<Categoria>(`${environment.backendUrlProj}/configuracion/grabarCargo`, cargo, { reportProgress: true }).toPromise();
  }
  
  obtenerCargo(codCargo: number){
    return this.http.get<any>(`${environment.backendUrlProj}/configuracion/obtenerCargo/${codCargo}`,{ reportProgress: true });
  }

  grabarTema(tema: Tema) {
    return this.http.post<Categoria>(`${environment.backendUrlProj}/configuracion/grabarTema`, tema, { reportProgress: true }).toPromise();
  }
  
  obtenerTema(codTema: number){
    return this.http.get<any>(`${environment.backendUrlProj}/configuracion/obtenerTema/${codTema}`,{ reportProgress: true });
  }

}