import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { WsResponse } from '../dto/WsResponse';
import { BehaviorSubject } from 'rxjs';
import {map, tap} from 'rxjs/operators';
import { environment } from 'environments/environment';
import { Resultado } from '../dto/Resultado';
import { WsResponsePersonaItem, PersonaItemResponse } from '../dto/response/PersonaItemResponse';
import { VerificarBalotarioRequest } from '../dto/request/VerificarBalotarioRequest';
import { BalotarioRequest } from '../dto/request/balotarioRequest';
import { ValidarExamenRealizadoRequest } from '../dto/request/ValidarExamenRealizadoRequest';
import { FiltroRequest } from '../dto/request/FiltroRequest';

import  * as FileSaver from 'file-saver';
import  * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';



@Injectable()
export class ExporterService {

  constructor() { }

  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + EXCEL_EXTENSION);
  }

}
