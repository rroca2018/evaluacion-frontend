import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { WsResponse } from '../dto/WsResponse';
import { BehaviorSubject } from 'rxjs';
import {map, tap} from 'rxjs/operators';
import { environment } from 'environments/environment';
import { Resultado } from '../dto/Resultado';
import { WsResponsePersonaItem, PersonaItemResponse } from '../dto/response/PersonaItemResponse';
import { VerificarBalotarioRequest } from '../dto/request/VerificarBalotarioRequest';
import { BalotarioRequest } from '../dto/request/balotarioRequest';
import { ValidarExamenRealizadoRequest } from '../dto/request/ValidarExamenRealizadoRequest';
import { FiltroRequest } from '../dto/request/FiltroRequest';

@Injectable()
export class BalotarioService {

  HOST: string = environment.backendUrlProj;

  constructor(
    private http: HttpClient
  ) { }

  obtenerListadoPreguntaRespuesta(cantidadPregunta: number, idBalotario: number): Observable<WsResponse> {
    return this.http.get<any>(`${this.HOST}/cuestionario/listaPregunta/${cantidadPregunta}/${idBalotario}`,{ reportProgress: true });
  }

  validarBalotarioUsuario(): Observable<WsResponse> {
    return this.http.get<any>(`${this.HOST}/cuestionario/validarIntentosBalotario`,{ reportProgress: true });
  }

  obtenerBalotario(codBalotario: number){
    return this.http.get<any>(`${this.HOST}/balotario/getBalotarioCuestionario/${codBalotario}`,{ reportProgress: true });
  }

  registrarResultado(resultado: Resultado) {
    return this.http.post<Resultado>(`${environment.backendUrlProj}/resultado/registrarResultado`, resultado, { reportProgress: true });
  }

  listadoArea(pagina: number, cantidad: number): Observable<WsResponse> {
    return this.http.get<WsResponse>('assets/listadoArea.json');
  }

  listado(): Observable<WsResponse> {
    return this.http.get<WsResponse>('assets/listado.json');
  }

  listarBalotario(): Observable<any> {
    return this.http.get<any>(`${environment.backendUrlProj}/balotario/listarBalotarios`,{ reportProgress: true });
  }

  listarTema(): Promise<any> {
    return this.http.get<any>(`${environment.backendUrlProj}/balotario/listarTemas`,{ reportProgress: true }).toPromise()
  }

  obtenerBalotarioCompleto(codBalotario: number){
    return this.http.get<any>(`${this.HOST}/balotario/obtenerBalotario/${codBalotario}`,{ reportProgress: true }); 
  }

  obtenerComboDominio(): Observable<WsResponse> {
    return this.http.get<WsResponse>('assets/comboDominio.json');
  }

/*
  listaPersonaPorAsignar(): Observable<WsResponsePersonaItem> {
    return this.http.get<WsResponsePersonaItem>(`${environment.backendUrlProj}/balotario/listarPersonasPorAsignar`); 
  }
*/

  obtenerTemasPreguntas(codBalotario: number): Promise<any>{
    return this.http.get<any>(`${this.HOST}/tema/getTemaPreguntas/${codBalotario}`,{ reportProgress: true }).toPromise();
  }

  obtenerCantidadPreguntas(codBalotario: number): Promise<any>{
    return this.http.get<any>(`${this.HOST}/tema/cantidadPreguntas/${codBalotario}`,{ reportProgress: true }).toPromise();
  }

  listaPersonaPorAsignar(filter: {name: any} = {name: ''}, page = 1): Observable<WsResponsePersonaItem> {
  
    let obj = (filter.name);
    let buscarPorMayuscula = "";
    
    if(obj!= null && obj.idCodigo == null){
      buscarPorMayuscula = (filter.name).toUpperCase();
    }
    return this.http.get<WsResponsePersonaItem>(`${environment.backendUrlProj}/balotario/listarPersonasPorAsignar`)
    .pipe(
      tap((response: WsResponsePersonaItem) => {
       
        response.response = response.response
          .map(persona => new PersonaItemResponse(persona.idCodigo, persona.nombreCompleto, persona.identificacion))
          // Not filtering in the server since in-memory-web-api has somewhat restricted api
          .filter(persona => persona.nombreCompleto.includes(buscarPorMayuscula))

        return response;
      })
      );
  }

  verificarExistenciaBalotario(fechaInicio: String , fechaFin : string
    , tipo : string): Promise<WsResponse>{
    return this.http.post<WsResponse>
    (`${environment.backendUrlProj}/balotario/verificarExisteBalotario`, 
    {fechaInicio,fechaFin,tipo}, { reportProgress: true }).toPromise();
  }

  buscarBalotarios(textoBuscar: String): any {
    return this.http.post<any>(`${environment.backendUrlProj}/balotario/buscarBalotarios`, textoBuscar); 
  }

  obtenerQtGlobal(): Promise<any> {
    return this.http.get<any>(`${environment.backendUrlProj}/balotario/obtenerQtGlobal`,{ reportProgress: true }).toPromise()
  }

  obtenerPersonas(codBalotario: number): Promise<any>{
    return this.http.get<any>(`${this.HOST}/balotario/obtenerPersonas/${codBalotario}`,{ reportProgress: true }).toPromise();
  }

  
  registrarBalotario(balotario: BalotarioRequest) {
    return this.http.post<BalotarioRequest>(`${environment.backendUrlProj}/balotario/registrarBalotario`, balotario, { reportProgress: true }).toPromise();
  }

  validarExamenRealizado(validar: ValidarExamenRealizadoRequest):Promise<any> {
    return this.http.post<ValidarExamenRealizadoRequest>(`${environment.backendUrlProj}/balotario/validarExamenRealizado`, validar, { reportProgress: true }).toPromise();
  }

  verificarBalotarioRealizado(objBalotario: ValidarExamenRealizadoRequest):Promise<any> {
    return this.http.post<ValidarExamenRealizadoRequest>(`${environment.backendUrlProj}/balotario/verificarBalotarioRealizado`, objBalotario, { reportProgress: true }).toPromise();
  }

  obtenerNotas(codBalotario: number, codCategoria: number, codArea:number, codLocal:number):Promise<any> {
    return this.http.get<any>(`${this.HOST}/monitoreo/obtenerNotas/${codBalotario}/${codCategoria}
    /${codArea}
    /${codLocal}`,{ reportProgress: true }).toPromise();
  }

  indicadorDetalle(codBalotario: number, codCategoria: number, codArea:number, codLocal:number):Promise<any> {
    return this.http.get<any>(`${this.HOST}/monitoreo/indicadorDetalle/${codBalotario}/${codCategoria}
    /${codArea}
    /${codLocal}`,{ reportProgress: true }).toPromise();
  }

  /* MONITOREO CARGO */
  tabCargoCabecera(codBalotario: number, codCategoria: number, codArea:number, codLocal:number):Promise<any> {
    return this.http.get<any>(`${this.HOST}/monitoreo/tabCargoCabecera/${codBalotario}/${codCategoria}
    /${codArea}
    /${codLocal}`,{ reportProgress: true }).toPromise();
  }

  tabCargoDetalleTabla(codBalotario: number, codCategoria: number, codArea:number, codLocal:number):Promise<any> {
    return this.http.get<any>(`${this.HOST}/monitoreo/tabCargoDetalle/${codBalotario}/${codCategoria}
    /${codArea}
    /${codLocal}`,{ reportProgress: true }).toPromise();
  }


 /* MONITOREO CATEGORIA */
  tabCategoriaCabecera(codBalotario: number, codCategoria: number, codArea:number, codLocal:number):Promise<any> {
    return this.http.get<any>(`${this.HOST}/monitoreo/tabCategoriaCabecera/${codBalotario}/${codCategoria}
    /${codArea}
    /${codLocal}`,{ reportProgress: true }).toPromise();
  }

  tabCategoriaDetalleTabla(codBalotario: number, codCategoria: number, codArea:number, codLocal:number):Promise<any> {
    return this.http.get<any>(`${this.HOST}/monitoreo/tabCategoriaDetalle/${codBalotario}/${codCategoria}
    /${codArea}
    /${codLocal}`,{ reportProgress: true }).toPromise();
  }

  /* MONITOREO AREA */
  tabAreaCabecera(codBalotario: number, codCategoria: number, codArea:number, codLocal:number):Promise<any> {
    return this.http.get<any>(`${this.HOST}/monitoreo/tabAreaCabecera/${codBalotario}/${codCategoria}
    /${codArea}
    /${codLocal}`,{ reportProgress: true }).toPromise();
  }

  tabAreaDetalleTabla(codBalotario: number, codCategoria: number, codArea:number, codLocal:number):Promise<any> {
    return this.http.get<any>(`${this.HOST}/monitoreo/tabAreaDetalle/${codBalotario}/${codCategoria}
    /${codArea}
    /${codLocal}`,{ reportProgress: true }).toPromise();
  }

  /* MONITOREO LOCAL */
  tabLocalCabecera(codBalotario: number, codCategoria: number, codArea:number, codLocal:number):Promise<any> {
    return this.http.get<any>(`${this.HOST}/monitoreo/tabLocalCabecera/${codBalotario}/${codCategoria}
    /${codArea}
    /${codLocal}`,{ reportProgress: true }).toPromise();
  }

  tabLocalDetalleTabla(codBalotario: number, codCategoria: number, codArea:number, codLocal:number):Promise<any> {
    return this.http.get<any>(`${this.HOST}/monitoreo/tabLocalDetalle/${codBalotario}/${codCategoria}
    /${codArea}
    /${codLocal}`,{ reportProgress: true }).toPromise();
  }




}
