
export class PersonaRequest {
    id?: number;
    codUsuario?: number;
    usuario: string;
    clave: string;
    codTrabajador: string;
    primerNombre: string;
    segundoNombre: string;
    apePaterno: string;
    apeMaterno: string;
    correoElectronico: string;
    sexo: string;
    fecNacimiento: string;
    fecIngreso: string;
    sueldoBasico: number;
    idTipoDocumento: number;
    numDocumento: string;
    idCargo?: number;
    idCategoria: number;
    idArea: number;
    idLocal: number;
    idRol: number;
    idEstado: number;
    flagEditar: number;
}