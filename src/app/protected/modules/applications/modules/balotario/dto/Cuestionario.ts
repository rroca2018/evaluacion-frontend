export class Cuestionario {

    public static get identityQuestion(): any {
        if (!Cuestionario.existQuestion()) {
            return {} as any;
        }
        return JSON.parse(atob(localStorage.getItem('question'))) as any;
    }

    public static get identityReloaded(): any {
        if (!Cuestionario.existReloaded()) {
            return {} as any;
        }
        return JSON.parse(atob(localStorage.getItem('reloaded'))) as any;
    }

    public static get identityIteration(): any {
        if (!Cuestionario.existIteration()) {
            return {} as any;
        }
        return JSON.parse(atob(localStorage.getItem('iteration'))) as any;
    }

    public static get identityNumberQuestion(): any {
        if (!Cuestionario.existNumberQuestion()) {
            return {} as any;
        }
        return JSON.parse(atob(localStorage.getItem('numberQuestion'))) as any;
    }

    public static get identityResult(): any {
        if (!Cuestionario.existResult()) {
            return {} as any;
        }
        return JSON.parse(atob(localStorage.getItem('result'))) as any;
    }

    public static get identityCalification(): any {
        if (!Cuestionario.existCalification()) {
            return {} as any;
        }
        return JSON.parse(atob(localStorage.getItem('calification'))) as any;
    }

    public static get identityResultCalificacion(): any {
        if (!Cuestionario.existResultCalificacion()) {
            return {} as any;
        }
        return JSON.parse(atob(localStorage.getItem('resultCalificacion'))) as any;
    }

    public static get identityTotalAttempt(): any {
        if (!Cuestionario.existTotalAttemptt()) {
            return {} as any;
        }
        return JSON.parse(atob(localStorage.getItem('totalAttempt'))) as any;
    }

    public static get detenerCuestionario() {
        return (JSON.parse(localStorage.getItem('jStorage')))? 
        JSON.stringify(JSON.parse(localStorage.getItem('jStorage')).__jstorage_meta.TTL).length : 
        2;
    }

    public static get identityResultPorcentaje(): any {
        if (!Cuestionario.existResultPorcentaje()) {
            return {} as any;
        }
        return JSON.parse(atob(localStorage.getItem('resultPorcentaje'))) as any;
    }

    /******************************************************************/

    public static existQuestion(): boolean {
        return localStorage.getItem('question') !== null;
    }

    public static existTotalAttemptt(): boolean {
        return localStorage.getItem('totalAttempt') !== null;
    }

    public static existReloaded(): boolean {
        return localStorage.getItem('reloaded') !== null;
    }

    public static existIteration(): boolean {
        return localStorage.getItem('iteration') !== null;
    }

    public static existNumberQuestion(): boolean {
        return localStorage.getItem('numberQuestion') !== null;
    }

    public static existResult(): boolean {
        return localStorage.getItem('result') !== null;
    }

    public static existCalification(): boolean {
        return localStorage.getItem('calification') !== null;
    }

    public static existResultCalificacion(): boolean {
        return localStorage.getItem('resultCalificacion') !== null;
    }

    public static existResultPorcentaje(): boolean {
        return localStorage.getItem('resultPorcentaje') !== null;
    }

/*
    public static existPreguntaCorrecta(): boolean {
        return localStorage.getItem('resultPorcentaje') !== null;
    }

    public static existPreguntaTotal(): boolean {
        return localStorage.getItem('resultPorcentaje') !== null;
    }*/


    /******************************************************************/

    public static start(question: any, totalAttempt: any): void {
        localStorage.setItem('question',  btoa(unescape(encodeURIComponent(JSON.stringify(question)))));
        localStorage.setItem('totalAttempt',  btoa(unescape(encodeURIComponent(JSON.stringify(totalAttempt)))));
       
       /* localStorage.setItem('question', btoa(JSON.stringify(question)));
        localStorage.setItem('totalAttempt', btoa(JSON.stringify(totalAttempt)));*/
    }
 
    public static startIteration(iteration: any): void {
        localStorage.setItem('iteration', btoa(JSON.stringify(iteration)));
    }

    public static startQuestion(numberQuestion: any): void {
        localStorage.setItem('numberQuestion', btoa(JSON.stringify(numberQuestion)));
    }

    public static startReloaded(reloaded: any): void {
        localStorage.setItem('reloaded', btoa(JSON.stringify(reloaded)));
    }

    public static startResultado(result: any): void {
        localStorage.setItem('result', btoa(JSON.stringify(result)));
    }

    public static startCalification(calification: any): void {
        localStorage.setItem('calification', btoa(JSON.stringify(calification)));
    }

    public static startResultCalificacion(resultCalificacion: any): void {
        localStorage.setItem('resultCalificacion', btoa(JSON.stringify(resultCalificacion)));
    }

    public static startResultPorcentaje(resultPorcentaje: any): void {
        localStorage.setItem('resultPorcentaje', btoa(JSON.stringify(resultPorcentaje)));
    }

}

