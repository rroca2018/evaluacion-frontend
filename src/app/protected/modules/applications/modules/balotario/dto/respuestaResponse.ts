export interface respuestaResponse {
    idRespuesta: number;
    nombreRespuesta: string;
    letra: string;
}