
export class PersonaItemResponse {


    constructor(public idCodigo: number, public nombreCompleto: string,public identificacion: string) {}
  }

  export interface WsResponsePersonaItem {
    response: PersonaItemResponse[];
    codResultado: number;
    msgResultado: string;
    total: number;
  }

	