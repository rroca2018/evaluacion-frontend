
export class Respuesta {
    idRespuesta?: number;
    letra?: string;
    nombreRespuesta?: string;
    fiCorrecta?: number;
    idPregunta?: number;
    checked?: boolean;
}