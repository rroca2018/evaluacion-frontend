export class BalotarioTema{

   codBalotario: number;
   codTema: number;
   descripcion :string;
   cantPorTema: number;
   flagAccion: number;

}