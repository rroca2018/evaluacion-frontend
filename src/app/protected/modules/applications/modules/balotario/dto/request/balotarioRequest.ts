import { BalotarioPersona } from './balotarioPersona';
import { BalotarioTema } from './balotarioTema';

export class BalotarioRequest {
    codBalotario?: number;
    codEmpresa?: number;
    codUsuario?: number;
    nomDescripcion?: string;
    cantIntento?: number;
    cantTiempo?: number;
    califAprobatoria?: number;
    nomMensaje?: string;
    fecInicio?: string;
    fecFin?: string;
    tipoBalotario?: string;
    codCargo?: number;
    codCategoria: number;
    codArea: number;
    codLocal: number;
    codEstado: number;
    flagEditar: number;
    estado: string;
    balotarioRealizado?: number;


	lstBalotarioPersona : BalotarioPersona[];
    lstBalotarioTema : BalotarioTema[];
    

   
}