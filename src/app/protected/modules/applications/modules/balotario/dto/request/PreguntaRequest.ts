import { SecurityRoutingModule } from 'app/protected/modules/security/security-routing.module';
import { Respuesta } from './respuesta';

export class PreguntaRequest {
    idPregunta?: number;
    descripcionPregunta? : string;
    descripcionCortaPregunta? : string;
    idTema? : number;
    estado?: string;
    descripcionTema? : string;
    respuesta?: Respuesta[];
    flagPreguntaRealizada?: number;
    flagEditar: number;
}