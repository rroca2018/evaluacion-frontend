import { NumberSymbol } from '@angular/common';

export class FiltroRequest {
    idYear?: number;
    idBalotario?: number;
    idCategoria?: number;
    idArea? :number;
    idLocal?: number;
    categoria?:  Categoria;
    descripcionBalotario?: string;
    catNombreLabel : string;
    areaNombreLabel : string;
    localNombreLabel: string;
    flagMenuSeleccionado: boolean;

}
interface Categoria{
    idCodigo?: number;
    cidNombre?: string;
}

