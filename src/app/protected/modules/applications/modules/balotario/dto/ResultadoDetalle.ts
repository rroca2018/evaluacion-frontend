export class ResultadoDetalle{

    codEmpresa: number;
	codBalotario: number;
	codUsuario: number;
	cantIntento: number;
	codPregunta: number;
	codRespuesta: number;
	flagCorrecta: string;
}