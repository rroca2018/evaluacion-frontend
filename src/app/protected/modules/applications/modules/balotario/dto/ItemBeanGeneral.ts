export class ItemBeanGeneral {
    idCodigo: number;
    cidCodigo?: string;
    cidNombre: string;
    categoria: string;
    area: string;
    estados?: string;
    alertas?:string;
    nombrePerfil?: string;
  }
 

  export interface WsItemBeanGeneralResponse {
    response: ItemBeanGeneral[];
    codResultado: number;
    msgResultado: string;
    total: number;
  }