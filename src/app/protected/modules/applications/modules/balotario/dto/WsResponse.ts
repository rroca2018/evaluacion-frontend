export interface WsResponse {
    response: any[];
    total: number;
    codResultado: number;
    msgResultado: string;
}