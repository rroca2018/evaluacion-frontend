export class Proyecto{

    item: string;
    codigo: string;
    descripcion: string;
    tiempoEjecucion: string;
    fecInicioEjecucion: string;
    fecFinEjecucion: string;
    fecUltimaActualizacion: string;
    supervisor: string;
    estado: string;
    cidEstado: string;
    numPlazo: number;
    color: string;
    fidProyecto: number;

}


export interface WsResponseProyecto {
    response: Proyecto[];
    total: number;
    codResultado: number;
    msgResultado: string;
  }
  