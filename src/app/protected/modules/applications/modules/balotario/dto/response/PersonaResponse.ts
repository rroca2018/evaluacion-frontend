export class Persona{

    item: number;
    nombre: string;
    identificacion: string;
   

}


export interface WsResponsePersona {
    response: Persona[];
    total: number;
    codResultado: number;
    msgResultado: string;
  }
  