import { ResultadoDetalle } from './ResultadoDetalle';

export class Resultado {

    codEmpresa: number;
    codBalotario: number;
    codUsuario: number;
    cantIntento: number;
    nota: number;
    lstResultadoDetalle: ResultadoDetalle[];


}