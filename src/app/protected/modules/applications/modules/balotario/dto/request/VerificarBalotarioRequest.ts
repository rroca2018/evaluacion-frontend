export class VerificarBalotarioRequest {
    codBalotario?: number;
    fecInicio: string;
    fecFin: string;
    tipo: string;
}