import { respuestaResponse } from './respuestaResponse';

export interface preguntaResponse {
    idPregunta: number;
    nombrePregunta: string;
    idRespuestaCorrecto: number;
    nombreTema: string;
    respuesta: respuestaResponse[];
}