import { Component, OnInit, ViewChild } from '@angular/core';
import { Proyecto } from '../../../dto/response/Proyecto';
import { MatTableDataSource, MatPaginator, MatSort, MatDialogRef, MatDialog, MatRadioGroup } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { BalotarioService } from '../../../service/balotario.service';
import { MENSAJES } from 'app/common';
import { VerPreguntasComponent } from './ver-preguntas/ver-preguntas.component';
import { RegistrarActualizarBalotariosComponent } from './registrar-actualizar-balotarios/registrar-actualizar-balotarios.component';
import { SelectionModel } from '@angular/cdk/collections';
import { InfoMessageComponent } from '@shared/components/info-message/info-message.component';

@Component({
  selector: 'app-configuracion-balotarios',
  templateUrl: './configuracion-balotarios.component.html',
  styleUrls: ['./configuracion-balotarios.component.scss']
})
export class ConfiguracionBalotariosComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('radioButonSeleccionado', null) radioButonSeleccionado: MatRadioGroup;

  dataSource: MatTableDataSource<Proyecto>;
  WsResponseBalotario : any;
  proyectoResponse : Proyecto[];
  balotarioResponse: any[];

  objproyectoResponse : Proyecto;

  filtrosForm: FormGroup;
  columnas: string[] = [];


  isLoading: boolean;
  idPerfil: number; 
  codPerfil: string;
  
  mensaje: string;
  disableBuscar: boolean;
  datos:Proyecto;

  txtBuscar: String;
  seleccionado: string = '';

  constructor(
    private dialog: MatDialog,
    private balotarioService: BalotarioService,

  ) {
    this.dataSource = new MatTableDataSource([]);
    this.dataSource.sort = this.sort;
   }

  ngOnInit() {
    this.crearFiltrosForm();
    this.tableColumnaBalotarios();
    this.listarBalotario();
  }


  crearFiltrosForm() {
    this.filtrosForm = new FormGroup({
      codDocFrmCtrl: new FormControl(null), 
      desProyectoFrmCtrl: new FormControl(null),
      fechaRegDesdeFrmCtrl: new FormControl(null),
      fechaRegHastaFrmCtrl: new FormControl(null),
      estadoFrmCtrl: new FormControl(null)
    });
  }
  
  tableColumnaBalotarios(): void{
    this.columnas = ['seleccion','nomDescripcion','fecInicio','fecFin','tipoBalotario' ,'estado','acciones'];   
  }

  cantBalotarios: number;
  public cargarTablaBalotario(): void {
    if (this.balotarioResponse != null && this.balotarioResponse.length > 0) {
      this.dataSource = new MatTableDataSource(this.balotarioResponse);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;   
      this.cantBalotarios = this.balotarioResponse.length;
    }
  }


  radioSeleccionado(event: any){
    this.seleccionado = event.value;
  }

 listarBalotario()  {
    this.balotarioService.listarBalotario().subscribe((wsResponseProyecto: any) => {
      if (wsResponseProyecto.codResultado == 1) {
        this.balotarioResponse = (wsResponseProyecto.response != null) ? wsResponseProyecto.response : [];
        this.cargarTablaBalotario();
        this.seleccionado = "";

      } else {
        this.mensaje = MENSAJES.ERROR_NOFUNCION;
      }

    },
      _error => {
        this.mensaje = MENSAJES.ERROR_SERVICIO;
      }

    );    
    this.cargarTablaBalotario();
  }



  verPreguntas(){
    if(this.seleccionado == ""){
      this.openDialogMensaje(MENSAJES.BALOTARIO.SELECCIONAR_BALOTARIO, true, false, null);
    }
      else{
      const dialogReg: MatDialogRef<VerPreguntasComponent> = this.dialog.open(VerPreguntasComponent, {
        disableClose: true,
        panelClass: 'dialog-no-padding',
        width: '50%',
        height: '80%',
        data: { 
              balotarioSeleccionado :  this.seleccionado 
        }
      });
    }
  }

  crearBalotario(): void{
    const dialogReg: MatDialogRef<RegistrarActualizarBalotariosComponent> = this.dialog.open(RegistrarActualizarBalotariosComponent, {
      disableClose: true,
      panelClass: 'dialog-no-padding',
      width: '45%',
      height: '100%',
      data: { 
      }
    });

    dialogReg.afterClosed().subscribe((valor: any) => {     
       
      if(valor) {
        this.listarBalotario();
        this.cargarTablaBalotario();
      } 
    });
  }

  editarBalotario(coBalotario : number): void{
  
    this.balotarioService.obtenerBalotarioCompleto(coBalotario).subscribe(dataBalotario => {
       
      const dialogReg: MatDialogRef<RegistrarActualizarBalotariosComponent> = this.dialog.open(RegistrarActualizarBalotariosComponent, {
        disableClose: true,
        panelClass: 'dialog-no-padding',
        width: '40%',
        height:'100%',     
        data: {
          dataBalotario     
        }
      });

      dialogReg.afterClosed().subscribe((valor: any) => {     
        console.log(valor)
        if(valor) {
          this.listarBalotario();
          this.txtBuscar = "";
        }     
      
      });
    });
  }

  onKeyup(){
     
    if(this.txtBuscar.trim().length>0){
      this.balotarioService.buscarBalotarios(this.txtBuscar)
      .subscribe(
        (wsResponseProyecto : any)=> {
           
          if(wsResponseProyecto.codResultado == 1){
            this.balotarioResponse = (wsResponseProyecto.response != null) ? wsResponseProyecto.response : [];       
            this.cargarTablaBalotario();
          }else{
            this.mensaje = MENSAJES.ERROR_NOFUNCION;
          // this.openDialogMensaje(null,  wsResponseProyecto.msgResultado, true, false, wsResponseProyecto.codResultado);
      
          }
        
        },
        _error => {
        
          this.mensaje = MENSAJES.ERROR_SERVICIO;
        /*   this.openDialogMensaje(this.mensaje, MENSAJES.PREFACTIBILIDAD.TITLE, true, false, null);
          console.error(error);
          this.disableBuscar = false; */
        }
      
      );    
    }else{
      this.listarBalotario();
    }
  }
  

  selection = new SelectionModel<Proyecto>(true, []);
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }


  public openDialogMensaje(message: string, alerta: boolean, confirmacion: boolean, valor: any
    ): void {
      const dialogRef = this.dialog.open(InfoMessageComponent, {
        disableClose: true,
        data: {
          message: message,
          alerta: alerta,
          confirmacion: confirmacion,
          valor: valor
        }
      });
      dialogRef.afterClosed().subscribe((ok: number) => {
        if (ok == 0) {
  
        }
      });
    }

}
