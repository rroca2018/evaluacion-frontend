import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { InfoMessageComponent } from '@shared/components/info-message/info-message.component';
import { MENSAJES } from 'app/common';
import { filter } from 'rxjs/operators';
import { Area } from '../../../../dto/request/Area';
import { ConfiguracionService } from '../../../../service/configuracion.service';
import { ItemService } from '../../../../service/item.service';
import { RegistrarActualizarCategoriaComponent } from '../registrar-actualizar-categoria/registrar-actualizar-categoria.component';

@Component({
  selector: 'app-registrar-actualizar-area',
  templateUrl: './registrar-actualizar-area.component.html',
  styleUrls: ['./registrar-actualizar-area.component.scss']
})
export class RegistrarActualizarAreaComponent implements OnInit {

  
  areaForm: FormGroup;
  textoButton: String;
  tituloModalPregunta: String;
  area: Area = new Area();
  dialogRefMessage: MatDialogRef<any>;
  mensaje: any;
  mostrarEstado: boolean = false;
  listCategoria: any[];

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<RegistrarActualizarCategoriaComponent>,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private configuracionService: ConfiguracionService,
    private itemService: ItemService,
    @Inject(MAT_DIALOG_DATA)
    public datos: DataArea,
  ) { }

  ngOnInit() {
    debugger;
    this.cargarCombos();
    console.log(this.datos.dataArea)
    if (this.datos.dataArea) {
    this.tituloModalPregunta = "Modificar área";
    this.textoButton = "Actualizar";
    this.area = this.datos.dataArea.response;
    this.mostrarEstado = true;  
    }
    else{
      this.tituloModalPregunta = "Agregar área";
      this.textoButton = "Grabar";
    }
  
    this.formulario();
  }

  cargarCombos() {
    this.itemService.listadoCategoria().subscribe(data => {
      this.listCategoria = data.response
      console.log('categoria:' + data.response)
    });
  }

  formulario() {
    this.areaForm = this.formBuilder.group({
      descripcionAreaFrm: [''],  
      categoriaFrm: [''],  
      estadoFrm: [''],  
    });
  }
  
  grabarArea(){
    if (this.validarFormulario() == false) {
      this.openDialogMensaje(MENSAJES.AREA.CAMPOS_OBLIGATORIOS, true, false, null);
    }
    else if (this.area.flagEditar != 1) {
      this.openDialogMensajeConfirm(MENSAJES.AREA.GUARDAR_AREA, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(async() => {   
       debugger;
          await this.configuracionService.grabarArea(this.area).then((wsResponse : any)=> {
            debugger;
            console.log(wsResponse);  
           },  error => {
                     
             this.mensaje = MENSAJES.ERROR_SERVICIO;
             console.error(this.mensaje); 
           }      
       ) ;
   
          this.dialogRef.close(true);
          this.snackBar.open("El área ha sido registrado correctamente!");
        });
    } 
    else {
            debugger;       
      this.openDialogMensajeConfirm(MENSAJES.AREA.MODIFICAR_AREA, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(async() => {   
       debugger;
          await this.configuracionService.grabarArea(this.area).then((wsResponse : any)=> {
            debugger;
            console.log(wsResponse);  
           },  error => {
                     
             this.mensaje = MENSAJES.ERROR_SERVICIO;
             console.error(this.mensaje); 
           }      
       ) ;
   
          this.dialogRef.close(true);
          this.snackBar.open("El área ha sido registrado correctamente!");
        });
    }    
 

  }

  validarFormulario(): boolean {
    debugger;
    let valido: boolean = true;
    if (typeof this.area.descripcionArea === 'undefined' || this.area.descripcionArea == "") {
      valido = false;
    }
    else if (typeof this.area.idCategoria === 'undefined' || this.area.idCategoria == 0) {
      valido = false;
    }
    return valido;
  }

  public openDialogMensajeConfirm(message: string, confirmacion: boolean): void {

    this.dialogRefMessage = this.dialog.open(InfoMessageComponent, {
      //width: '400px',
      disableClose: true,
      data: { message: message, confirmacion: confirmacion }
    });
  }



  public openDialogMensaje(message: string, alerta: boolean, confirmacion: boolean, valor: any
  ): void {
    const dialogRef = this.dialog.open(InfoMessageComponent, {
      disableClose: true,
      data: {
        message: message,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe((ok: number) => {
      if (ok == 0) {

      }
    });
  }

  cerrarFormulario() {
    if (this.area.flagEditar != 1) {
      this.openDialogMensajeConfirm(MENSAJES.AREA.BOTON_SALIR_GUARDAR, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(() => {
          this.dialogRef.close(true);

        });
    } else {
      this.openDialogMensajeConfirm(MENSAJES.AREA.BOTON_SALIR_MODIFICAR, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(() => {
          this.dialogRef.close(true);

        });
    }
  }
}
interface DataArea {
  dataArea?: any
}
