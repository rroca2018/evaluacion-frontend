import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { CargaMasivaComponent } from './carga-masiva/carga-masiva.component';
import { PersonaService } from '../../../../service/persona.service';

@Component({
  selector: 'app-plantilla-excel',
  templateUrl: './plantilla-excel.component.html',
  styleUrls: ['./plantilla-excel.component.scss']
})
export class PlantillaExcelComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<PlantillaExcelComponent>,
    private dialog: MatDialog,
    private personaService: PersonaService) { }

  ngOnInit() {
  }


  descargarPlantillaExcel(): void{
    let filename = "Plantilla_Carga_Usuarios.xls";
    this.personaService.downloadFile()
    .subscribe(data => {
      //Guarda en la pc del cliente.
      saveAs(new Blob([data], {type: MimeType['xls']}),filename);
    })
  
  }




  importarExcel(): void{
    const dialogReg: MatDialogRef<CargaMasivaComponent> = this.dialog.open(CargaMasivaComponent, {
      disableClose: true,
      panelClass: 'dialog-no-padding',
      width: '40%',
      data: {
        
      },
     
    });

    dialogReg.afterClosed().subscribe((valor: any) => {     
      debugger;
   
      if(valor == 0) {
        this.dialogRef.close(valor);
      }     
    
    });

  }
 

}
