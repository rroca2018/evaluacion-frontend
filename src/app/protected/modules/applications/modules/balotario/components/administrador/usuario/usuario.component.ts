import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';


import { MatTableDataSource, MatPaginator, MatDialog, PageEvent, MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { InfoMessageComponent } from '@shared/components/info-message/info-message.component';
import { MENSAJES } from 'app/common';
import { RegistrarActualizarComponent } from './registrar-actualizar/registrar-actualizar.component';
import { ItemService } from '../../../service/item.service';
import { ItemBean } from '../../../dto/ItemBean';
import { Proyecto, WsResponseProyecto } from '../../../dto/response/Proyecto';
import { PersonaService } from '../../../service/persona.service';
import { PlantillaExcelComponent } from './plantilla-excel/plantilla-excel.component';
import {MatSort} from '@angular/material/sort';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss']
})
export class UsuarioComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  //@ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatSort) sort: MatSort;

  filtrosForm: FormGroup;

  fechaActual = new Date();

  estados: any[];
  alertas: any[];
  proyectos: Proyecto[];
  pagina = 1;
  cantidad = 2;
  total = 0;
  cantidadObservacion = 3;
  paginaObservacion = 1;

  totalObservacion = 0;


  // LISTA EXCEL
  exportExcelPreliminar: string;

  dataItemEstado: ItemBean;
  dataItemAlerta: ItemBean;


  public cidCodigo : string = " ";
  public fechaInicio : string = " ";
  public fechaFin : string = " ";


  // Tabla
  dataSource: MatTableDataSource<Proyecto>;
  WsResponsePersona : any;
  proyectoResponse : Proyecto[];
  personaResponse: any[];

  objproyectoResponse : Proyecto;

  txtBuscar: String;




  isLoading: boolean;
  idPerfil: number; 
  codPerfil: string;
  columnas: string[] = [];
  mensaje: string;
  disableBuscar: boolean;
  datos:Proyecto;
  expedienteSuscripcionEncargado : Subscription;
  idUsuarioCoordinador: number;
  cantPersonas: number;
  
    constructor(private formBuilder: FormBuilder,
      private dialog: MatDialog,
      private personaService: PersonaService,
      private router: Router) 
      { 
      this.dataSource = new MatTableDataSource([]);
      this.dataSource.sort = this.sort;
      }

    ngOnInit() {
      this.dataSource.sort = this.sort;
      this.crearFiltrosForm();
      this.tableColumnaPersona();
      this.listarPersona();
     
      //this.expedienteSuscripcionEncargado = this.socket.fromEvent('expedienteCoordinador').subscribe(() => this.cargarPerfilPrefactibilidad());
      
    }

    cambiarPagina(event: PageEvent) {

        this.pagina = event.pageIndex + 1;
        this.cantidad = event.pageSize;
     
    }

    crearFiltrosForm() {
      this.filtrosForm = new FormGroup({
        codDocFrmCtrl: new FormControl(null), 
        desProyectoFrmCtrl: new FormControl(null),
        fechaRegDesdeFrmCtrl: new FormControl(null),
        fechaRegHastaFrmCtrl: new FormControl(null),
        estadoFrmCtrl: new FormControl(null)
      });
    }

    get codDocFrmCtrl() { return this.filtrosForm.get('codDocFrmCtrl'); }
    get desProyectoFrmCtrl(){ return this.filtrosForm.get('desProyectoFrmCtrl');}
    get fechaRegDesdeFrmCtrl() { return this.filtrosForm.get('fechaRegDesdeFrmCtrl'); }
    get fechaRegHastaFrmCtrl() { return this.filtrosForm.get('fechaRegHastaFrmCtrl'); }
    get estadoFrmCtrl() { return this.filtrosForm.get('estadoFrmCtrl'); }


    reiniciar() {
      this.filtrosForm.reset('');
    }

    /* ngOnDestroy(){
      this.expedienteSuscripcionEncargado.unsubscribe();
    } */

    /*
    cargarDataEstado() {
      this.itemComboService.ObtenerEstadoProyecto().subscribe(dataItem => {
        this.dataItemEstado = Object.assign({
          estados: dataItem.response
        });
      });
    } */

    public cargarTablaPersona(): void {
      if (this.personaResponse != null && this.personaResponse.length > 0) {
        this.dataSource = new MatTableDataSource(this.personaResponse);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        console.log("tabla -> " + this.dataSource);
        this.cantPersonas = this.personaResponse.length;
        console.log("personas -> " +  this.cantPersonas);
      }
    }



    tableColumnaPersona(): void{
      this.columnas = ['nombreCompleto','noUsuario','correo','desCargo','desArea','acciones'];
       
    }


    async listarPersona()  {

      await this.personaService.listarPersonas()
      .then(
        (wsResponseProyecto : any)=> {
          debugger;
          if(wsResponseProyecto.codResultado == 1){
             this.personaResponse = (wsResponseProyecto.response != null) ? wsResponseProyecto.response : [];       
             this.cargarTablaPersona();
          }else{
            this.mensaje = MENSAJES.ERROR_NOFUNCION;
           // this.openDialogMensaje(null,  wsResponseProyecto.msgResultado, true, false, wsResponseProyecto.codResultado);
      
          }
        
        },
        error => {
         
          this.mensaje = MENSAJES.ERROR_SERVICIO;
        /*   this.openDialogMensaje(this.mensaje, MENSAJES.PREFACTIBILIDAD.TITLE, true, false, null);
          console.error(error);
          this.disableBuscar = false; */
        }
      
      );    

      this.cargarTablaPersona();
    }


    onKeyup(){
      debugger;
      if(this.txtBuscar.trim().length>0){
        this.personaService.buscarPersonas(this.txtBuscar)
        .subscribe(
          (wsResponseProyecto : any)=> {
            debugger;
            if(wsResponseProyecto.codResultado == 1){
              this.personaResponse = (wsResponseProyecto.response != null) ? wsResponseProyecto.response : [];       
              this.cargarTablaPersona();
            }else{
              this.mensaje = MENSAJES.ERROR_NOFUNCION;
            // this.openDialogMensaje(null,  wsResponseProyecto.msgResultado, true, false, wsResponseProyecto.codResultado);
        
            }
          
          },
          error => {
          
            this.mensaje = MENSAJES.ERROR_SERVICIO;
          /*   this.openDialogMensaje(this.mensaje, MENSAJES.PREFACTIBILIDAD.TITLE, true, false, null);
            console.error(error);
            this.disableBuscar = false; */
          }
        
        );    
      }else{
        this.listarPersona();
      }
    }

   
        
  agregarPersona(): void{
    const dialogReg: MatDialogRef<RegistrarActualizarComponent> = this.dialog.open(RegistrarActualizarComponent, {
      disableClose: true,
      panelClass: 'dialog-no-padding',
      width: '40%',
      height: '100%',
      data: { 
      }
    });

    dialogReg.afterClosed().subscribe((valor: any) => {     
      if(valor) {
        this.listarPersona();
        this.cargarTablaPersona();
      } 
    });

  }

  editarPersona(coUsuario : string): void{
  
    this.personaService.obtenerPersona(coUsuario).subscribe(dataPersona => {
      const dialogReg: MatDialogRef<RegistrarActualizarComponent> = this.dialog.open(RegistrarActualizarComponent, {
        disableClose: true,
        panelClass: 'dialog-no-padding',
        width: '40%',
        height:'100%',
        data: {
          dataPersona
        }
      });

      dialogReg.afterClosed().subscribe((valor: any) => {     
        debugger;
        if(valor) {
          this.listarPersona();
          this.txtBuscar = "";
        }     
      
      });
    });
  }

  descargarPlantillaExcel(): void{
    const dialogReg: MatDialogRef<PlantillaExcelComponent> = this.dialog.open(PlantillaExcelComponent, {
      disableClose: true,
      panelClass: 'dialog-no-padding',
      width: '40%',
      data: {
        
      },
     
    });

    dialogReg.afterClosed().subscribe((valor: any) => {     
      debugger;
      if(valor == 0) {
       this.listarPersona();
      }     
    
    });

  }
 


}

