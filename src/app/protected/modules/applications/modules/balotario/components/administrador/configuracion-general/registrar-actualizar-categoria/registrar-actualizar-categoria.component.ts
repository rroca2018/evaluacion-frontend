import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { InfoMessageComponent } from '@shared/components/info-message/info-message.component';
import { MENSAJES } from 'app/common';
import { filter } from 'rxjs/operators';
import { Categoria } from '../../../../dto/request/Categoria';
import { ConfiguracionService } from '../../../../service/configuracion.service';

@Component({
  selector: 'app-registrar-actualizar-categoria',
  templateUrl: './registrar-actualizar-categoria.component.html',
  styleUrls: ['./registrar-actualizar-categoria.component.scss']
})
export class RegistrarActualizarCategoriaComponent implements OnInit {

  categoriaForm: FormGroup;
  textoButton: String;
  tituloModalCategoria: String;
  categoria: Categoria = new Categoria();
  dialogRefMessage: MatDialogRef<any>;
  mensaje: any;
  mostrarEstado: boolean = false;


  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<RegistrarActualizarCategoriaComponent>,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private configuracionService: ConfiguracionService,
    @Inject(MAT_DIALOG_DATA)
    public datos: DataCategoria,
  ) { }

  ngOnInit() {
    debugger;
    console.log(this.datos.dataCategoria)
    if (this.datos.dataCategoria) {
    this.tituloModalCategoria = "Modificar categoría";
    this.textoButton = "Actualizar";
    this.categoria = this.datos.dataCategoria.response;
    this.mostrarEstado = true;  
    }
    else{
      this.tituloModalCategoria = "Agregar categoría";
      this.textoButton = "Grabar";
    }
  
    this.formulario();
  }


  formulario() {
    this.categoriaForm = this.formBuilder.group({
      descripcionCategoriaFrm: [''],  
      estadoFrm: [''],  
    });
  }
  
  grabarCategoria(){
    if (this.validarFormulario() == false) {
      this.openDialogMensaje(MENSAJES.CATEGORIA.CAMPOS_OBLIGATORIOS, true, false, null);
    }
    else if (this.categoria.flagEditar != 1) {
      this.openDialogMensajeConfirm(MENSAJES.CATEGORIA.GUARDAR_CATEGORIA, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(async() => {   
       debugger;
          await this.configuracionService.grabarCategoria(this.categoria).then((wsResponse : any)=> {
            debugger;
            console.log(wsResponse);  
           },  error => {
                     
             this.mensaje = MENSAJES.ERROR_SERVICIO;
             console.error(this.mensaje); 
           }      
       ) ;
   
          this.dialogRef.close(true);
          this.snackBar.open("La categoría ha sido registrado correctamente!");
        });
    } 
    else {
            debugger;       
      this.openDialogMensajeConfirm(MENSAJES.CATEGORIA.MODIFICAR_CATEGORIA, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(async() => {   
       debugger;
          await this.configuracionService.grabarCategoria(this.categoria).then((wsResponse : any)=> {
            debugger;
            console.log(wsResponse);  
           },  error => {
                     
             this.mensaje = MENSAJES.ERROR_SERVICIO;
             console.error(this.mensaje); 
           }      
       ) ;
   
          this.dialogRef.close(true);
          this.snackBar.open("La categoría ha sido registrado correctamente!");
        });
    }    
 

  }

  validarFormulario(): boolean {
    debugger;
    let valido: boolean = true;
    if (typeof this.categoria.descripcionCategoria === 'undefined' || this.categoria.descripcionCategoria  == "") {
      valido = false;
    }
    return valido;
  }
  public openDialogMensajeConfirm(message: string, confirmacion: boolean): void {

    this.dialogRefMessage = this.dialog.open(InfoMessageComponent, {
      //width: '400px',
      disableClose: true,
      data: { message: message, confirmacion: confirmacion }
    });
  }



  public openDialogMensaje(message: string, alerta: boolean, confirmacion: boolean, valor: any
  ): void {
    const dialogRef = this.dialog.open(InfoMessageComponent, {
      disableClose: true,
      data: {
        message: message,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe((ok: number) => {
      if (ok == 0) {

      }
    });
  }

  cerrarFormulario() {
    if (this.categoria.flagEditar != 1) {
      this.openDialogMensajeConfirm(MENSAJES.CATEGORIA.BOTON_SALIR_GUARDAR, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(() => {
          this.dialogRef.close(true);

        });
    } else {
      this.openDialogMensajeConfirm(MENSAJES.CATEGORIA.BOTON_SALIR_MODIFICAR, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(() => {
          this.dialogRef.close(true);

        });
    }
  }

}

interface DataCategoria {
  dataCategoria?: any
}
