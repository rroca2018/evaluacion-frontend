import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { filter } from 'rxjs/operators';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { MENSAJES } from 'app/common';
import { InfoMessageComponent } from '@shared/components/info-message/info-message.component';
import { PreguntaRequest } from '../../../../dto/request/PreguntaRequest';
import { ItemService } from '../../../../service/item.service';
import { Respuesta } from '../../../../dto/request/respuesta';
import { PreguntasRespuestasService } from '../../../../service/preguntasRespuestas.service';
import { validateHorizontalPosition } from '@angular/cdk/overlay';

@Component({
  selector: 'app-registrar-actualizar-preguntas',
  templateUrl: './registrar-actualizar-preguntas.component.html',
  styleUrls: ['./registrar-actualizar-preguntas.component.scss']
})
export class RegistrarActualizarPreguntasComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  preguntaForm: FormGroup;
  pregunta: PreguntaRequest = new PreguntaRequest();
  respuesta: Respuesta = new Respuesta();
  respuestaResponse: Respuesta[];

  dialogRefMessage: MatDialogRef<any>;
  mensaje: string;
  textoButton: string;
  tituloModalPregunta: string;

  listTema: any[];
  columnas3: string[] = [];
  dataSource: MatTableDataSource<Respuesta>;
  preguntaRealizada: boolean = false;
  mostrarEstado: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<RegistrarActualizarPreguntasComponent>,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA)
    public datos: Datapregunta,
    private itemService: ItemService,
    private preguntasRespuestasService: PreguntasRespuestasService,
  ) {
    this.dataSource = new MatTableDataSource([]);
    this.dataSource.sort = this.sort;
  }
  arrrayRespuesta: Respuesta[];
  cantRespuestas: number;

  ngOnInit() {
    debugger;
    console.log(this.datos.dataPregunta)
    this.dataSource.sort = this.sort;
    this.cargarCombos();
    if (this.datos.dataPregunta) {
      this.mostrarEstado = true;
      this.tableColumnaTemas();
      this.tituloModalPregunta = "Modificar pregunta";
      this.textoButton = "Actualizar";
      this.pregunta = this.datos.dataPregunta.response;
      if(this.pregunta.flagPreguntaRealizada==1){
        this.preguntaRealizada=true;
      }
      this.pregunta.flagEditar =1;
      this.listarRespuestas(this.pregunta.idPregunta);

    }
    else {
      this.tableColumnaTemas();
      this.tituloModalPregunta = "Nueva pregunta";
      this.textoButton = "Grabar";
      this.pregunta = new PreguntaRequest();
  /*     this.dataSource = new MatTableDataSource(arrayRespuesta); */
      this.cantRespuestas = this.dataSource.data.length;
    }
    this.formulario();
  }

  formulario() {
    this.preguntaForm = this.formBuilder.group({
      descripcionPreguntaFrm: [{value: '', disabled: this.preguntaRealizada}],
      descripcionCortaPreguntaFrm: [{value: '', disabled: this.preguntaRealizada}],
      estadoFrm: [''],
      temaFrm: [{value: '', disabled: this.preguntaRealizada}],
    });
  }

  validarFormulario(): boolean {
    debugger;
    let valido: boolean = true;
    if (typeof this.pregunta.descripcionPregunta === 'undefined' || this.pregunta.descripcionPregunta == "") {
      valido = false;
    } else if (typeof this.pregunta.descripcionCortaPregunta === 'undefined' || this.pregunta.descripcionCortaPregunta == "") {
      valido = false;
    } else if (typeof this.pregunta.idTema === 'undefined' || this.pregunta.idTema == 0) {
      valido = false;
    }
    return valido;
  }

  validarTablaRespuestas(): boolean {
    let i: number;
    let valido: boolean = true;
    debugger;
    for (i = 0; i < this.dataSource.data.length; i++) {
      if (typeof this.dataSource.data[i].nombreRespuesta === 'undefined' || this.dataSource.data[i].nombreRespuesta == "") {
        valido = false;
        return valido;
      }
    }
    return valido;
  }

  validarRespuestaCorrectaSeleccionada(): boolean {
    debugger;
    console.log(this.dataSource);
    let i: number;
    let valido: boolean = false;
    debugger;
    for (i = 0; i < this.dataSource.data.length; i++) {
      if (typeof this.dataSource.data[i].fiCorrecta === 'undefined' || this.dataSource.data[i].fiCorrecta == 1) {
        valido = true;
        return valido;
      }
    }
    return valido;
  }

  radioSeleccionado(indice: number) {
    let i: number;
    debugger;
    for (i = 0; i < this.dataSource.data.length; i++) {
      if (i == indice) {
        this.dataSource.data[i].fiCorrecta = 1;
      }
      else {
        this.dataSource.data[i].fiCorrecta = 0;
      }
    }
    debugger;
    console.log(this.dataSource);
  }

  tableColumnaTemas(): void {
    if (this.datos.dataPregunta) {
      this.columnas3 = ['correcta', 'nro', 'nombreRespuesta', 'accion'];
    }
    else {
      this.columnas3 = ['correcta', 'nombreRespuesta', 'accion'];
    }

  }

  guardarPregunta() {
     
          if (this.validarFormulario() == false) {
            this.openDialogMensaje(MENSAJES.PREGUNTA.CAMPOS_OBLIGATORIOS, true, false, null);
          }
          else if (this.validarTablaRespuestas() == false) {
            this.openDialogMensaje(MENSAJES.PREGUNTA.DESCRIPCION_RESPUESTA, true, false, null);
          }  
          else if (this.dataSource.data.length<2) {
            this.openDialogMensaje(MENSAJES.PREGUNTA.VALIDAR_CANTIDAD_RESPUESTAS, true, false, null);
          }
          else if (this.validarRespuestaCorrectaSeleccionada() == false) {
            this.openDialogMensaje(MENSAJES.PREGUNTA.ELEGIR_RESPUESTA, true, false, null);
          } 
    
         else if (this.pregunta.flagEditar != 1) {
            let respuesta = new Respuesta();
            let i: number;
            let arrrayRespuesta: Respuesta[] = [];
            debugger;
            for (i = 0; i < this.dataSource.data.length; i++) {
              respuesta.fiCorrecta = this.dataSource.data[i].fiCorrecta;
              respuesta.nombreRespuesta = this.dataSource.data[i].nombreRespuesta;
              arrrayRespuesta.push(respuesta);
            }
            this.pregunta.respuesta = this.dataSource.data;
            console.log(this.pregunta);
 
            this.openDialogMensajeConfirm(MENSAJES.PREGUNTA.GUARDAR_PREGUNTA, true);
            debugger;
            this.dialogRefMessage.afterClosed()
              .pipe(filter(verdadero => !!verdadero))
              .subscribe(async() => {   
                this.registrarPregunta(this.pregunta); 
                this.dialogRef.close(true);
                this.snackBar.open("Pregunta de grabó correctamente.");
              });
          }
          
          else{
            this.pregunta.flagEditar =1;
            let respuesta = new Respuesta();
            let i: number;
            let arrrayRespuesta: Respuesta[] = [];
            debugger;
            for (i = 0; i < this.dataSource.data.length; i++) {
              respuesta.fiCorrecta = this.dataSource.data[i].fiCorrecta;
              respuesta.nombreRespuesta = this.dataSource.data[i].nombreRespuesta;
              arrrayRespuesta.push(respuesta);
            }
            this.pregunta.respuesta = this.dataSource.data;
            console.log(this.pregunta);

            
            this.openDialogMensajeConfirm(MENSAJES.PREGUNTA.MODIFICAR_PREGUNTA, true);
            debugger;
            this.dialogRefMessage.afterClosed()
              .pipe(filter(verdadero => !!verdadero))
              .subscribe(async() => {   
                this.registrarPregunta(this.pregunta); 
                this.dialogRef.close(true);
                this.snackBar.open("Pregunta se modifico correctamente.");
              });
    
          }
    
         


  }


  agregarRespuesta() {
    debugger;
    let qtRespuestasGlobal : number;
    this.preguntasRespuestasService.obtenerQtRespuestasGlobal()
      .then(
        (wsResponse: any) => {

          if (wsResponse.codResultado == 1) {
            qtRespuestasGlobal = (wsResponse.response != null) ? wsResponse.response : [];
            if(this.dataSource.data.length < qtRespuestasGlobal){
              let respuesta = new Respuesta();
              respuesta.fiCorrecta = 0;
              respuesta.nombreRespuesta = "";
              this.dataSource.data.push(respuesta);
              let indice = this.dataSource.data.length - 1;
              this.dataSource.data[indice].letra = this.obtenerLetra(indice + 1);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
              this.cantRespuestas = this.dataSource.data.length;
            }
            else{
              this.openDialogMensaje(MENSAJES.PREGUNTA.VALIDAR_QTRESPUESTAS_GLOBAL+" "+qtRespuestasGlobal, true, false, null);
            }
          } else {
            this.mensaje = MENSAJES.ERROR_NOFUNCION;

          }

        },
        error => {

          this.mensaje = MENSAJES.ERROR_SERVICIO;

        }

      );
  
  }

  eliminarRespuesta(rowid: number) {

    this.openDialogMensajeConfirm(MENSAJES.PREGUNTA.ELIMINAR_RESPUESTA, true);
    debugger;
    this.dialogRefMessage.afterClosed()
      .pipe(filter(verdadero => !!verdadero))
      .subscribe(async() => {   
        if (rowid > -1) {
          this.dataSource.data.splice(rowid, 1);
          let i: number;
          for (i = 0; i < this.dataSource.data.length; i++) {
            this.dataSource.data[i].letra = this.obtenerLetra(i + 1);
          }
          this.dataSource._updateChangeSubscription();
        }
      });
  }

  async registrarPregunta(pregunta: PreguntaRequest) {
    debugger;
    await this.preguntasRespuestasService.registrarPregunta(pregunta).then(
      (wsResponse: any) => {

        console.log(wsResponse);
        if (wsResponse.codResultado == 1) {
          console.log(wsResponse.msgResultado);
        } else {
          console.log(wsResponse.msgResultado);
        }
      },
      error => {
        console.error(error);
      }
    );
  }

  async listarRespuestas(codPregunta: number) {
    await this.preguntasRespuestasService.obtenerRespuestas(codPregunta)
      .then(
        (wsResponseProyecto: any) => {

          if (wsResponseProyecto.codResultado == 1) {
            this.respuestaResponse =
              (wsResponseProyecto.response != null) ? wsResponseProyecto.response : [];
            this.cargarTablaRespuestas();
            console.log(this.respuestaResponse);
          } else {
            this.mensaje = MENSAJES.ERROR_NOFUNCION;

          }

        },
        error => {

          this.mensaje = MENSAJES.ERROR_SERVICIO;

        }

      );

    this.cargarTablaRespuestas();
  }

  public cargarTablaRespuestas(): void {
    if (this.respuestaResponse != null && this.respuestaResponse.length > 0) {
      this.dataSource = new MatTableDataSource(this.respuestaResponse);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.cantRespuestas = this.respuestaResponse.length;
    }
  }


  cerrarFormulario() {
    if (this.pregunta.flagEditar != 1) {
      this.openDialogMensajeConfirm(MENSAJES.PREGUNTA.BOTON_SALIR_GUARDAR, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(() => {
          this.dialogRef.close(true);

        });
    } else {
      this.openDialogMensajeConfirm(MENSAJES.PREGUNTA.BOTON_SALIR_MODIFICAR, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(() => {
          this.dialogRef.close(true);

        });
    }
  }

  public openDialogMensajeConfirm(message: string, confirmacion: boolean): void {

    this.dialogRefMessage = this.dialog.open(InfoMessageComponent, {
      //width: '400px',
      disableClose: true,
      data: { message: message, confirmacion: confirmacion }
    });
  }



  public openDialogMensaje(message: string, alerta: boolean, confirmacion: boolean, valor: any
  ): void {
    const dialogRef = this.dialog.open(InfoMessageComponent, {
      disableClose: true,
      data: {
        message: message,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe((ok: number) => {
      if (ok == 0) {

      }
    });
  }

  cargarCombos() {
    debugger;
    this.itemService.listadoTema().subscribe(data => {
      this.listTema = data.response
      console.log('tema:' + this.listTema)
    });
  }


  obtenerLetra(cont: number): string {
    if (cont == 1) {
      return "a";
    } else if (cont == 2) {
      return "b";
    } else if (cont == 3) {
      return "c";
    } else if (cont == 4) {
      return "d";
    } else if (cont == 5) {
      return "e";
    } else if (cont == 6) {
      return "f";
    } else if (cont == 7) {
      return "g";
    } else if (cont == 8) {
      return "h";
    } else if (cont == 9) {
      return "i";
    } else if (cont == 10) {
      return "j";
    } else if (cont == 11) {
      return "k";
    } else if (cont == 12) {
      return "l";
    } else if (cont == 13) {
      return "m";
    } else if (cont == 14) {
      return "n";
    } else if (cont == 15) {
      return "o)";
    } else if (cont == 16) {
      return "p)";
    } else if (cont == 17) {
      return "q)";
    } else if (cont == 18) {
      return "r)";
    } else if (cont == 19) {
      return "s)";
    } else if (cont == 20) {
      return "t)";
    }

    return "No letra";




  }

}

interface Datapregunta {
  dataPregunta?: any
}
