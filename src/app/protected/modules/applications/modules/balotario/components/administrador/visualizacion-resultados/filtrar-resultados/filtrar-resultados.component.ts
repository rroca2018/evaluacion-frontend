import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialogRef, MatDialog, MatSelectionList, MatListOption, MAT_DIALOG_DATA, throwMatDialogContentAlreadyAttachedError } from '@angular/material';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MENSAJES } from 'app/common';
import { filter } from 'rxjs/operators';
import { InfoMessageComponent } from '@shared/components/info-message/info-message.component';
import { ItemService } from '../../../../service/item.service';
import { FiltroRequest } from '../../../../dto/request/FiltroRequest';
import { SelectionModel } from '@angular/cdk/collections';
import { ItemBean } from '../../../../dto/ItemBean';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-filtrar-resultados',
  templateUrl: './filtrar-resultados.component.html',
  styleUrls: ['./filtrar-resultados.component.scss']
})
export class FiltrarResultadosComponent implements OnInit {

  @ViewChild(MatSelectionList) arrayCategoriaSeleccionada: MatSelectionList;

  @ViewChild(MatSelectionList) columna3: MatSelectionList;

  @ViewChild(MatSelectionList) columna2: MatSelectionList;

  filtrosForm: FormGroup;
  filtrosBalForm: FormGroup;
  dialogRefMessage: MatDialogRef<any>;
  dialogPrincipal: MatDialogRef<FiltrarResultadosComponent>;
  listYears: any[];
  listCategoria: any[];
  listArea: any[];
  listLocal: any[];
  listBalotarioCombo: ItemBean[];

  ngModelBalotarioCombo: ItemBean;

  filtro: FiltroRequest = new FiltroRequest();


  activo1: boolean = true;
  activo2: boolean = true;
  activo3: boolean = false;

  stringSelect: string = "Mayorsa";
  categoriaDato: number = 2;

  opcionSeleccionado: string = '0';
  verSeleccion: string = '';

  patientCategory: FormGroup;

  constructor(
    private fb: FormBuilder,
    private formBuilder: FormBuilder,
    private formBuilder2: FormBuilder,
    public dialogRef: MatDialogRef<FiltrarResultadosComponent>,
    private itemService: ItemService,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA)
    public datos: any,
  ) { }
  yearNombre: string;
  categoria: Array<number>;
  ngOnInit() {

    console.log(this.datos);
    /*  if(this.datos.filtro.idCategoria){
       this.filtro = new FiltroRequest();

       //this.filtro.categoria.;
      // this.filtro.categoria = [this.datos.filtro.idCategoria];
       console.log("filtro data: "+this.filtro.categoria)
     //this.filtro.categoria[0] = this.datos.filtro.idCategoria;
     } */
    this.formulario();
    this.formularioFiltros();
    this.cargarCombos();


    this.filtro.flagMenuSeleccionado =true;
    console.log("Filtro flag: " + this.datos.filtro.flagMenuSeleccionado)

    if (typeof this.datos !== 'undefined') {

      if (typeof this.datos.filtro !== 'undefined') {

        if (typeof this.datos.filtro.idArea !== 'undefined')
          if (this.datos.filtro.idArea.length > 0) {
            console.log("Filtro desde bandeja  AREA: " + this.datos.filtro.idArea[0]);
          }

        if (typeof this.datos.filtro.idCategoria !== 'undefined')
          console.log("Filtro desde bandeja CATEGORIA: " + this.datos.filtro.idCategoria[0]);

        if (typeof this.datos.filtro.idLocal !== 'undefined')
          console.log("Filtro desde bandeja LOCAL: " + this.datos.filtro.idLocal[0]);


      }

    }

    // console.log("Filtro desde bandeja: "+this.datos.dataFiltro.area);


    this.filtro.idYear = 1;

    if (this.datos.filtro.idCategoria) {
      console.log(this.datos.filtro.idCategoria[0])
      this.categoria = this.datos.filtro.idCategoria[0]
    }

  }


  public obtenerCodBalotario(codYear: number): number {
    this.itemService.comboBalotarioYear(codYear).subscribe(data => {
      this.listBalotarioCombo = data.response;
      console.log(this.listBalotarioCombo);

    });
    return this.listBalotarioCombo[0].idCodigo
  }

  formularioFiltros() {
    this.filtrosBalForm = this.formBuilder2.group({
      categoriaFrm: [{ value: '' }],
    });
  }


  formulario() {
    this.filtrosForm = this.formBuilder.group({
      año: [{ value: new Date(), disabled: true }],
      balotario: [{ value: new Date(), disabled: true }]
    });
  }
  yearCodigo: number;
  cargarCombos() {

    this.itemService.listadoCategoria().subscribe(data => {
      this.listCategoria = data.response
      console.log("lista de categorias -> " + this.listCategoria)
      if (this.filtro.flagMenuSeleccionado == true) {
        this.filtro.flagMenuSeleccionado = false;
        if (typeof this.datos.filtro !== 'undefined' && typeof (this.datos.filtro.idCategoria) !== 'undefined') {
          if (this.datos.filtro.idCategoria.length > 0) {
            this.filtro.flagMenuSeleccionado = true;
            this.filtro.idCategoria = this.datos.filtro.idCategoria;
            this.buscarArea(this.filtro.idCategoria[0], this.datos.filtro.catNombreLabel);

          } else {
            /* alert("LA CATEGORIA AÚN NO HA SIDO SELECCIONADA"); */
          }
        }

      }
    });
    this.itemService.listarYears().subscribe(data => {

      this.listYears = data.response;
      this.filtro.idYear =this.listYears[0].idCodigo;
      this.buscarBalotarioYear(this.listYears[0].cidNombre);
    });
  }

  cargarListaCategoria() {
    this.itemService.listadoCategoria().subscribe(data => {
      this.listCategoria = data.response
    });
  }

  buscarBalotarioYear(balotarioYear: number) {
    this.itemService.comboBalotarioYear(balotarioYear).subscribe(data => {
      this.listBalotarioCombo = data.response;
      let i: number;
      if (this.listBalotarioCombo.length > 0) {

        this.ngModelBalotarioCombo = this.listBalotarioCombo[0];
        for(i=0; i < this.listBalotarioCombo.length;i++){
          if(this.listBalotarioCombo[i].idCodigo==this.datos.filtro.idBalotario)
          this.ngModelBalotarioCombo = this.listBalotarioCombo[i];
        }


      }
      console.log("Balotario ngmodel :" + this.ngModelBalotarioCombo);
      console.log(this.listBalotarioCombo);
      if (this.filtro.idYear == 1)
        this.filtro.idBalotario = this.listBalotarioCombo[0].idCodigo;

    });
  }

  catNombreLabel: string;
  areaNombreLabel: string;
  localNombreLabel: string;

  buscarArea(idCategoria: number, catNombre: string) {
    if (typeof (this.filtro.idCategoria[0]) === 'undefined') {
      this.listArea = [];
      this.stringSelect = "MAYORSA"
    }
    else {
      this.catNombreLabel = catNombre;
      this.stringSelect = "MAYORSA" + " > " + this.catNombreLabel;
      this.itemService.listadoArea(idCategoria).subscribe(data => {
        this.listArea = data.response
        if (data.response.length > 0 && this.listArea != null && this.listArea != []) {
          this.listArea = data.response
          this.activo1 = true;
          this.activo2 = true;
          this.activo3 = false;
          if (this.filtro.flagMenuSeleccionado == true) {
            this.filtro.flagMenuSeleccionado = false;
            if (typeof this.datos.filtro !== 'undefined' && typeof (this.datos.filtro.idArea) !== 'undefined') {
              if (this.datos.filtro.idCategoria.length > 0 && this.datos.filtro.idArea.length > 0) {
                this.filtro.flagMenuSeleccionado = true;
                this.filtro.idArea = this.datos.filtro.idArea;
                this.buscarlocal(this.filtro.idCategoria[0], this.filtro.idArea[0], this.datos.filtro.areaNombreLabel);

              } else {
               /*  alert("LA ÁREA AÚN NO HA SIDO SELECCIONADA"); */
              }

            }
          }
        } else {
          this.listArea = [];
        }
      });
    }
  }

  cargarListaArea(idCategoria: number) {
    this.itemService.listadoArea(idCategoria).subscribe(data => {
      this.listArea = data.response
      if (data.response.length > 0 && this.listArea != null && this.listArea != []) {
        this.listArea = data.response
      }
    });
  }


  buscarlocal(idCategoria: number, idArea: number, nombreArea: string) {
    if (typeof (this.filtro.idArea[0]) === 'undefined') {
      this.listLocal = [];
      this.stringSelect = "MAYORSA" + " > " + this.catNombreLabel;
    }
    else {
      this.areaNombreLabel = nombreArea;
      this.stringSelect = "MAYORSA" + " > " + this.catNombreLabel;
      this.stringSelect = "MAYORSA" + " > " + this.catNombreLabel + " > " + this.areaNombreLabel;
      this.itemService.listadoLocal(idCategoria, idArea).subscribe(data => {
        if (data.response.length > 0) {
          this.listLocal = data.response
          this.activo1 = false;
          this.activo2 = true;
          this.activo3 = true;
          if (this.filtro.flagMenuSeleccionado == true) {
            this.filtro.flagMenuSeleccionado = false;
            if (typeof this.datos.filtro !== 'undefined' ||typeof this.datos.filtro.idCategoria !== 'undefined' ) {
              if (this.datos.filtro.idCategoria.length > 0 && this.datos.filtro.idArea.length > 0 && this.datos.filtro.idLocal.length > 0) {
                this.filtro.idLocal = this.datos.filtro.idLocal;
                this.localNombreLabel = this.datos.filtro.localNombreLabel;
                this.stringSelect = "MAYORSA" + " > " + this.catNombreLabel + " > " + this.areaNombreLabel + " > " + this.localNombreLabel;
              } else {
               /*  alert("EL LOCAL AÚN NO HA SIDO SELECCIONADA"); */
              }
            }
          }
        } else {
          this.listLocal = [];
        }
      });
    }
  }

  cargarListaLocal(idCategoria: number, idArea: number) {
    this.itemService.listadoLocal(idCategoria, idArea).subscribe(data => {
      if (data.response.length > 0)
        this.listLocal = data.response;
    });

  }

  elegirLocal(nombreLocal: string) {
    this.localNombreLabel = nombreLocal;
    if (typeof (this.filtro.idLocal[0]) === 'undefined') {
      this.stringSelect = "MAYORSA" + " > " + this.catNombreLabel + " > " + this.areaNombreLabel;
      console.log("AAAA")
    }
    else {
      this.stringSelect = "MAYORSA" + " > " + this.catNombreLabel + " > " + this.areaNombreLabel + " > " + this.localNombreLabel;
      console.log("BBB")
    }
  }

  atras() {
    console.log(this.filtro)
    if (this.activo1 == false && this.activo2 == true) {
      console.log(this.filtro.idLocal)
      if (this.filtro.idLocal == null || typeof (this.filtro.idLocal) === 'undefined' || typeof (this.filtro.idLocal[0]) === 'undefined') {
        this.activo1 = true;
        this.activo2 = true;
        this.activo3 = false;
        this.listLocal = [];
        this.cargarListaArea(this.filtro.idCategoria[0]);

        this.stringSelect = "MAYORSA" + " > " + this.catNombreLabel;

      }
      else {

        this.cargarListaLocal(this.filtro.idCategoria[0], this.filtro.idArea[0]);
        this.stringSelect = "MAYORSA" + " > " + this.catNombreLabel + " > " + this.areaNombreLabel;
      }

    }
    else if (this.activo1 == true && this.activo2 == true) {
      if (this.filtro.idArea == null || typeof (this.filtro.idArea) === 'undefined' || typeof (this.filtro.idArea[0]) === 'undefined') {
        this.listCategoria = [];
        this.listArea = [];
        this.cargarListaCategoria();
        this.stringSelect = "MAYORSA";
      }
      else if (this.filtro.idArea && this.filtro.idArea != null) {
        this.listArea = [];
        this.stringSelect = "MAYORSA";
      }
    }
  }

  cerrarFiltro() {
    this.dialogRef.close(false);

  }

  seleccionar() {

    console.log(this.ngModelBalotarioCombo);
    if (typeof this.ngModelBalotarioCombo !== 'undefined') {
      this.filtro.idBalotario = this.ngModelBalotarioCombo.idCodigo;
      this.filtro.descripcionBalotario = this.ngModelBalotarioCombo.cidNombre;
    }
    this.filtro.catNombreLabel = this.catNombreLabel;
    this.filtro.areaNombreLabel = this.areaNombreLabel;
    this.filtro.localNombreLabel = this.localNombreLabel;

    this.dialogRef.close(this.filtro);
    console.log(this.filtro)

  }

  public openDialogMensajeConfirm(message: string, confirmacion: boolean): void {
    this.dialogRefMessage = this.dialog.open(InfoMessageComponent, {
      //width: '400px',
      disableClose: false,
      data: { message: message, confirmacion: confirmacion }
    });
  }

  handleSelection(event, arrayCategoria) {
    let seleccionado: any;
    if (event.option) {
      arrayCategoria.selectedOptions.selected.forEach(element => {
        if (element._hasFocus) {
          seleccionado = element.value;
          return true;
        }
      });
      event.option.selectionList.options._results.forEach(element => {
        if (element.value != seleccionado) {
          element.selected = false;
        }
      });
    }
    /*
      else {
        event.source.selectionList.options.toArray().forEach(element => {

          if (element.value.cidNombre == arrayCategoria.cidNombre) {
            element.selected = true;
          }
        });
      }*/
  }
}

interface ComboAnio {
  value: string;
  viewValue: string;
}


interface DataFiltro {
  filtro?: any;
}

interface Year {
  idCodigo?: number;
  cidNombre?: string;
}

interface Categoria {
  idCodigo?: number;
  cidNombre?: string;
}
