import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Cuestionario } from '../../../dto/Cuestionario';

@Component({
  selector: 'app-calificacion',
  templateUrl: './calificacion.component.html',
  styleUrls: ['./calificacion.component.scss']
})
export class CalificacionComponent implements OnInit {

  @Input() resultCalificacion: number;
  @Input() cantidadIntento: number;
  @Input() numeroIntento: number;
  @Input() resultPorcentaje: number;

  @Input() resultPreguntaCorrecta: number;
  @Input() resultPreguntaTotal: number;
  @Input() calAprobatoria: number;
  

  @Output() intenta = new EventEmitter();
  @Output() finaliza = new EventEmitter();

  CresultCalificacion: number;
  CcantidadIntento: number;
  CnumeroIntento: number;

  CresultPorcentaje: number;

  CpreguntasCorrecta: number;
  CpreguntasTotal: number;

  CnotaAprobatoria: number;

  cantidad: number;
  ocultar: boolean = false;

  aprobado: boolean = true;

  constructor() { }

  ngOnInit() {
    debugger;
    console.log(Cuestionario.identityResultPorcentaje);
    console.log(this.CresultPorcentaje);
    if (Cuestionario.identityCalification == true) {
      (Cuestionario.identityResultCalificacion) ? this.CresultCalificacion = Cuestionario.identityResultCalificacion : this.CresultCalificacion = this.resultCalificacion;
      (Cuestionario.identityTotalAttempt) ? this.CcantidadIntento = Cuestionario.identityTotalAttempt : this.CcantidadIntento = this.cantidadIntento;
      (Cuestionario.identityResultPorcentaje) ? this.CresultPorcentaje = Cuestionario.identityResultPorcentaje : this.CresultPorcentaje = this.resultPorcentaje;
      (localStorage.getItem('#nI')) ? this.CnumeroIntento = +localStorage.getItem('#nI') : this.CnumeroIntento = this.numeroIntento;

    
      this.CpreguntasCorrecta = this.resultPreguntaCorrecta;
      this.CpreguntasTotal = this.resultPreguntaTotal;
      
      debugger;
      this.CnotaAprobatoria = this.calAprobatoria;

      if(this.CresultPorcentaje >=this.CnotaAprobatoria){
          this.aprobado = true;
      }else{
          this.aprobado = false;
      }
     
     /* if (Cuestionario.detenerCuestionario == 2) {
        // localStorage.setItem('dt', (true).toString());
        this.ocultar = true;
      } else {
        // localStorage.setItem('dt', (false).toString());
      }*/
      
      if (this.CnumeroIntento == this.CcantidadIntento) {
        this.ocultar = true;
      }

    }
  }

  intentarNuevo() {
    this.cantidad = +localStorage.getItem('#nI');
    localStorage.setItem('#nI', (this.cantidad + 1).toString());
    this.intenta.emit(null);
  }

  finalizar() {
    this.finaliza.emit();
  }



}

