import { Component, OnInit } from '@angular/core';
import { BalotarioService } from '../../service/balotario.service';
import { WsResponse } from '../../dto/WsResponse';
import { preguntaResponse } from '../../dto/preguntaResponse';
import { Cuestionario } from '../../dto/Cuestionario';
import { Session } from '@shared/auth/Session';
import { AuthService } from 'app/protected/services/auth.service';
import { Resultado } from '../../dto/Resultado';
import { ResultadoComponent } from '../resultado/resultado.component';
import { ResultadoDetalle } from '../../dto/ResultadoDetalle';
import { Router } from '@angular/router';

declare const startCountDown: any;

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.scss'],
  providers: [BalotarioService]
})
export class PersonaComponent implements OnInit {



  numeroPregunta: number = 1;
  cantidadPregunta: number = 0;
  totalPregunta: number;
  porcentaje: number;
  bloqueo: boolean = true;

  pregunta: preguntaResponse[];
  iteracion: number = 0;
  estadoCorrecto: boolean;
  idPregunta: number;
  idRespuesta: number;

  resultado: any[] = [];
  calificacion: boolean = (Cuestionario.identityCalification == true) ? true : false;

  cronometro: boolean;

  contadorPreguntaCorrecta: number = 0;
  contadorPreguntaIncorrecta: number = 0;

  resultCalificacion: number;
  resultPorcentaje: number;
  numeroIntento: number;
  cantidadIntento: number;
  tiempo: string;
  tituloBalotario: string;
  calAprobatoria: number;

  resultPreguntaCorrecta: number;
  resultPreguntaTotal: number;

  mostrarInicio: boolean = false;
  mostrarProgreso: boolean = false;

  tema: string;

  objResultado: Resultado;

  codBal: number;
  idUsuario: any;
  resultadoDetalle: ResultadoDetalle[];

  constructor(
    private balotarioService: BalotarioService,
    private authService: AuthService,
    private router: Router
  ) { 

    debugger;
    this.resultadoDetalle = new Array<ResultadoDetalle>();

  }

  ngOnInit() {
    debugger;
    console.log("LocalStorage -> " + localStorage);
    console.log("init storage -> " + localStorage.getItem('init'));

    if (localStorage.getItem('init') == 'true') {
      this.mostrarInicio = true;
      this.mostrarProgreso = true;
      this.iniciar(true);
    }
  }


  tituloAutorizacionGasto() {
    this.authService.cabecera.next({
      titulo: this.tituloBalotario,
      icono: ''
    });
  }

  ngOnDestroy() {
    this.authService.cabecera.next({
      titulo: '',
      icono: ''
    });
  }

  response(cuestionario) {
    this.idPregunta = cuestionario.idPregunta;
    this.idRespuesta = cuestionario.idRespuesta;
    this.estadoCorrecto = cuestionario.correcto;
    this.bloqueo = cuestionario.estado;
  }

  async intentaNuevo(intento) {
    Cuestionario.startIteration(Object.assign({}, Cuestionario.identityIteration, ''));
    Cuestionario.startQuestion(Object.assign({}, Cuestionario.identityNumberQuestion, ''));
    Cuestionario.startCalification(Object.assign({}, Cuestionario.identityCalification, ''));
    Cuestionario.startResultCalificacion(Object.assign({}, Cuestionario.identityResultCalificacion, ''));
    Cuestionario.startResultPorcentaje(Object.assign({}, Cuestionario.identityResultPorcentaje, ''));
    Cuestionario.startResultado(Object.assign({}, Cuestionario.identityResult, ''));
    Cuestionario.startReloaded(Object.assign({}, Cuestionario.identityReloaded, ''));
    await this.cargarBalotario(this.cantidadPregunta);
    this.bloqueo = true;
    this.calificacion = false;
    this.mostrarProgreso = true;
  }

  async finalizaCuestionario(finaliza) {
    Session.stop();
    this.router.navigate(['/anonimo/iniciar-sesion']);
   /* Cuestionario.startIteration(Object.assign({}, Cuestionario.identityIteration, ''));
    Cuestionario.startQuestion(Object.assign({}, Cuestionario.identityNumberQuestion, ''));
    Cuestionario.startCalification(Object.assign({}, Cuestionario.identityCalification, ''));
    Cuestionario.startResultCalificacion(Object.assign({}, Cuestionario.identityResultCalificacion, ''));
    Cuestionario.startResultPorcentaje(Object.assign({}, Cuestionario.identityResultPorcentaje, ''));
    Cuestionario.startResultado(Object.assign({}, Cuestionario.identityResult, ''));
    Cuestionario.startReloaded(Object.assign({}, Cuestionario.identityReloaded, ''));
    localStorage.setItem('#nI', (1).toString());
    await this.cargarBalotario(this.cantidadPregunta);
    localStorage.setItem('init', null);
    this.mostrarInicio = false;
    this.calificacion = false;
    this.bloqueo = true;*/
  }

  terminarTiempo(terminaTiempo) {
    this.calificacion = terminaTiempo;
    this.contadorPreguntaCorrecta = 0;
    this.contadorPreguntaIncorrecta = 0;
    this.resultado = [];
    this.resultadoDetalle = [];
    this.idUsuario = Session.identity.id_usuario;
    if (Cuestionario.identityResult.length != undefined) {
      Cuestionario.identityResult.forEach(element => {
        if (element.IdRespuesta == 1) {
          this.contadorPreguntaCorrecta = this.contadorPreguntaCorrecta + 1;
        } else {
          this.contadorPreguntaIncorrecta = this.contadorPreguntaIncorrecta + 1;
        }

        let resDetalle : ResultadoDetalle = new ResultadoDetalle();
          
        resDetalle.codEmpresa = 1;
        resDetalle.codBalotario = this.codBal;
        resDetalle.codUsuario = this.idUsuario;
        resDetalle.cantIntento = this.numeroIntento;
        resDetalle.codPregunta = element.IdPregunta;
        resDetalle.codRespuesta = element.IdRespSeleccionada;
        resDetalle.flagCorrecta = element.IdRespuesta;
        this.resultadoDetalle.push(resDetalle);


    

      });
    }
    this.resultCalificacion = (this.contadorPreguntaCorrecta) * 20 / (this.cantidadPregunta);
    this.resultPreguntaCorrecta = this.contadorPreguntaCorrecta;
    this.resultPreguntaTotal = this.cantidadPregunta;
    this.resultPorcentaje = (this.contadorPreguntaCorrecta / this.cantidadPregunta)*100 ;
    localStorage.setItem('#nI', (this.numeroIntento).toString());
    Cuestionario.startCalification(true);
    Cuestionario.startResultCalificacion((this.contadorPreguntaCorrecta) * 20 / (this.cantidadPregunta));
    Cuestionario.startResultPorcentaje((this.contadorPreguntaCorrecta / this.cantidadPregunta)*100);

      this.objResultado = new Resultado();
      this.objResultado.codEmpresa = 1;
      this.objResultado.codBalotario = this.codBal;
      this.objResultado.codUsuario = this.idUsuario;
      this.objResultado.cantIntento = this.numeroIntento;
      this.objResultado.nota = this.resultCalificacion;
  
      let i: number;
      for(i = this.resultadoDetalle.length; i< this.cantidadPregunta;i++) {
        let resDetalle : ResultadoDetalle = new ResultadoDetalle();
        resDetalle.codEmpresa = 1;
        resDetalle.codBalotario = this.codBal;
        resDetalle.codUsuario = this.idUsuario;
        resDetalle.cantIntento = this.numeroIntento;
        resDetalle.codPregunta = this.pregunta[i].idPregunta;
        resDetalle.codRespuesta = 99;
        resDetalle.flagCorrecta = "0";
        this.resultadoDetalle.push(resDetalle);
      }
      this.objResultado.lstResultadoDetalle = this.resultadoDetalle;
      console.log(this.objResultado);
      this.registrarResultado(this.objResultado);
      this.mostrarProgreso = false;
  }


  iniciar(mostrar) {
    console.log("PERSONA-COMPONENT iniciar(mostrar)-> " + mostrar);
    this.mostrarInicio = mostrar;
    this.mostrarProgreso = mostrar;
    debugger;
    this.codBal  =  Number(localStorage.getItem('cod_bal'));
    this.obtenerBalotario(this.codBal);
    this.cargarBalotario(this.cantidadPregunta);
   
  
  }

  public obtenerBalotario(codBalotario): void{
    debugger;
    this.balotarioService.obtenerBalotario(codBalotario).subscribe(
      (wsResponse: any) => {
        if (wsResponse.codResultado == 1) {
          this.tituloBalotario = wsResponse.response.nomDescripcion;
          this.cantidadIntento = wsResponse.response.cantIntento;     
          this.numeroIntento = wsResponse.response.cantIntentoDisponible;
          this.tiempo = wsResponse.response.cantTiempo;
          this.calAprobatoria = wsResponse.response.calAprobatoria;
          this.tituloAutorizacionGasto();
        } else { 
          console.log("Sin resultados");
        }
      },
      error => {
        console.error(error);
      }
    );
  }

  public cargarBalotario(cantidadPregunta): void {
    this.codBal  =  Number(localStorage.getItem('cod_bal'));
    if (Cuestionario.identityReloaded && Cuestionario.identityReloaded !== false) {
      this.balotarioService.obtenerListadoPreguntaRespuesta(cantidadPregunta,this.codBal).subscribe(
        (WsResponse: WsResponse) => {
          if (WsResponse.codResultado == 1) {

            this.cronometro = true;
            localStorage.setItem('chr', (this.cronometro).toString());
            this.totalPregunta = (WsResponse.total != 0) ? WsResponse.total : 0;
            this.cantidadPregunta = this.totalPregunta;
            this.porcentaje = 100 / this.totalPregunta;
            this.pregunta = (WsResponse.response != null) ? WsResponse.response : [];
            /*this.pregunta.forEach(function (value){
              console.log(value);
            })*/
        
            this.tema = this.pregunta[0].nombreTema;
            this.iteracion = 0;
            this.numeroPregunta = 1;
            this.numeroIntento = +((localStorage.getItem('#nI') == null) ?  this.numeroIntento : localStorage.getItem('#nI'));
            Cuestionario.start(this.pregunta, this.cantidadIntento);
            Cuestionario.startReloaded(true);

            startCountDown(this.tiempo);
          } else { }
        },
        error => {
          console.error(error);
        }
      );

    } else {

      this.cronometro = true;
      this.iteracion = Cuestionario.identityIteration;
      this.pregunta = Cuestionario.identityQuestion;
      this.totalPregunta = Cuestionario.identityQuestion.length;
      this.cantidadPregunta = this.totalPregunta;
      this.numeroPregunta = Cuestionario.identityNumberQuestion;
      this.porcentaje = this.numeroPregunta * 100 / this.totalPregunta;
      this.numeroIntento = +((localStorage.getItem('#nI') == null) ?  this.numeroIntento : localStorage.getItem('#nI'));
      if (Cuestionario.identityCalification == true) {
        this.calificacion = true;
      }
    }
   

  }

  continuar() {

    if (this.numeroPregunta <= this.totalPregunta) {
      if (Cuestionario.identityResult.length == undefined) {
        this.resultado.push({
          IdPregunta: this.idPregunta,
          IdRespSeleccionada: this.idRespuesta,
          IdRespuesta: ((this.estadoCorrecto) ? '1' : '0')
        });
      } else {
        this.resultado = Cuestionario.identityResult;
        this.resultado.push({
          IdPregunta: this.idPregunta,
          IdRespSeleccionada: this.idRespuesta,
          IdRespuesta: ((this.estadoCorrecto) ? '1' : '0')
        });
      }
      Cuestionario.startResultado(this.resultado);
      // Finalizar !!!
      if (this.numeroPregunta == this.totalPregunta) {
        this.idUsuario = Session.identity.id_usuario;
        this.calificacion = true;
        this.contadorPreguntaCorrecta = 0;
        this.contadorPreguntaIncorrecta = 0;
        this.resultado = [];
        this.resultadoDetalle = [];
        Cuestionario.identityResult.forEach(element => {
          if (element.IdRespuesta == 1) {
            this.contadorPreguntaCorrecta = this.contadorPreguntaCorrecta + 1;
          } else {
            this.contadorPreguntaIncorrecta = this.contadorPreguntaIncorrecta + 1;
          }

          let resDetalle : ResultadoDetalle = new ResultadoDetalle();
          
          resDetalle.codEmpresa = 1;
          resDetalle.codBalotario = this.codBal;
          resDetalle.codUsuario = this.idUsuario;
          resDetalle.cantIntento = this.numeroIntento;
          resDetalle.codPregunta = element.IdPregunta;
          resDetalle.codRespuesta = element.IdRespSeleccionada;
          resDetalle.flagCorrecta = element.IdRespuesta;
          this.resultadoDetalle.push(resDetalle);
        });
        debugger;
        //this.resultCalificacion = (this.contadorPreguntaCorrecta) * 20 / (this.contadorPreguntaCorrecta + this.contadorPreguntaIncorrecta);
        this.resultCalificacion = (this.contadorPreguntaCorrecta) * 20 / (this.cantidadPregunta);
        this.resultPorcentaje = (this.contadorPreguntaCorrecta / this.cantidadPregunta)*100 ;
        localStorage.setItem('#nI', (this.numeroIntento).toString());
        Cuestionario.startCalification(true);
        //Cuestionario.startResultCalificacion((this.contadorPreguntaCorrecta) * 20 / (this.contadorPreguntaCorrecta + this.contadorPreguntaIncorrecta));
        Cuestionario.startResultCalificacion((this.contadorPreguntaCorrecta) * 20 / (this.cantidadPregunta));
        Cuestionario.startResultPorcentaje((this.contadorPreguntaCorrecta / this.cantidadPregunta)*100);

      
        this.resultPreguntaCorrecta = this.contadorPreguntaCorrecta;
        this.resultPreguntaTotal = this.cantidadPregunta;

        this.mostrarProgreso = false;

        this.objResultado = new Resultado();
        this.objResultado.codEmpresa = 1;
        this.objResultado.codBalotario = this.codBal;
        this.objResultado.codUsuario = this.idUsuario;
        this.objResultado.cantIntento = this.numeroIntento;
        this.objResultado.nota = this.resultPorcentaje;
        this.objResultado.lstResultadoDetalle = this.resultadoDetalle;
        console.log(this.objResultado);
        this.registrarResultado(this.objResultado);
      }
    }

    if (this.numeroPregunta < this.totalPregunta) {
      this.bloqueo = true;
      this.numeroPregunta = this.numeroPregunta + 1;
      this.iteracion = this.iteracion + 1;
      Cuestionario.startReloaded(false);
      Cuestionario.startIteration(this.iteracion);
      Cuestionario.startQuestion(this.numeroPregunta);
      this.porcentaje = this.numeroPregunta * 100 / this.totalPregunta;
    }

  }

  
    registrarResultado(res: Resultado):void{
      this.balotarioService.registrarResultado(res).subscribe(
      (wsResponse: any) => {

        console.log(wsResponse);
        if (wsResponse.codResultado == 1) {
          console.log(wsResponse.msgResultado);
        } else { 
          console.log(wsResponse.msgResultado);
         }
      },
        error => {
          console.error(error);
        }
      );
    }

}
