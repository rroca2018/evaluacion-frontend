import { Component, OnInit } from '@angular/core';
import { CargaMasivaPreguntasComponent } from './carga-masiva-preguntas/carga-masiva-preguntas.component';
import { MatDialogRef, MatDialog } from '@angular/material';
import { PreguntasRespuestasService } from '../../../../service/preguntasRespuestas.service';

@Component({
  selector: 'app-plantilla-excel-preguntas',
  templateUrl: './plantilla-excel-preguntas.component.html',
  styleUrls: ['./plantilla-excel-preguntas.component.scss']
})
export class PlantillaExcelPreguntasComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<PlantillaExcelPreguntasComponent>,
    private dialog: MatDialog,
    private preguntasRespuestasService: PreguntasRespuestasService,
  ) { 

  }

  ngOnInit() {
  }

  descargarPlantillaExcel(): void{
    let filename = "Plantilla_Carga_Preguntas_y_Respuestas.xls";
    this.preguntasRespuestasService.downloadFile()
    .subscribe(data => {
      //Guarda en la pc del cliente.
      saveAs(new Blob([data], {type: MimeType['xls']}),filename);
    })
  
  }




  importarExcel(): void{
    const dialogReg: MatDialogRef<CargaMasivaPreguntasComponent> = this.dialog.open(CargaMasivaPreguntasComponent, {
      disableClose: true,
      panelClass: 'dialog-no-padding',
      width: '40%',
      data: {
        
      },
     
    });

    dialogReg.afterClosed().subscribe((valor: any) => {     
      debugger;
   
      if(valor == 0) {
        this.dialogRef.close(valor);
      }     
    
    });

  }
 

}
