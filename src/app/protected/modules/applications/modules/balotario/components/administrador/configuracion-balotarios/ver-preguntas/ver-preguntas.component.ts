import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { BalotarioRequest } from '../../../../dto/request/balotarioRequest';
import { MENSAJES } from 'app/common';
import { MatDialogRef, MatSnackBar, MatDialog, MatTableDataSource, MAT_DIALOG_DATA, MatPaginator, MatSort } from '@angular/material';
import { filter } from 'rxjs/operators';
import { InfoMessageComponent } from '@shared/components/info-message/info-message.component';
import { Proyecto } from '../../../../dto/response/Proyecto';
import { BalotarioService } from '../../../../service/balotario.service';

@Component({
  selector: 'app-ver-preguntas',
  templateUrl: './ver-preguntas.component.html',
  styleUrls: ['./ver-preguntas.component.scss']
})
export class VerPreguntasComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  


  tituloModalUsuario: string;
  columnas: string[] = [];

  temaPreguntaResponse: any[];
  mensaje: string;

  codBalotario: number;
  descBalotario: string;
  constructor(
    public dialogRef: MatDialogRef<VerPreguntasComponent>,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA)
    public dataBalotarioCodigo: DataBalotarioCodigo,
    private balotarioService: BalotarioService
  ) { 
    this.dataSource = new MatTableDataSource([]);
    this.dataSource.sort = this.sort;


  }

  ngOnInit() {
    this.tableColumnaPreguntas();
    console.log(this.dataBalotarioCodigo)
    this.dataSource = new MatTableDataSource();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    console.log( this.dataBalotarioCodigo.balotarioSeleccionado)
    let arregloPalabra = this.dataBalotarioCodigo.balotarioSeleccionado.split(",");
    this.codBalotario = parseInt(arregloPalabra[0]);
    this.descBalotario = arregloPalabra[1]


    this.listarTemasPorPregunta(this.codBalotario);

  }


  
  async listarTemasPorPregunta(codBal : number)  {
    await this.balotarioService.obtenerTemasPreguntas(codBal)
    .then(
      (wsResponseProyecto : any)=> {
        
        if(wsResponseProyecto.codResultado == 1){
          debugger;
           this.temaPreguntaResponse =  (wsResponseProyecto.response != null) ? wsResponseProyecto.response : [];       
           this.cargarTablaTemaPreguntas();
           console.log(this.temaPreguntaResponse)
        }else{
          this.mensaje = MENSAJES.ERROR_NOFUNCION;
         // this.openDialogMensaje(null,  wsResponseProyecto.msgResultado, true, false, wsResponseProyecto.codResultado);
    
        }
      
      },
      error => {
       
        this.mensaje = MENSAJES.ERROR_SERVICIO;
      /*   this.openDialogMensaje(this.mensaje, MENSAJES.PREFACTIBILIDAD.TITLE, true, false, null);
        console.error(error);
        this.disableBuscar = false; */
      }
    
    );    

   
  }


  cantTemaPregunta: number;
  public cargarTablaTemaPreguntas(): void {
    if (this.temaPreguntaResponse != null && this.temaPreguntaResponse.length > 0) {
      this.dataSource = new MatTableDataSource(this.temaPreguntaResponse);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      console.log("tabla -> " + this.dataSource);
      this.cantTemaPregunta = this.temaPreguntaResponse.length;
      console.log("balotarios -> " +  this.cantTemaPregunta);
    }
  
  }

  tableColumnaPreguntas(): void{
    this.columnas = ['nro','nomTema','nomPregunta'];
  }

  dialogRefMessage: MatDialogRef<any>;
  balotario: BalotarioRequest = new BalotarioRequest();

  cerrarFormulario() {
    this.dialogRef.close(true);
   /* if(this.balotario.flagEditar !=1){
    this.openDialogMensajeConfirm(MENSAJES.PERSONA.BOTON_SALIR_GUARDAR, true);

    this.dialogRefMessage.afterClosed()
      .pipe(filter(verdadero => !!verdadero))
      .subscribe(() => {
        this.dialogRef.close(true);

      });
    }else{
      this.openDialogMensajeConfirm(MENSAJES.PERSONA.BOTON_SALIR_MODIFICAR, true);

    this.dialogRefMessage.afterClosed()
      .pipe(filter(verdadero => !!verdadero))
      .subscribe(() => {
        this.dialogRef.close(true);

      });
    }*/
  }

  public openDialogMensajeConfirm(message: string, confirmacion: boolean): void {

    this.dialogRefMessage = this.dialog.open(InfoMessageComponent, {
      //width: '400px',
      disableClose: true,
      data: { message: message, confirmacion: confirmacion }
    });
  }

}


interface DataBalotarioCodigo {
  balotarioSeleccionado?: any
}

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}
