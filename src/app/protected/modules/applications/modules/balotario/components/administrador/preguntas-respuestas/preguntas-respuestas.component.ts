import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatSort, MatDialogRef, MatDialog } from '@angular/material';
import { PreguntasRespuestasService } from '../../../service/preguntasRespuestas.service';
import { MENSAJES } from 'app/common';
import { RegistrarActualizarPreguntasComponent } from './registrar-actualizar-preguntas/registrar-actualizar-preguntas.component';
import { PlantillaExcelPreguntasComponent } from './plantilla-excel-preguntas/plantilla-excel-preguntas.component';

@Component({
  selector: 'app-preguntas-respuestas',
  templateUrl: './preguntas-respuestas.component.html',
  styleUrls: ['./preguntas-respuestas.component.scss']
})
export class PreguntasRespuestasComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  filtrosForm: FormGroup;
  
  dataSource: MatTableDataSource<any>;
  columnas: string[] = [];
  preguntasRespuestasResponse: any;
  cantPreguntasRespuestas: number;
  mensaje: string;
  
  constructor(
    private dialog: MatDialog,
    private preguntasRespuestasService: PreguntasRespuestasService,
  ) { 
    this.dataSource = new MatTableDataSource([]);
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.crearFiltrosForm();
    this.tableColumnaPreguntasRespuestas();
    this.cargarTablaPreguntasRespuestas();
    this.listarPreguntasRespuestas();

  }

  crearFiltrosForm() {
    this.filtrosForm = new FormGroup({
      codDocFrmCtrl: new FormControl(null), 
    });
  }

  public cargarTablaPreguntasRespuestas(): void {
    if (this.preguntasRespuestasResponse != null && this.preguntasRespuestasResponse.length > 0) {
      this.dataSource = new MatTableDataSource(this.preguntasRespuestasResponse);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      console.log("tabla -> " + this.dataSource);
      this.cantPreguntasRespuestas = this.preguntasRespuestasResponse.length;
    }
  }



  tableColumnaPreguntasRespuestas(): void{
    this.columnas = ['descPregunta','descTema','descEstado','acciones'];
     
  }


  async listarPreguntasRespuestas()  {

    await this.preguntasRespuestasService.listarPreguntasRespuestas()
    .then(
      (wsResponsePreguntasRespuestas : any)=> {
        debugger;
        if(wsResponsePreguntasRespuestas.codResultado == 1){
           this.preguntasRespuestasResponse = (wsResponsePreguntasRespuestas.response != null) ? wsResponsePreguntasRespuestas.response : [];       
           this.cargarTablaPreguntasRespuestas();
        }else{
          this.mensaje = MENSAJES.ERROR_NOFUNCION;
         // this.openDialogMensaje(null,  wsResponseProyecto.msgResultado, true, false, wsResponseProyecto.codResultado);
    
        }
      
      },
      error => {
       
        this.mensaje = MENSAJES.ERROR_SERVICIO;
      /*   this.openDialogMensaje(this.mensaje, MENSAJES.PREFACTIBILIDAD.TITLE, true, false, null);
        console.error(error);
        this.disableBuscar = false; */
      }
    
    );    

    this.cargarTablaPreguntasRespuestas();
  }

  agregarPregunta(): void{
    const dialogReg: MatDialogRef<RegistrarActualizarPreguntasComponent> = this.dialog.open(RegistrarActualizarPreguntasComponent, {
      disableClose: true,
      panelClass: 'dialog-no-padding',
      width: '40%',
    
      data: { 
      }
    });

    dialogReg.afterClosed().subscribe((valor: any) => {     
      if(valor) {
        this.listarPreguntasRespuestas();
        this.cargarTablaPreguntasRespuestas();
      } 
    });

  }

  editarPregunta(coBalotario : number): void{
  debugger
    this.preguntasRespuestasService.obtenerPregunta(coBalotario).subscribe(dataPregunta => {

      const dialogReg: MatDialogRef<RegistrarActualizarPreguntasComponent> = this.dialog.open(RegistrarActualizarPreguntasComponent, {
        disableClose: true,
        panelClass: 'dialog-no-padding',
        width: '40%',   
        data: {
          dataPregunta    
        }
      });

      dialogReg.afterClosed().subscribe((valor: any) => {     
        console.log(valor)
        if(valor) {
          this.listarPreguntasRespuestas();
          this.txtBuscar = "";
        }     
      
      });
    });
  }

  descargarPlantillaExcel(): void{
    const dialogReg: MatDialogRef<PlantillaExcelPreguntasComponent> = this.dialog.open(PlantillaExcelPreguntasComponent, {
      disableClose: true,
      panelClass: 'dialog-no-padding',
      width: '40%',
      data: {
        
      },
     
    });

    dialogReg.afterClosed().subscribe((valor: any) => {     
      debugger;
      if(valor == 0) {
       this.listarPreguntasRespuestas();
      }     
    
    });

  }
  
  txtBuscar: string;
  onKeyup(){
    debugger;
    if(this.txtBuscar.trim().length>0){
      this.preguntasRespuestasService.buscarPregunta(this.txtBuscar)
      .subscribe(
        (wsResponsePreguntasRespuestas : any)=> {
          debugger;
          if(wsResponsePreguntasRespuestas.codResultado == 1){
            this.preguntasRespuestasResponse = (wsResponsePreguntasRespuestas.response != null) ? wsResponsePreguntasRespuestas.response : [];       
            this.cargarTablaPreguntasRespuestas();
          }else{
            this.mensaje = MENSAJES.ERROR_NOFUNCION;
          // this.openDialogMensaje(null,  wsResponseProyecto.msgResultado, true, false, wsResponseProyecto.codResultado);
      
          }
        
        },
        error => {
        
          this.mensaje = MENSAJES.ERROR_SERVICIO;
        /*   this.openDialogMensaje(this.mensaje, MENSAJES.PREFACTIBILIDAD.TITLE, true, false, null);
          console.error(error);
          this.disableBuscar = false; */
        }
      
      );    
    }else{
      this.listarPreguntasRespuestas();
    }
  }

}
