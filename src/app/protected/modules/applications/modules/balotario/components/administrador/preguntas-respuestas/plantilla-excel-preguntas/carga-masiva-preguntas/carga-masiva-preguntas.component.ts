import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { ExcelService } from '../../../../../service/excel.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MENSAJES } from 'app/common';
import { InfoMessageComponent } from '@shared/components/info-message/info-message.component';
import { filter } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { ArchivoRequest } from '../../../../../dto/request/ArchivoRequest';
import { PreguntasRespuestasService } from '../../../../../service/preguntasRespuestas.service';

@Component({
  selector: 'app-carga-masiva-preguntas',
  templateUrl: './carga-masiva-preguntas.component.html',
  styleUrls: ['./carga-masiva-preguntas.component.scss']
})
export class CargaMasivaPreguntasComponent implements OnInit {

  @ViewChild('myInput')
  myInputVariable: ElementRef;
  mensaje: any;
  fileUploadRes = { status: '', message: '', messageAux: '' };
  archivo: string;
  examinarFiles: boolean;
  archivoForm: FormGroup;
  
  selectedFiles: boolean;
  btnEliminar: boolean;
  fileUpload: File;
  fileUploadAux: File;
  dialogRefMessage: MatDialogRef<any>;
  
  constructor(
   
    public dialogRef: MatDialogRef<CargaMasivaPreguntasComponent>,
    private dialog: MatDialog,
    private excelService: ExcelService,
    private formBuilder: FormBuilder,
    private preguntasRespuestasService: PreguntasRespuestasService,
  ) { 
  
  }

  ngOnInit() {
   
    this.fileUploadRes.message = '20';
    this.archivo = 'Ningún archivo seleccionado'
    this.examinarFiles = false;
  }

  
  selectFile(event) {

    console.log(event);
    setTimeout(() => {
      this.selectedFiles = true;
    }, 800)// 1 segundo = 1000
    if( this.fileUpload == null){
      this.fileUpload = event.target.files[0];
      this.fileUploadAux =  this.fileUpload;
      this.archivo = event.target.files[0].name;
      
    }else{// Para controlar si ya existe un archivo seleccionado pero abre de nuevo el explorador y no selecciona ningún archivo.
      this.fileUpload = event.target.files[0];
      if(typeof this.fileUpload == "undefined" || this.fileUpload == null){
        this.fileUpload = this.fileUploadAux;
        this.archivo = this.fileUpload.name;
      }else{
        this.fileUpload = event.target.files[0];
        this.fileUploadAux =  this.fileUpload;
        this.archivo = event.target.files[0].name;
      }
    }
    
  }

  crearFormulario(): void {
    this.archivoForm = this.formBuilder.group({
      descArchivoFrmCtrl: [''],
      tipoDocArchivoFrmCtrl: ['']
    });
  }

  public openDialogMensajeConfirm(message: string, confirmacion: boolean): void {

    this.dialogRefMessage = this.dialog.open(InfoMessageComponent, {
      width: '400px',
      disableClose: true,
      data: { message: message, confirmacion: confirmacion }
    });
  }

  reset() {
    //Elimina el archivo seleccionado cuando se abre el explorador para elegir el archivo
    console.log(this.myInputVariable.nativeElement.files);
    this.myInputVariable.nativeElement.value = "";
    console.log(this.myInputVariable.nativeElement.files);
  }

  cargarExcelPresupuesto(event) {

    const archivoRequest: ArchivoRequest = new ArchivoRequest();
    archivoRequest.nomArchivo = this.fileUpload.name;
    archivoRequest.archivo = this.fileUpload;

    //this.proyectoService.cargarExcelPresupuesto(archivoRequest).pipe(delay(2000)).subscribe(
    this.preguntasRespuestasService.cargarExcel(archivoRequest).subscribe(
      (response: any) => {

        if (response.codResultado == 1) {
          console.log('carga exitosa');
          this.mensaje = MENSAJES.ARCHIVO_INFO_SUCCESS;
          this.openDialogMensajeExcelPrueba("", this.mensaje, true, false, null);
        
       
          this.reset();
          this.reiniciar();
          this.selectedFiles = false;
       //   this.examinarFiles = false;


        } else if (response.codResultado == 0) {
 
          debugger;
          console.log('No se encontro el dato del excel en la bd');
          console.log(response);

          console.log(response.response.value);

          //this.cargarExcelPartidaTxtError("listaErroresPresupuesto");
          if(response.response.nroColumna=='Z'){
            this.openDialogMensajeExcel(null, response.response.message , true, false, null);
         
          }else{
            this.openDialogMensajeExcel(null, response.response.message + " (Fila " + response.response.nroFila + ", " + "Columna " + response.response.nroColumna +")" , true, false, null);
        
          }
           //this.listadoArchivo();
          this.reset();
          this.reiniciar();
          this.selectedFiles = false;
        //  this.examinarFiles = false;

        } else if (response.codResultado == 2) {
          console.log('carga errorea');
          this.openDialogMensaje(null, response.msgResultado, true, false, null);
          this.reset();
          this.reiniciar();
          this.selectedFiles = false;
         // this.examinarFiles = false;

        }
 

      }, error => {

        this.mensaje = MENSAJES.ERROR_SERVICIO;
        this.openDialogMensaje(null, error, true, false, null);
        this.reset();
        this.reiniciar();
        console.error(error);
        this.selectedFiles = false;

      });

  }

  public openDialogMensaje(
    message: string,
    message2: string,
    alerta: boolean,
    confirmacion: boolean,
    valor: any
  ): void {
    const dialogRef = this.dialog.open(InfoMessageComponent, {
      width: '400px',
      disableClose: true,
      data: {
        title: MENSAJES.ARCHIVO_TITLE,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe((ok: number) => {
      if (ok == 0) {

      }
    });
  }


  public openDialogMensajeExcelPrueba(
    message: string,
    message2: string,
    alerta: boolean,
    confirmacion: boolean,
    valor: any
  ): void {
    const dialogRef = this.dialog.open(InfoMessageComponent, {
      width: '400px',
      disableClose: true,
      data: {
        title: MENSAJES.ARCHIVO_TITLE,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    }); debugger
    dialogRef.afterClosed().subscribe((ok: number) => {
      debugger;
     
      if (ok == 0) {
        this.dialogRef.close(ok);
      }
    });
   
  }

  public openDialogMensajeExcel(
    message: string,
    message2: string,
    alerta: boolean,
    confirmacion: boolean,
    valor: any
  ): void {
    const dialogRef = this.dialog.open(InfoMessageComponent, {
      width: '400px',
      disableClose: true,
      data: {
        title: MENSAJES.EXCEL_TITLE_ERROR,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe((ok: number) => {
      if (ok == 0) {

      }
    });
  }

  reiniciar() {
    this.archivo = 'Ningún archivo seleccionado';
  }

  cargarExcelPartidaTxtError(errorPartida: string) {
    this.excelService.cargarExcelPartidaTxtError(errorPartida);
  }

  cancelarCarga(){
    this.openDialogMensajeConfirm(MENSAJES.EXCEL_CANCELAR_CARGA, true);
    this.dialogRefMessage.afterClosed()
    .pipe(filter(verdadero => !!verdadero))
    .subscribe(() => {   
      this.dialogRef.close(true);
    });
  }
}
