import { Component, OnInit, ViewChild, Inject, ElementRef, Input } from '@angular/core';
import { MatDialogRef, MatSnackBar, MatDialog, MAT_DIALOG_DATA, MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { BalotarioRequest } from '../../../../dto/request/balotarioRequest';
import { MENSAJES } from 'app/common';
import { filter, debounceTime, switchMap, tap, finalize } from 'rxjs/operators';
import { InfoMessageComponent } from '@shared/components/info-message/info-message.component';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ItemService } from '../../../../service/item.service';
import { PersonaItemResponse } from '../../../../dto/response/PersonaItemResponse';
import { BalotarioService } from '../../../../service/balotario.service';
import { getLocaleDateFormat, DatePipe, AsyncPipe } from '@angular/common';
import { Proyecto } from '../../../../dto/response/Proyecto';
import { Persona } from '../../../../dto/response/PersonaResponse';
import { VerificarBalotarioRequest } from '../../../../dto/request/VerificarBalotarioRequest';
import { isNull } from 'util';
import { BalotarioPersona } from '../../../../dto/request/balotarioPersona';
import { BalotarioTema } from '../../../../dto/request/balotarioTema';
import { QtGlobalRequest } from '../../../../dto/request/QtGlobalRequest';
import { PersonaRequest } from '../../../../dto/request/PersonaRequest';
import * as _moment from 'moment';
import { ValidarExamenRealizadoRequest } from '../../../../dto/request/ValidarExamenRealizadoRequest';
import { MatOptionSelectionChange} from '@angular/material';


@Component({
  selector: 'app-registrar-actualizar-balotarios',
  templateUrl: './registrar-actualizar-balotarios.component.html',
  styleUrls: ['./registrar-actualizar-balotarios.component.scss']
})
export class RegistrarActualizarBalotariosComponent implements OnInit {
  balotarioForm: FormGroup;

  columnas2: string[] = [];
  columnas3: string[] = [];
  columnas4: string[] = [];
  textoButton: string;
  tituloModalBalotario: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('myInput')
  myInputVariable: ElementRef;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<BalotarioPersona>;
  dataSource2: MatTableDataSource<any>;
  dataSource3: MatTableDataSource<CantidadPreguntasData>;
  estado_modificar: boolean;
  activo: boolean;
  activo2: boolean;
  activo3:boolean = false;
  balotarioRealizado:boolean;
  balotarioResponse: any[];
  temaResponse: any[];
  temaAuxResponse: any[] = [];
  qtGlobalResponse: QtGlobalRequest[] = [];
  temasForm: FormGroup;
  cantBalotarios: number;
  cantTemas: number;
  cantPersonas: number;
  mensaje: string;
  //Vsriables Buscar Autocomplete Inicio
  filteredPersonas: PersonaItemResponse[] = [];
  personaItem: PersonaItemResponse;
  personasForm: FormGroup;
  isLoading = false;
  //Vsriables Buscar Autocomplete Fin

  examenRealizado:boolean;
  validar : ValidarExamenRealizadoRequest;
  objVerificarBalotarioRealizado: ValidarExamenRealizadoRequest;

  objBalotario: BalotarioRequest;
  objQtGlobal: QtGlobalRequest;
  balotarioPersonaArray: BalotarioPersona[] = [];
  balotarioTemaArray: BalotarioTema[] = [];

  dialogRefMessage: MatDialogRef<any>;
  balotario: BalotarioRequest = new BalotarioRequest();
  qtGlobal: QtGlobalRequest = new QtGlobalRequest();

  qtTemas: number;
  qtPreguntas: number;
  dataSource4: MatTableDataSource<QtGlobalRequest>;


  personaResponse: BalotarioPersona;
  personaResponse2: BalotarioPersona[];

  @Input() cantidadPreguntaTipeada = '';

  cantidadTotalPreguntas: number = 0;
  CantidadPreguntasModificado: boolean = false;

  listCargo: any[];
  listCategoria: any[];
  listArea: any[];
  listLocal: any[];
  listEstado: any[];

  constructor(
    private itemService: ItemService,
    public dialogRef: MatDialogRef<RegistrarActualizarBalotariosComponent>,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private balotarioService: BalotarioService,
    @Inject(MAT_DIALOG_DATA)
    public datos: Databalotario,
  ) {
    this.dataSource = new MatTableDataSource([]);
    this.dataSource2 = new MatTableDataSource([]);
    this.dataSource.sort = this.sort;
    
  }

  ngOnInit() {
    this.tableColumnaPersonas();
    this.tableColumnaTemas();
    this.tableColumnaTotal();

    this.cargarCombos();
    this.dataSource.sort = this.sort;

    this.obtenrQtGlobal();
    let booleanEditar = false;
    if (this.datos.dataBalotario) {
      booleanEditar = true;
      this.tituloModalBalotario = "Modificar Balotario";
      this.textoButton = "Actualizar";
      this.balotario = this.datos.dataBalotario.response;
      this.activo=true;
      this.validar = new ValidarExamenRealizadoRequest();
      this.validar.codBalotario = this.balotario.codBalotario;
     
      if(this.balotario.fecFin < this.fechaActual()){
         this.activo2=true;
         this.activo3=true;
      }
      else if(this.balotario.balotarioRealizado == 1){
          this.activo3=true;
      }

      this.listarPersonas(this.balotario.codBalotario);
      this.listaCantidaPreguntasxTema(this.balotario.codBalotario);
      this.dataSource3 = new MatTableDataSource();
      this.dataSource3.paginator = this.paginator;
      this.dataSource3.sort = this.sort;

      this.buscarArea(this.balotario.codCategoria);
      this.buscarlocal(this.balotario.codCategoria, this.balotario.codArea)
   
      this.estado_modificar = true;
      this.balotario.flagEditar = 1;
    } else {
      this.activo = false;
      this.activo2 = false;
      this.tituloModalBalotario = "Nuevo Balotario";
      this.textoButton = "Grabar";
      this.balotario = new BalotarioRequest();
      this.balotario.cantIntento = 2;
      this.balotario.cantTiempo = 45;
      this.balotario.califAprobatoria = 80.00;
      this.balotario.tipoBalotario = "C";
      this.estado_modificar = false;
      this.balotario.flagEditar = 0;
      this.listarTemas();
      //this.llenarDataFormularioPrueba(); 
    }
    this.crearFormInputPersonaBuscar();
    this.listarPersonasPorAsignar();
    this.formulario();
    this.crearTemasForm();

    if(booleanEditar){
      this.balotarioForm.controls['fechaInicio'].setValue(this.retornaFecha(this.balotario.fecInicio));
      this.balotarioForm.controls['fechaFin'].setValue(this.retornaFecha(this.balotario.fecFin));
    }
  }
 
  crearTemasForm() {
    this.temasForm = this.formBuilder.group({
      id: [{value: '', disabled: this.activo2}],
      denominacion:[{value: '', disabled: true}],
      value:[{value: '', disabled: true}],
    });
  }

  crearFormInputPersonaBuscar() {
    this.personasForm = this.formBuilder.group({
      personaInput: null
    })

  }

  async listarPersonas(coBalotario: number) {
    await this.balotarioService.obtenerPersonas(coBalotario)
      .then(
        (wsResponseProyecto: any) => {

          if (wsResponseProyecto.codResultado == 1) {
            this.personaResponse2 =
              (wsResponseProyecto.response != null) ? wsResponseProyecto.response : [];
            this.cargarTablaPersonas();
           

            let i: number;
            for (i = 0; i < this.personaResponse2.length; i++) {
              let balotarioPersona: BalotarioPersona = new BalotarioPersona();
              balotarioPersona.codUsuario = this.personaResponse2[i].codUsuario;
              balotarioPersona.flagAccion = 1;
              this.balotarioPersonaArray.push(balotarioPersona);
            }
            
          } else {
            this.mensaje = MENSAJES.ERROR_NOFUNCION;

          }

        },
        error => {

          this.mensaje = MENSAJES.ERROR_SERVICIO;

        }

      );

    this.cargarTablaPersonas();
  }

  public cargarTablaPersonas(): void {
    if (this.personaResponse2 != null && this.personaResponse2.length > 0) {
   
      this.dataSource = new MatTableDataSource(this.personaResponse2);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;     
      this.cantPersonas = this.personaResponse2.length;
    }
  }

  async listarTemas() {
    await this.balotarioService.listarTema()
      .then(
        (wsResponseProyecto: any) => {

          if (wsResponseProyecto.codResultado == 1) {
            this.temaResponse =
              (wsResponseProyecto.response != null) ? wsResponseProyecto.response : [];
            this.cargarTablaTema();
            this.calcularTotal();
          } else {
            this.mensaje = MENSAJES.ERROR_NOFUNCION;

          }

        },
        error => {

          this.mensaje = MENSAJES.ERROR_SERVICIO;

        }

      );
  }

  
  public calcularTotal() {
    this.temaResponse.forEach((element) => {
      this.cantidadTotalPreguntas  +=  element.cantidad;
    });
    return this.cantidadTotalPreguntas;
  }

  public verificarCantidadPreguntaTemaModificado(){
    
    var temasModificados = [];
    for (var i = 0; i < this.temaResponse.length; i++) {
        var igual=false;
         for (var j = 0; j < this.temaAuxResponse.length && !igual; j++) {
             if(this.temaResponse[i]['cantidad'] == this.temaAuxResponse[j]['cantidad']) 
                     igual=true;
         }
        if(!igual)temasModificados.push(this.temaResponse[i]);
    }

    if(temasModificados.length == 0){

      return false;
    }else{
      return true;
    }

  }
  
  public cantidadTotalActualizar(){
    this.cantidadTotalPreguntas = 0;
    this.calcularTotal(); 
  }

  keyup (evento) { 
    this.cantidadTotalActualizar();
  }

  aux : [];
  public cargarTablaTema(): void {
    if (this.temaResponse != null && this.temaResponse.length > 0) {
      this.dataSource2 = new MatTableDataSource(this.temaResponse);
      this.dataSource2.paginator = this.paginator;
      this.dataSource2.sort = this.sort;

      console.log("tabla -> " + this.dataSource2);
      this.cantTemas = this.temaResponse.length;
      console.log("temass -> " + this.cantTemas);
      this.temaAuxResponse = JSON.parse(JSON.stringify(this.temaResponse));
      this.aux = JSON.parse(JSON.stringify(this.temaResponse));
      console.log("temas arreglo auxiliar : " + this.temaAuxResponse);
    }

  }

  listarPersonasPorAsignar() {
    (this.personasForm.get('personaInput').valueChanges)

      .pipe(debounceTime(100),
        tap(() => this.isLoading = true),
        switchMap(value => this.balotarioService.listaPersonaPorAsignar({ name: value }, 1)
          .pipe(
            finalize(() => this.isLoading = false),
          )
        )
      )
      .subscribe(persona => this.filteredPersonas = persona.response);
  }

  displayFn(persona: PersonaItemResponse) {
    if (persona) { return persona.nombreCompleto; }
  }

  
  agregarPersona() {
    this.personaItem = this.personasForm.get('personaInput').value;
    // alert(this.personaItem.idCodigo + " - " + this.personaItem.nombreCompleto + " -  " + this.personaItem.identificacion);
    //   let i: number;
     
    
    if (this.personaItem  !== null){
      if(typeof (this.personaItem.idCodigo) !== "undefined"){

      console.log(this.personasForm.get('personaInput').value);

      this.personaResponse = {
        codUsuario: this.personaItem.idCodigo,
        nombreCompleto: this.personaItem.nombreCompleto,
        identificacion: this.personaItem.identificacion
      };


      let i: number;
       
      let validarExistePersona: boolean = true;
      for (i = 0; i < this.dataSource.data.length; i++) {
        if (this.dataSource.data[i].codUsuario == this.personaResponse.codUsuario) {
          validarExistePersona = false;
          this.openDialogMensaje(MENSAJES.BALOTARIO.USUARIO_ASIGNADO, true, false, null);
        }
      }

      if (validarExistePersona == true) {
        let balotarioPersona: BalotarioPersona = new BalotarioPersona();
        this.dataSource.data.push(this.personaResponse);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        balotarioPersona.codUsuario = this.personaResponse.codUsuario;
        balotarioPersona.flagAccion = 0;
        this.balotarioPersonaArray.push(balotarioPersona);

      }

    }

    else {
       
      if (this.personasForm.get('personaInput').value) {
        this.openDialogMensaje(MENSAJES.BALOTARIO.PERSONA_NO_EXISTE, true, false, null);
      }
      else {

        this.openDialogMensaje(MENSAJES.BALOTARIO.INGRESAR_PERSONA, true, false, null);
      }
    }

    }
    else {
       
      if (this.personasForm.get('personaInput').value) {
        this.openDialogMensaje(MENSAJES.BALOTARIO.PERSONA_NO_EXISTE, true, false, null);
      }
      else {

        this.openDialogMensaje(MENSAJES.BALOTARIO.INGRESAR_PERSONA, true, false, null);
      }
    }
   
  }

  retornaFecha(fecha: string) {
    return new Date(`${fecha}T23:00`);
  }

  fechaActual() {
     
    var fechaDateHoy = new Date();

      var dd = fechaDateHoy.getDate();
      var mm = fechaDateHoy.getMonth() + 1;
      var yyyy = fechaDateHoy.getFullYear();

      dd = this.adicionarCero(dd);
      mm = this.adicionarCero(mm);

    return yyyy + '-' + mm + '-' + dd;
  }


  adicionarCero(i) {
    if (i < 10) {
      i = '0' + i;
    }
    return i;
  }


  fechaActualDDMMYYYY() {
    var fechaDateHoy = new Date();

      var dd = fechaDateHoy.getDate();
      var mm = fechaDateHoy.getMonth() + 1;
      var yyyy = fechaDateHoy.getFullYear();

      dd = this.adicionarCero(dd);
      mm = this.adicionarCero(mm);

    return dd + '-' + mm + '-' + yyyy;
  }



  formulario() {
    this.balotarioForm = this.formBuilder.group({
      fechaInicio: [{value: new Date(), disabled: this.activo3}],
      fechaFin: [{value: new Date(), disabled: this.activo2}],
      descripcion: [{value: '', disabled: this.activo2}],
      nroIntentos: [{value: '', disabled: this.activo2}],
      limiteTiempo: [{value: '', disabled: this.activo2}],
      resultadoAprobatorio: [{value: '', disabled: this.activo2}],
      mensajeInicial: [{value: '', disabled: this.activo2}],
      tipoBalotario: [{value: '', disabled: this.activo}],
      cargo: [{value: '', disabled: this.activo3}],
      categoria: [{value: '', disabled: this.activo3}],
      area: [{value: '', disabled: this.activo3}],
      local: [{value: '', disabled: this.activo3}],
      estado: [{value: '', disabled: this.activo2}]
    });
    
   
  }

  cargarCombos() {
    this.itemService.listadoCargo().subscribe(data => {
      this.listCargo = data.response
      console.log('cargo:' + this.listCargo)
    });
    this.itemService.listadoCategoria().subscribe(data => {
      this.listCategoria = data.response
      console.log('categoria:' + data.response)
    });
    this.itemService.listadoEstado().subscribe(data => {
      this.listEstado = data.response
    });
  }


  tableColumnaPersonas(): void {
    this.columnas2 = ['nombreCompleto', 'identificacion', 'accion'];

  }
  tableColumnaTemas(): void {
    this.columnas3 = ['descripcion', 'cantidad'];

  }
  tableColumnaTotal(): void {
    this.columnas4 = ['total'];

  }

  eventoTipoBalotario() {
    this.balotario.codCategoria = 0;
    this.balotario.codArea = 0;
    this.balotario.codLocal = 0;
    this.balotario.codCargo = 0;
    this.dataSource.data = [];
  }


  buscarArea(idCategoria: number) {  
    this.listLocal = [];
    this.itemService.listadoArea(idCategoria).subscribe(data => {
      this.listArea = data.response
      if (data.response.length > 0) {
        if(this.balotario.codCategoria){
          this.listArea = data.response
        }
        else{
        this.balotario.codArea = 0;
        this.balotario.codLocal = 0;
        this.listArea = data.response
        }

      } else {    
        this.listArea = [];
        if (typeof this.balotario.codArea !== 'undefined') {
          this.balotario.codArea = 0;
        }
      }
    });

  }

  buscarlocal(idCategoria: number, idArea: number) {

    this.itemService.listadoLocal(idCategoria, idArea).subscribe(data => {
      if (data.response.length > 0) {
        if(this.balotario.codCategoria){
          this.listLocal = data.response
        }
        else{
        this.balotario.codLocal = 0;
        this.listLocal = data.response
        }
      } else {
        this.listLocal = [];

        if (typeof this.balotario.codLocal !== 'undefined') {
          
          this.balotario.codLocal = 0;
        }
      }
    });
  }





  verificar: VerificarBalotarioRequest;
  async grabarBalotario() {

    console.log(this.balotarioPersonaArray);
    console.log(this.qtTemas = this.dataSource4.data[0].qtTemas);
    console.log(this.qtPreguntas = this.dataSource4.data[0].qtPreguntas);

    const { valFecInicio, valFecFin } = this.validarFechaBalotario();
    const { valQtTemas, valQtPreguntas } = this.validarQtTemasPreguntas();
    if (this.validarFormulario() == false) {
      this.openDialogMensaje(MENSAJES.BALOTARIO.CAMPOS_OBLIGATORIOS, true, false, null);
    }
    else if (valFecInicio == false) {
      this.openDialogMensaje(MENSAJES.BALOTARIO.FECHA_INICIO_MAYOR_FECHA_ACTUAL, true, false, null);
    }
    else if (valFecFin == false) {
      this.openDialogMensaje(MENSAJES.BALOTARIO.FECHA_INICIO_MENOR_FECHA_FINAL, true, false, null);
    }
    else if (this.validarNroIntentos() == false) {
      this.openDialogMensaje(MENSAJES.BALOTARIO.NUMERO_INTENTOS, true, false, null);
    }
    else if (this.validarLimiteTiempo() == false) {
      this.openDialogMensaje(MENSAJES.BALOTARIO.LIMITE_TIEMPO, true, false, null);
    }
    else if (this.validarResultado() == false) {
      this.openDialogMensaje(MENSAJES.BALOTARIO.RESULTADO_APROBATORIO, true, false, null);
    }
    else if (this.validarExistePersonaTabla()==false) {
      this.openDialogMensaje(MENSAJES.BALOTARIO.BALOTARIO_INDIVIDUAL, true, false, null);
    }
    
    else if (valQtTemas == false) {
      this.openDialogMensaje(MENSAJES.BALOTARIO.VALIDAR_QT_TEMAS + " " + this.qtTemas, true, false, null);
    }
    else if (valQtPreguntas == false) {
      this.openDialogMensaje(MENSAJES.BALOTARIO.VALIDAR_QT_PREGUNTAS + " " + this.qtPreguntas, true, false, null);
    }
    else if (this.validarCantidadPreguntasNegativa()==false){
      
      this.openDialogMensaje(MENSAJES.BALOTARIO.VALIDAR_INGRESO_CANTIDAD_PREGUNTAS_NEGATIVA, true, false, null);
    }
    else if (this.validarIngresoCantidaPreguntas()==false){
      
      this.openDialogMensaje(MENSAJES.BALOTARIO.VALIDAR_INGRESO_CANTIDAD_PREGUNTAS , true, false, null);
    }
    
    
    else  if (this.balotario.flagEditar != 1) {
        let fecInicio = _moment(this.balotarioForm.get('fechaInicio').value).format('YYYY/MM/DD');
        let fecFin = _moment(this.balotarioForm.get('fechaFin').value).format('YYYY/MM/DD');
        let tipo = this.balotario.tipoBalotario;
  
       await this.balotarioService.verificarExistenciaBalotario(
          fecInicio, fecFin, tipo).then(res => {
            if (res.codResultado == 1) {
              console.log(res.codResultado);
              this.openDialogMensaje(MENSAJES.BALOTARIO.BALOTARIO_EXISTE, true, false, null);
              
            }
            else {
        
              if (this.balotario.flagEditar == 1) {
                this.openDialogMensajeConfirm(MENSAJES.BALOTARIO.MODIFICAR_BALOTARIO, true);
              }else{
                this.openDialogMensajeConfirm(MENSAJES.BALOTARIO.GUARDAR_BALOTARIO, true);
              }
               
              this.dialogRefMessage.afterClosed()
                .pipe(filter(verdadero => !!verdadero))
                .subscribe(async () => {
                  this.temaResponse.forEach((element) => {
                    let objTemasPorCantidad: BalotarioTema = new BalotarioTema();
        
                    if (element.cantidad > 0) {
                      objTemasPorCantidad.codBalotario = null;
                      objTemasPorCantidad.codTema = element.idCodigo;
                      objTemasPorCantidad.cantPorTema = element.cantidad;
                      objTemasPorCantidad.flagAccion = 0;
                      this.balotarioTemaArray.push(objTemasPorCantidad);
                    }
                  });
        
        
                  let fecInicio = _moment(this.balotarioForm.get('fechaInicio').value).format('YYYY/MM/DD');
                  let fecFin = _moment(this.balotarioForm.get('fechaFin').value).format('YYYY/MM/DD');
        
                  this.balotario.fecInicio = fecInicio;
                  this.balotario.fecFin = fecFin;
                  this.balotario.codEmpresa = 1; // valor por defecto codEmpresa = 1
                  this.balotario.codEstado = 1;  // valor por defecto codEstado = 1
                  this.balotario.cantTiempo = parseFloat(this.balotario.cantTiempo.toFixed(2));
        
        
                  if (this.balotario.tipoBalotario == 'I') {
                    this.balotario.lstBalotarioPersona = this.balotarioPersonaArray;
                    this.balotario.lstBalotarioTema = this.balotarioTemaArray;
                    await this.registrarBalotario(this.balotario);
                    console.log(this.balotario);
                  }
                  else {
                    this.balotario.lstBalotarioTema = this.balotarioTemaArray;
                    await this.registrarBalotario(this.balotario);
                    console.log(this.balotario);
                  }
        
        
                 
                  if (this.balotario.flagEditar != 1) 
                  this.snackBar.open(MENSAJES.BALOTARIO.MENSAJE_EXITOSO_GRABAR_BALOTARIO);
                  else
                  this.snackBar.open(MENSAJES.BALOTARIO.MENSAJE_EXITOSO_MODIFICAR_BALOTARIO);
        
                  this.dialogRef.close(true);
                  
                });
              
        
            }
        

          })
      }

    else {

      if (this.balotario.flagEditar == 1) {
        this.openDialogMensajeConfirm(MENSAJES.BALOTARIO.MODIFICAR_BALOTARIO, true);
      }else{
        this.openDialogMensajeConfirm(MENSAJES.BALOTARIO.GUARDAR_BALOTARIO, true);
      }
   
       
      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(async () => {

          this.temaResponse.forEach((element) => {
            let objTemasPorCantidad: BalotarioTema = new BalotarioTema();

            if (element.cantidad > 0) {
              objTemasPorCantidad.codBalotario = null;
              objTemasPorCantidad.codTema = element.idCodigo;
              objTemasPorCantidad.cantPorTema = element.cantidad;
              objTemasPorCantidad.flagAccion = 0;
              this.balotarioTemaArray.push(objTemasPorCantidad);
            }
          });


          let fecInicio = _moment(this.balotarioForm.get('fechaInicio').value).format('YYYY/MM/DD');
          let fecFin = _moment(this.balotarioForm.get('fechaFin').value).format('YYYY/MM/DD');

          this.balotario.fecInicio = fecInicio;
          this.balotario.fecFin = fecFin;
          this.balotario.codEmpresa = 1; // valor por defecto codEmpresa = 1
          //this.balotario.codEstado = 1;  // valor por defecto codEstado = 1
          this.balotario.cantTiempo = parseFloat(this.balotario.cantTiempo.toFixed(2));


          if (this.balotario.tipoBalotario == 'I') {
            this.balotario.lstBalotarioPersona = this.balotarioPersonaArray;
            this.balotario.lstBalotarioTema = this.balotarioTemaArray;
            await this.registrarBalotario(this.balotario);
            console.log(this.balotario);
          }
          else {
            this.balotario.lstBalotarioTema = this.balotarioTemaArray;
            await this.registrarBalotario(this.balotario);
            console.log(this.balotario);
          }


         
          if (this.balotario.flagEditar != 1) 
          this.snackBar.open(MENSAJES.BALOTARIO.MENSAJE_EXITOSO_GRABAR_BALOTARIO);
          else
          this.snackBar.open(MENSAJES.BALOTARIO.MENSAJE_EXITOSO_MODIFICAR_BALOTARIO);

          this.dialogRef.close(true);
          
        });
      

    }


  }


   async registrarBalotario(balotario: BalotarioRequest) {
     debugger;
     await this.balotarioService.registrarBalotario(balotario).then((wsResponse: any) => {
        console.log(wsResponse);
        if (wsResponse.codResultado == 1) {
          console.log(wsResponse.msgResultado);
        } else {
          console.log(wsResponse.msgResultado);
        }
      },
      error => {
        console.error(error);
      }
    );
  }

  validarExistePersonaTabla(): boolean {
    let valido: boolean = true;
    if (this.balotario.tipoBalotario == 'I' && this.dataSource.data.length == 0) {
      valido = false;
      console.log("SI ENTROO")
    }
    return valido;
  }

  async validarExisteBalotario() {
     
    let valido: boolean = true;


    if (this.balotario.flagEditar != 1) {
      let fecInicio = _moment(this.balotarioForm.get('fechaInicio').value).format('YYYY/MM/DD');
      let fecFin = _moment(this.balotarioForm.get('fechaFin').value).format('YYYY/MM/DD');
      let tipo = this.balotario.tipoBalotario;

     await this.balotarioService.verificarExistenciaBalotario(
        fecInicio, fecFin, tipo).then(res => {
          if (res.codResultado == 1) {
            console.log(res.codResultado);

            valido = false;
          }
        })
    }
    return valido;
  }


  validarFormulario(): boolean {
     
    let valido: boolean = true;
     if (typeof this.balotario.nomDescripcion === 'undefined' || this.balotario.nomDescripcion == "") {
      valido = false;
    } else if (typeof this.balotario.cantIntento === 'undefined' || this.balotario.cantIntento == 0) {
      valido = false;
    } else if (typeof this.balotario.cantTiempo === 'undefined' || this.balotario.cantTiempo == 0) {
      valido = false;
    } else if (typeof this.balotario.califAprobatoria === 'undefined' || this.balotario.califAprobatoria == 0) {
      valido = false;
    } else if (typeof this.balotario.nomMensaje === 'undefined' || this.balotario.nomMensaje == "") {
      valido = false;
    } else if (typeof this.balotario.tipoBalotario === 'undefined' || this.balotario.tipoBalotario == "") {

      valido = false;
    }

    else if (this.balotario.tipoBalotario == 'C') {
      if (typeof this.balotario.codCargo === 'undefined' || this.balotario.codCargo == 0) {
        valido = false;
      }
    } else if (this.balotario.tipoBalotario == 'L') {
      if (typeof this.balotario.codCategoria === 'undefined' || this.balotario.codCategoria == 0) {
        valido = false;
      } else if (typeof this.balotario.codArea === 'undefined' || this.balotario.codArea == 0) {
        valido = false;
      } else if (typeof this.balotario.codLocal === 'undefined' || this.balotario.codLocal == 0) {
        valido = false;
      }
    } else if (this.balotario.tipoBalotario == 'I') {

    }
    return valido;
  }

  validarFechaBalotario = () => {
    let valFecInicio: boolean = true;
    let valFecFin: boolean = true;
    var fecInicio = _moment(this.balotarioForm.get('fechaInicio').value).format('DD-MM-YYYY');
    var fecFin = _moment(this.balotarioForm.get('fechaFin').value).format('DD-MM-YYYY');
    let bvc = (new Date(fecInicio)).getTime();
    var sd = (new Date(fecInicio)).getTime() - (new Date(fecFin)).getTime();
    if (fecInicio < this.fechaActualDDMMYYYY()) {
      console.log("Validacion fecha inicio menor a la fecha actual")
      valFecInicio = false;
    } 
    if (fecFin <fecInicio ) {
      valFecFin = false;
    }
    return { valFecInicio, valFecFin };
  }

  validarNroIntentos(): boolean {
    let valNroIntentos: boolean = true;
    let nroIntentos = this.balotario.cantIntento;
    if (nroIntentos <= 0 || nroIntentos > 5) {
      valNroIntentos = false;
    }
    return valNroIntentos;
  }

  validarLimiteTiempo(): boolean {
    let valLimiteTiempo: boolean = true;
    let limiteTiempo = this.balotario.cantTiempo;
    if (limiteTiempo <= 0 || limiteTiempo > 60) {
      valLimiteTiempo = false;
    }
    return valLimiteTiempo;
  }

  validarResultado(): boolean {
    let valResultado: boolean = true;
    let resultado = this.balotario.califAprobatoria;
    if (resultado <= 0 || resultado > 100) {
      valResultado = false;
    }
    return valResultado;
  }


  validarQtTemasPreguntas = () => {
     
    let valQtTemas: boolean = true;
    let valQtPreguntas: boolean = true;
    let Qt_Temas = this.qtTemas;
    let Qt_Preguntas= this.qtPreguntas;

    if (this.cantidadTotalPreguntas > Qt_Preguntas) {
      valQtPreguntas = false;
    }

    this.temaResponse.forEach((objTemas) => {
      if (objTemas.cantidad > Qt_Temas) {
        valQtTemas = false;
      }
    });
    return { valQtTemas, valQtPreguntas };
  }

  validarIngresoCantidaPreguntas(): boolean {
    let valIngresoCantidaPreguntas: boolean = false;

      this.temaResponse.forEach((element) => {
          if (element.cantidad > 0) {
        valIngresoCantidaPreguntas = true;
          return;
    
      }
    });

    return valIngresoCantidaPreguntas;
  }

  validarCantidadPreguntasNegativa(): boolean {
     
    let valCantidadPreguntasNegativa: boolean = true;

      this.temaResponse.forEach((element) => {
          if (element.cantidad < 0) {
            valCantidadPreguntasNegativa = false;
          return;
      }
    });
    return valCantidadPreguntasNegativa;
  }

  public openDialogMensaje(message: string, alerta: boolean, confirmacion: boolean, valor: any
  ): void {
    const dialogRef = this.dialog.open(InfoMessageComponent, {
      disableClose: true,
      data: {
        message: message,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe((ok: number) => {
      if (ok == 0) {

      }
    });
  }
  public openDialogMensajeConfirm(message: string, confirmacion: boolean): void {

    this.dialogRefMessage = this.dialog.open(InfoMessageComponent, {
      width: '400px',
      disableClose: true,
      data: { message: message, confirmacion: confirmacion }
    });
  }

  cerrarFormulario() {
    if (this.balotario.flagEditar != 1) {
      this.openDialogMensajeConfirm(MENSAJES.PERSONA.BOTON_SALIR_GUARDAR, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(() => {
          this.dialogRef.close(true);

        });
    } else {
      this.openDialogMensajeConfirm(MENSAJES.PERSONA.BOTON_SALIR_MODIFICAR, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(() => {
          this.dialogRef.close(true);

        });
    }
  }

  eliminarPersona(rowid: number) {
    this.validar = new ValidarExamenRealizadoRequest();
    this.validar.codBalotario = this.balotario.codBalotario;
    this.validar.codUsuario = this.dataSource.data[rowid].codUsuario;

    console.log(this.dataSource.data[rowid].codUsuario);
    console.log(this.validar);
    if (this.datos.dataBalotario) {


      this.balotarioService.validarExamenRealizado(this.validar).then(
        (wsResponse: any) => {
          console.log(wsResponse);
          if (wsResponse.codResultado == 1) {

            this.examenRealizado = true;
            console.log(wsResponse.msgResultado);
            this.openDialogMensaje(MENSAJES.PERSONA.PERSONA_REALIZO_EXAMEN, true, false, null);

          } else if (wsResponse.codResultado == -1) {
            this.examenRealizado = false;
            console.log(wsResponse.msgResultado);
            this.openDialogMensajeConfirm(MENSAJES.PERSONA.ELIMINAR_PERSONA_BALOTARIO, true);
            this.dialogRefMessage.afterClosed()
              .pipe(filter(verdadero => !!verdadero))
              .subscribe(() => {
                console.log(this.dataSource.data[rowid].codUsuario);
                if (rowid > -1) {
                  this.dataSource.data.splice(rowid, 1);
                  this.dataSource._updateChangeSubscription();
                  this.balotarioPersonaArray.splice(rowid, 1);
                }
              });

          }
        },
        error => {
          console.error(error);
        }
      );

    }
    else {
      this.openDialogMensajeConfirm(MENSAJES.PERSONA.ELIMINAR_PERSONA_BALOTARIO, true);
      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(() => {
          console.log(this.dataSource.data[rowid].codUsuario);
          if (rowid > -1) {
            this.dataSource.data.splice(rowid, 1);
            this.dataSource._updateChangeSubscription();
            this.balotarioPersonaArray.splice(rowid, 1);
          }
        });
    }
  }

  // calculation() {
  //   let sum: number = 0;
  //   if (this.dataSource)
  //     for (let row of this.dataSource.data) {
  //       if (row.id != 0) sum += row.id;
  //     }
  //   return sum;
  // }




  cantidadPreguntaResponse: any[];
  async listaCantidaPreguntasxTema(codBal: number) {
    await this.balotarioService.obtenerCantidadPreguntas(codBal)
      .then(
        (wsResponseProyecto: any) => {

          if (wsResponseProyecto.codResultado == 1) {
            this.temaResponse = (wsResponseProyecto.response != null) ? wsResponseProyecto.response : [];
            this.cargarTablaCantidadPreguntas();
            this.calcularTotal();
            console.log(this.temaResponse)
          } else {
            this.mensaje = MENSAJES.ERROR_NOFUNCION;
            // this.openDialogMensaje(null,  wsResponseProyecto.msgResultado, true, false, wsResponseProyecto.codResultado);
          }

        },
        error => {

          this.mensaje = MENSAJES.ERROR_SERVICIO;
          /*   this.openDialogMensaje(this.mensaje, MENSAJES.PREFACTIBILIDAD.TITLE, true, false, null);
            console.error(error);
            this.disableBuscar = false; */
        }

      );


  }

  cantTemaPregunta: number;
  public cargarTablaCantidadPreguntas(): void {
    if (this.temaResponse != null && this.temaResponse.length > 0) {
      this.dataSource2 = new MatTableDataSource(this.temaResponse);
      this.dataSource2.paginator = this.paginator;
      this.dataSource2.sort = this.sort;
      console.log("tabla -> " + this.dataSource2);
      this.cantTemaPregunta = this.temaResponse.length;
      console.log("balotarios -> " + this.cantTemaPregunta);
      this.temaAuxResponse = JSON.parse(JSON.stringify(this.temaResponse));
    }

  }


  async obtenrQtGlobal() {
    await this.balotarioService.obtenerQtGlobal()
      .then(
        (wsResponseProyecto: any) => {
          if (wsResponseProyecto.codResultado == 1) {
            this.qtGlobalResponse =
              (wsResponseProyecto.response != null) ? wsResponseProyecto.response : []
            console.log(this.qtGlobalResponse);
            this.dataSource4 = new MatTableDataSource(this.qtGlobalResponse);
            this.qtTemas = this.dataSource4.data[0].qtTemas;
            this.qtPreguntas = this.dataSource4.data[0].qtPreguntas;
            console.log(this.qtTemas);
            console.log(this.qtPreguntas);

          } else {
            this.mensaje = MENSAJES.ERROR_NOFUNCION;
          }
        },
        error => { this.mensaje = MENSAJES.ERROR_SERVICIO; }
      );
  }


  async validarExamenRealizado(validar : ValidarExamenRealizadoRequest){
   
    await  this.balotarioService.validarExamenRealizado(validar).then(
        (wsResponse: any) => {

          if (wsResponse.codResultado == 1) {
            this.examenRealizado = true;
          } else {
            this.examenRealizado = false;
     
          }
        },
        error => {
       
        }
      );
    }


  async verficarBalotarioRealizado(objBalotario : ValidarExamenRealizadoRequest){
      await  this.balotarioService.verificarBalotarioRealizado(objBalotario).then(
          (wsResponse: any) => {
            console.log(wsResponse);
            if (wsResponse.codResultado == 1) {
              this.balotarioRealizado = true;
              console.log(wsResponse.msgResultado);
            } else {
              this.balotarioRealizado = false;
              console.log(wsResponse.msgResultado);
            }
          },
          error => {
            console.error(error);
          }
        );
      }




}// FIN DEL COMPONENT

interface Databalotario {
  dataBalotario?: any
}

export interface UserData {
  id: number;
  name: string;
  identificacion: string;

}

export interface TemaData {
  correlativo: number;
  codigoTema: number;
  nomTema: string;
  cantidad: number;

}

export interface CantidadPreguntasData {
  correlativo: number;
  codigoTema: number;
  nomTema: string;
  cantidad: number;
}


