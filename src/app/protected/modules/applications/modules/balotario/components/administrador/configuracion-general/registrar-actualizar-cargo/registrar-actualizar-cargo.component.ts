import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { InfoMessageComponent } from '@shared/components/info-message/info-message.component';
import { MENSAJES } from 'app/common';
import { filter } from 'rxjs/operators';
import { Cargo } from '../../../../dto/request/Cargo';
import { ConfiguracionService } from '../../../../service/configuracion.service';

@Component({
  selector: 'app-registrar-actualizar-cargo',
  templateUrl: './registrar-actualizar-cargo.component.html',
  styleUrls: ['./registrar-actualizar-cargo.component.scss']
})
export class RegistrarActualizarCargoComponent implements OnInit {

  cargoForm: FormGroup;
  textoButton: String;
  tituloModalCargo: String;
  cargo: Cargo = new Cargo();
  dialogRefMessage: MatDialogRef<any>;
  mensaje: any;
  mostrarEstado: boolean = false;


  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<RegistrarActualizarCargoComponent>,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private configuracionService: ConfiguracionService,
    @Inject(MAT_DIALOG_DATA)
    public datos: DataCargo,
  ) { }

  ngOnInit() {
    debugger;
    console.log(this.datos.dataCargo)
    if (this.datos.dataCargo) {
    this.tituloModalCargo = "Modificar cargo";
    this.textoButton = "Actualizar";
    this.cargo = this.datos.dataCargo.response;
    this.mostrarEstado = true;  
    }
    else{
      this.tituloModalCargo = "Agregar cargo";
      this.textoButton = "Grabar";
    }
  
    this.formulario();
  }


  formulario() {
    this.cargoForm = this.formBuilder.group({
      descripcionCargoFrm: [''],  
      estadoFrm: [''],  
    });
  }
  
  grabarCargo(){
    if (this.validarFormulario() == false) {
      this.openDialogMensaje(MENSAJES.CARGO.CAMPOS_OBLIGATORIOS, true, false, null);
    }
    else if (this.cargo.flagEditar != 1) {
      this.openDialogMensajeConfirm(MENSAJES.CARGO.GUARDAR_CARGO, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(async() => {   
       debugger;
          await this.configuracionService.grabarCargo(this.cargo).then((wsResponse : any)=> {
            debugger;
            console.log(wsResponse);  
           },  error => {
                     
             this.mensaje = MENSAJES.ERROR_SERVICIO;
             console.error(this.mensaje); 
           }      
       ) ;
   
          this.dialogRef.close(true);
          this.snackBar.open("El cargo ha sido registrado correctamente!");
        });
    } 
    else {
            debugger;       
      this.openDialogMensajeConfirm(MENSAJES.CATEGORIA.MODIFICAR_CATEGORIA, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(async() => {   
       debugger;
          await this.configuracionService.grabarCargo(this.cargo).then((wsResponse : any)=> {
            debugger;
            console.log(wsResponse);  
           },  error => {
                     
             this.mensaje = MENSAJES.ERROR_SERVICIO;
             console.error(this.mensaje); 
           }      
       ) ;
   
          this.dialogRef.close(true);
          this.snackBar.open("El cargo ha sido registrado correctamente!");
        });
    }    
 

  }

  validarFormulario(): boolean {
    debugger;
    let valido: boolean = true;
    if (typeof this.cargo.descripcionCargo === 'undefined' || this.cargo.descripcionCargo  == "") {
      valido = false;
    }
    return valido;
  }
  public openDialogMensajeConfirm(message: string, confirmacion: boolean): void {

    this.dialogRefMessage = this.dialog.open(InfoMessageComponent, {
      //width: '400px',
      disableClose: true,
      data: { message: message, confirmacion: confirmacion }
    });
  }



  public openDialogMensaje(message: string, alerta: boolean, confirmacion: boolean, valor: any
  ): void {
    const dialogRef = this.dialog.open(InfoMessageComponent, {
      disableClose: true,
      data: {
        message: message,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe((ok: number) => {
      if (ok == 0) {

      }
    });
  }

  cerrarFormulario() {
    if (this.cargo.flagEditar != 1) {
      this.openDialogMensajeConfirm(MENSAJES.CARGO.BOTON_SALIR_GUARDAR, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(() => {
          this.dialogRef.close(true);

        });
    } else {
      this.openDialogMensajeConfirm(MENSAJES.CARGO.BOTON_SALIR_MODIFICAR, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(() => {
          this.dialogRef.close(true);

        });
    }
  }

}

interface DataCargo {
  dataCargo?: any
}
