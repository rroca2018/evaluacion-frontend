import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { InfoMessageComponent } from '@shared/components/info-message/info-message.component';
import { MENSAJES } from 'app/common';
import { filter } from 'rxjs/operators';
import { Local } from '../../../../dto/request/Local';
import { ConfiguracionService } from '../../../../service/configuracion.service';
import { ItemService } from '../../../../service/item.service';

@Component({
  selector: 'app-registrar-actualizar-local',
  templateUrl: './registrar-actualizar-local.component.html',
  styleUrls: ['./registrar-actualizar-local.component.scss']
})
export class RegistrarActualizarLocalComponent implements OnInit {

  areaForm: FormGroup;
  textoButton: String;
  tituloModalLocal: String;
  local: Local = new Local();
  dialogRefMessage: MatDialogRef<any>;
  mensaje: any;
  mostrarEstado: boolean = false;

  listCategoria: any[];
  listArea: any[];
  listLocal: any[];

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<RegistrarActualizarLocalComponent>,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private configuracionService: ConfiguracionService,
    private itemService: ItemService,
    @Inject(MAT_DIALOG_DATA)
    public datos: DataLocal,
  ) { }

  ngOnInit() {
    debugger;
    this.cargarCombos();
    console.log(this.datos.dataLocal)
    if (this.datos.dataLocal) {
    this.tituloModalLocal = "Modificar local";
    this.textoButton = "Actualizar";
    this.local = this.datos.dataLocal.response;
    this.buscarArea(this.local.idCategoria);
    this.mostrarEstado = true;  
    }
    else{
      this.tituloModalLocal = "Agregar local";
      this.textoButton = "Grabar";
    }
  
    this.formulario();
  }

  cargarCombos() {
    this.itemService.listadoCategoria().subscribe(data => {
      this.listCategoria = data.response
      console.log('categoria:' + data.response)
    });
    
  }

  buscarArea(idCategoria: number) {  
    this.listLocal = [];
    this.itemService.listadoArea(idCategoria).subscribe(data => {
      this.listArea = data.response
      if (data.response.length > 0) {
        this.listArea = data.response
      } else {
        this.listArea = [];
        if(typeof this.local.idArea !== 'undefined'){
          this.local.idArea = 0;
        }
      }
    });

  }

  formulario() {
    this.areaForm = this.formBuilder.group({
      descripcionLocalFrm: [''],  
      categoriaFrm: [''],  
      areaFrm: [''],  
      estadoFrm: [''],  
    });
  }
  
  grabarLocal(){
    if (this.validarFormulario() == false) {
      this.openDialogMensaje(MENSAJES.LOCAL.CAMPOS_OBLIGATORIOS, true, false, null);
    }
    else if (this.local.flagEditar != 1) {
      this.openDialogMensajeConfirm(MENSAJES.LOCAL.GUARDAR_LOCAL, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(async() => {   
       debugger;
          await this.configuracionService.grabarLocal(this.local).then((wsResponse : any)=> {
            debugger;
            console.log(wsResponse);  
           },  error => {                   
             this.mensaje = MENSAJES.ERROR_SERVICIO;
             console.error(this.mensaje); 
           }      
       ) ;
          this.dialogRef.close(true);
          this.snackBar.open("El local ha sido registrado correctamente!");
        });
    } 
    else {
            debugger;       
      this.openDialogMensajeConfirm(MENSAJES.AREA.MODIFICAR_AREA, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(async() => {   
       debugger;
          await this.configuracionService.grabarLocal(this.local).then((wsResponse : any)=> {
            debugger;
            console.log(wsResponse);  
           },  error => {
                     
             this.mensaje = MENSAJES.ERROR_SERVICIO;
             console.error(this.mensaje); 
           }      
       ) ;
   
          this.dialogRef.close(true);
          this.snackBar.open("El área ha sido registrado correctamente!");
        });
    }    
 

  }

  validarFormulario(): boolean {
    debugger;
    let valido: boolean = true;
    if (typeof this.local.descripcionLocal === 'undefined' || this.local.descripcionLocal == "") {
      valido = false;
    }
    else if (typeof this.local.idCategoria === 'undefined' || this.local.idCategoria == 0) {
      valido = false;
    }
    else if (typeof this.local.idArea === 'undefined' || this.local.idArea == 0) {
      valido = false;
    }
    return valido;
  }

  public openDialogMensajeConfirm(message: string, confirmacion: boolean): void {

    this.dialogRefMessage = this.dialog.open(InfoMessageComponent, {
      //width: '400px',
      disableClose: true,
      data: { message: message, confirmacion: confirmacion }
    });
  }



  public openDialogMensaje(message: string, alerta: boolean, confirmacion: boolean, valor: any
  ): void {
    const dialogRef = this.dialog.open(InfoMessageComponent, {
      disableClose: true,
      data: {
        message: message,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe((ok: number) => {
      if (ok == 0) {

      }
    });
  }

  cerrarFormulario() {
    if (this.local.flagEditar != 1) {
      this.openDialogMensajeConfirm(MENSAJES.AREA.BOTON_SALIR_GUARDAR, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(() => {
          this.dialogRef.close(true);

        });
    } else {
      this.openDialogMensajeConfirm(MENSAJES.AREA.BOTON_SALIR_MODIFICAR, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(() => {
          this.dialogRef.close(true);

        });
    }
  }
}
interface DataLocal {
  dataLocal?: any
}
