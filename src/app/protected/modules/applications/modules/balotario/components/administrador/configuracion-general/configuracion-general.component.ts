import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MENSAJES } from 'app/common';
import { ConfiguracionService } from '../../../service/configuracion.service';
import { RegistrarActualizarAreaComponent } from './registrar-actualizar-area/registrar-actualizar-area.component';
import { RegistrarActualizarCargoComponent } from './registrar-actualizar-cargo/registrar-actualizar-cargo.component';
import { RegistrarActualizarCategoriaComponent } from './registrar-actualizar-categoria/registrar-actualizar-categoria.component';
import { RegistrarActualizarLocalComponent } from './registrar-actualizar-local/registrar-actualizar-local.component';
import { RegistrarActualizarTemaComponent } from './registrar-actualizar-tema/registrar-actualizar-tema.component';

@Component({
  selector: 'app-configuracion-general',
  templateUrl: './configuracion-general.component.html',
  styleUrls: ['./configuracion-general.component.scss']
})
export class ConfiguracionGeneralComponent implements OnInit {

  @ViewChild('paginatorCargo') paginatorCargo: MatPaginator;
  @ViewChild('paginatorCategoria') paginatorCategoria: MatPaginator;
  @ViewChild('paginatorArea') paginatorArea: MatPaginator;
  @ViewChild('paginatorLocal') paginatorLocal: MatPaginator;
  @ViewChild('paginatorTemas') paginatorTemas: MatPaginator;


  @ViewChild('cargoSort') cargoSort: MatSort;
  @ViewChild('categoriaSort') categoriaSort: MatSort;
  @ViewChild('areaSort') areaSort: MatSort;
  @ViewChild('localSort') localSort: MatSort;
  @ViewChild('temaSort') temaSort: MatSort;



  dataSourceCategoriaTabla: MatTableDataSource<any>;
  dataSourceAreaTabla: MatTableDataSource<any>;
  dataSourceLocalTabla: MatTableDataSource<any>;
  dataSourceCargoTabla: MatTableDataSource<any>;
  dataSourceTemaTabla: MatTableDataSource<any>;

  columnasCategoria: string[] = [];
  columnasArea: string[] = [];
  columnasLocal: string[] = [];
  columnasCargo: string[] = [];
  columnasTema: string[] = [];

  categoriaResponse: any;
  areaResponse: any;
  localResponse: any;
  cargoResponse: any;
  temaResponse: any;

  mensaje: string;
  cantCategoria: number;
  cantArea: number;
  cantLocal: number;
  cantCargo: number;
  cantTema: number;

  constructor(
    private dialog: MatDialog,
    private configuracionService: ConfiguracionService,
  ) {
    this.dataSourceCategoriaTabla = new MatTableDataSource([]);
    this.dataSourceAreaTabla = new MatTableDataSource([]);
    this.dataSourceLocalTabla = new MatTableDataSource([]);
    this.dataSourceCargoTabla = new MatTableDataSource([]);
    this.dataSourceTemaTabla = new MatTableDataSource([]);
  }

  filtrosForm: FormGroup;
  ngOnInit() {
    this.crearFiltrosForm();

    this.dataSourceCategoriaTabla.sort = this.categoriaSort;
    this.dataSourceAreaTabla.sort = this.areaSort;
    this.dataSourceLocalTabla.sort = this.localSort;
    this.dataSourceCargoTabla.sort = this.cargoSort;
    this.dataSourceTemaTabla.sort = this.temaSort;

    this.columnaCategoriaTabla();
    this.columnaAreaTabla();
    this.columnaLocalTabla();
    this.columnaCargoTabla();
    this.columnaTemaTabla();

    this.cargarTablaCategoria();
    this.cargarTablaArea();
    this.cargarTablaLocal();
    this.cargarTablaCargo();

    this.listarCategoria();
    this.listarArea();
    this.listarLocal();
    this.listarCargo();
    this.listarTema();
  }


  columnaCategoriaTabla(): void {
    this.columnasCategoria = ['categoria', 'estado', 'editar'];
  }
  columnaAreaTabla(): void {
    this.columnasArea = ['categoria', 'cidNombre', 'estados', 'editar'];
  }
  columnaLocalTabla(): void {
    this.columnasLocal = ['categoria', 'area', 'local', 'estado', 'editar'];
  }
  columnaCargoTabla(): void {
    this.columnasCargo = ['cargo', 'estado', 'editar'];
  }
  columnaTemaTabla(): void {
    this.columnasTema = ['tema', 'estado', 'editar'];
  }

  async listarCategoria() {
    await this.configuracionService.listadoCategoria()
      .subscribe(
        (wsResponseCategorias: any) => {
          debugger;
          if (wsResponseCategorias.codResultado == 1) {
            this.categoriaResponse = (wsResponseCategorias.response != null) ? wsResponseCategorias.response : [];
            this.cargarTablaCategoria();
          } else {
            this.mensaje = MENSAJES.ERROR_NOFUNCION;
          }
        },
        error => {
          this.mensaje = MENSAJES.ERROR_SERVICIO;
        }
      );

    this.cargarTablaCategoria();
  }

  public cargarTablaCategoria(): void {
    debugger;
    if (this.categoriaResponse != null && this.categoriaResponse.length > 0) {
      this.dataSourceCategoriaTabla = new MatTableDataSource(this.categoriaResponse);
      this.dataSourceCategoriaTabla.paginator = this.paginatorCategoria;
      this.dataSourceCategoriaTabla.sort = this.categoriaSort;
      this.cantCategoria = this.categoriaResponse.length;
      console.log("categorias -> " + this.cantCategoria);
    }
  }

  async listarArea() {
    debugger;
    await this.configuracionService.listadoArea()
      .subscribe(
        (wsResponseAreas: any) => {
          debugger;
          if (wsResponseAreas.codResultado == 1) {
            this.areaResponse = (wsResponseAreas.response != null) ? wsResponseAreas.response : [];
            this.cargarTablaArea();
          } else {
            this.mensaje = MENSAJES.ERROR_NOFUNCION;
          }
        },
        error => {
          this.mensaje = MENSAJES.ERROR_SERVICIO;
        }
      );

    this.cargarTablaArea();
  }

  public cargarTablaArea(): void {
    debugger;
    if (this.areaResponse != null && this.areaResponse.length > 0) {
      debugger;
      this.dataSourceAreaTabla = new MatTableDataSource(this.areaResponse);
      this.dataSourceAreaTabla.paginator = this.paginatorArea;
      this.dataSourceAreaTabla.sort = this.areaSort;
      this.cantArea = this.areaResponse.length;
      console.log("areas -> " + this.cantArea);
    }
  }

  async listarLocal() {
    debugger;
    await this.configuracionService.listadoLocal()
      .subscribe(
        (wsResponseLocal: any) => {
          debugger;
          if (wsResponseLocal.codResultado == 1) {
            this.localResponse = (wsResponseLocal.response != null) ? wsResponseLocal.response : [];
            this.cargarTablaLocal();
          } else {
            this.mensaje = MENSAJES.ERROR_NOFUNCION;
          }
        },
        error => {
          this.mensaje = MENSAJES.ERROR_SERVICIO;
        }
      );

    this.cargarTablaLocal();
  }

  public cargarTablaLocal(): void {
    debugger;
    if (this.localResponse != null && this.localResponse.length > 0) {
      debugger;
      this.dataSourceLocalTabla = new MatTableDataSource(this.localResponse);
      this.dataSourceLocalTabla.paginator = this.paginatorLocal;
      this.dataSourceLocalTabla.sort = this.localSort;
      this.cantLocal = this.localResponse.length;
      console.log("locales -> " + this.cantLocal);
    }
  }

  async listarCargo() {
    debugger;
    await this.configuracionService.listadoCargo()
      .subscribe(
        (wsResponseCargo: any) => {
          debugger;
          if (wsResponseCargo.codResultado == 1) {
            this.cargoResponse = (wsResponseCargo.response != null) ? wsResponseCargo.response : [];
            this.cargarTablaCargo();
          } else {
            this.mensaje = MENSAJES.ERROR_NOFUNCION;
          }
        },
        error => {
          this.mensaje = MENSAJES.ERROR_SERVICIO;
        }
      );

    this.cargarTablaCargo();
  }

  public cargarTablaCargo(): void {
    debugger;
    if (this.cargoResponse != null && this.cargoResponse.length > 0) {
      debugger;
      this.dataSourceCargoTabla = new MatTableDataSource(this.cargoResponse);
      this.dataSourceCargoTabla.paginator = this.paginatorCargo;
      this.dataSourceCargoTabla.sort = this.cargoSort;
      this.cantCargo = this.cargoResponse.length;
      console.log("cargos -> " + this.cantCargo);
    }
  }


  async listarTema() {
    debugger;
    await this.configuracionService.listadoTema()
      .subscribe(
        (wsResponseTema: any) => {
          debugger;
          if (wsResponseTema.codResultado == 1) {
            this.temaResponse = (wsResponseTema.response != null) ? wsResponseTema.response : [];
            this.cargarTablaTema();
          } else {
            this.mensaje = MENSAJES.ERROR_NOFUNCION;
          }
        },
        error => {
          this.mensaje = MENSAJES.ERROR_SERVICIO;
        }
      );

    this.cargarTablaTema();
  }

  public cargarTablaTema(): void {
    debugger;
    if (this.temaResponse != null && this.temaResponse.length > 0) {
      debugger;
      this.dataSourceTemaTabla = new MatTableDataSource(this.temaResponse);
      this.dataSourceTemaTabla.paginator = this.paginatorTemas;
      this.dataSourceTemaTabla.sort = this.temaSort;
      this.cantTema = this.temaResponse.length;
      console.log("temas -> " + this.cantTema);
    }
  }

  agregarCategoria() {
    const dialogReg: MatDialogRef<RegistrarActualizarCategoriaComponent> = this.dialog.open(RegistrarActualizarCategoriaComponent, {
      disableClose: true,
      panelClass: 'dialog-no-padding',
      width: '40%',
      data: {
      }
    });

    dialogReg.afterClosed().subscribe((valor: any) => {
      if (valor) {
        this.listarCategoria();
        this.cargarTablaCategoria();
      }
    });
  }

  editarCategoria(coCategoria: number): void {
    debugger
    this.configuracionService.obtenerCategoria(coCategoria).subscribe(dataCategoria => {

      const dialogReg: MatDialogRef<RegistrarActualizarCategoriaComponent> = this.dialog.open(RegistrarActualizarCategoriaComponent, {
        disableClose: true,
        panelClass: 'dialog-no-padding',
        width: '50%',
        data: {
          dataCategoria
        }
      });

      dialogReg.afterClosed().subscribe((valor: any) => {
        console.log(valor)
        if (valor) {
          this.listarCategoria();
        }

      });
    });
  }

  agregarArea() {
    const dialogReg: MatDialogRef<RegistrarActualizarAreaComponent> = this.dialog.open(RegistrarActualizarAreaComponent, {
      disableClose: true,
      panelClass: 'dialog-no-padding',
      width: '40%',
      data: {
      }
    });

    dialogReg.afterClosed().subscribe((valor: any) => {
      if (valor) {
        this.listarArea();
      }
    });
  }

  editarArea(codArea: number): void {
    debugger
    this.configuracionService.obtenerArea(codArea).subscribe(dataArea => {

      const dialogReg: MatDialogRef<RegistrarActualizarAreaComponent> = this.dialog.open(RegistrarActualizarAreaComponent, {
        disableClose: true,
        panelClass: 'dialog-no-padding',
        width: '40%',
        data: {
          dataArea
        }
      });

      dialogReg.afterClosed().subscribe((valor: any) => {
        console.log(valor)
        if (valor) {
          this.listarArea();
        }

      });
    });
  }

  agregarLocal() {
    const dialogReg: MatDialogRef<RegistrarActualizarLocalComponent> = this.dialog.open(RegistrarActualizarLocalComponent, {
      disableClose: true,
      panelClass: 'dialog-no-padding',
      width: '40%',
      data: {
      }
    });
    dialogReg.afterClosed().subscribe((valor: any) => {
      if (valor) {
        this.listarLocal();
      }
    });
  }

  editarLocal(codLocal: number): void {
    debugger
    this.configuracionService.obtenerLocal(codLocal).subscribe(dataLocal => {

      const dialogReg: MatDialogRef<RegistrarActualizarLocalComponent> = this.dialog.open(RegistrarActualizarLocalComponent, {
        disableClose: true,
        panelClass: 'dialog-no-padding',
        width: '40%',
        data: {
          dataLocal
        }
      });

      dialogReg.afterClosed().subscribe((valor: any) => {
        console.log(valor)
        this.listarLocal();

      });
    });


  }

  agregarCargo() {
    const dialogReg: MatDialogRef<RegistrarActualizarCargoComponent> = this.dialog.open(RegistrarActualizarCargoComponent, {
      disableClose: true,
      panelClass: 'dialog-no-padding',
      width: '40%',
      data: {
      }
    });
    dialogReg.afterClosed().subscribe((valor: any) => {
      if (valor) {
        this.listarCargo();
      }
    });
  }

  editarCargo(codCargo: number): void {
    debugger
    this.configuracionService.obtenerCargo(codCargo).subscribe(dataCargo => {

      const dialogReg: MatDialogRef<RegistrarActualizarCargoComponent> = this.dialog.open(RegistrarActualizarCargoComponent, {
        disableClose: true,
        panelClass: 'dialog-no-padding',
        width: '40%',
        data: {
          dataCargo
        }
      });

      dialogReg.afterClosed().subscribe((valor: any) => {
        console.log(valor)
          this.listarCargo();
      });
    });

  }

  agregarTema() {
    const dialogReg: MatDialogRef<RegistrarActualizarTemaComponent> = this.dialog.open(RegistrarActualizarTemaComponent, {
      disableClose: true,
      panelClass: 'dialog-no-padding',
      width: '40%',
      data: {
      }
    });
    dialogReg.afterClosed().subscribe((valor: any) => {
      if (valor) {
        this.listarTema();
      }
    });
  }

  editarTema(codCargo: number): void {
    debugger
    this.configuracionService.obtenerTema(codCargo).subscribe(dataTema => {

      const dialogReg: MatDialogRef<RegistrarActualizarTemaComponent> = this.dialog.open(RegistrarActualizarTemaComponent, {
        disableClose: true,
        panelClass: 'dialog-no-padding',
        width: '40%',
        data: {
          dataTema
        }
      });

      dialogReg.afterClosed().subscribe((valor: any) => {
        console.log(valor)
          this.listarTema();
      });
    });

  }
  crearFiltrosForm() {
    this.filtrosForm = new FormGroup({
      codDocFrmCtrl: new FormControl(null),
    });
  }
}