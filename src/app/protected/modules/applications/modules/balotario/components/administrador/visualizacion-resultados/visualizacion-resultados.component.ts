import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { BalotarioService } from '../../../service/balotario.service';
import { FiltrarResultadosComponent } from './filtrar-resultados/filtrar-resultados.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FiltroRequest } from '../../../dto/request/FiltroRequest';
import { MENSAJES } from 'app/common';
import { ItemService } from '../../../service/item.service';
import { ExporterService } from '../../../service/exporter.service';
import { BalotarioRequest } from '../../../dto/request/balotarioRequest';

@Component({
  selector: 'app-visualizacion-resultados',
  templateUrl: './visualizacion-resultados.component.html',
  styleUrls: ['./visualizacion-resultados.component.scss']
})
export class VisualizacionResultadosComponent implements OnInit {

  @ViewChild('paginatorCargo') paginatorCargo: MatPaginator;
  @ViewChild('paginatorCategoria') paginatorCategoria: MatPaginator;
  @ViewChild('paginatorArea') paginatorArea: MatPaginator;
  @ViewChild('paginatorLocal') paginatorLocal: MatPaginator;
  @ViewChild('paginatorNotas') paginatorNotas: MatPaginator;
  @ViewChild('paginatorDetalleEvaluaciones') paginatorDetalleEvaluaciones: MatPaginator;

  @ViewChild('cargoSort') cargoSort: MatSort;
  @ViewChild('categoriaSort') categoriaSort: MatSort;
  @ViewChild('areaSort') areaSort: MatSort;
  @ViewChild('localSort') localSort: MatSort;
  @ViewChild('notaSort') notaSort: MatSort;
  @ViewChild('detallEvalSort') detallEvalSort: MatSort;


  columnasMonitoreo: string[] = [];
  columnasNotas: string[] = [];
  columnasDetalles: string[] = [];
  filtrosForm: FormGroup;
  notasForm: FormGroup;
  detalleForm: FormGroup;

  tablaNotasResponse: any[];
  tablaDetalleEvaluacionesResponse: any[];

  tabCargoCabeceraData: any[];
  tabCargoDetalleData: CargoDetalle[];

  tabCategoriaCabeceraData: any[];
  tabCategoriaDetalleData: CategoriaDetalle[];

  tabAreaCabeceraData: any[];
  tabAreaDetalleData: AreaDetalle[];

  tabLocalCabeceraData: any[];
  tabLocalDetalleData: LocalDetalle[];

  listYears: any[];
  listBalotarioCombo: any[];


  // BARRA CARGO
  valorInicial = 0;
  totalVerde = 0;
  totalRojo = 0;
  barra_verde = 0;
  barra_rojo = 0;

  valorInicialGrilla = 0;
  totalVerdeGrilla = 0;
  totalRojoGrilla = 0;
  barra_verdeGrilla = 0;
  barra_rojoGrilla = 0;

  cienPorcientoVerde = false;
  cienPorcientoRojo = false;

  // BARRA CATEGORIA
  valorInicialCat = 0;
  totalVerdeCat = 0;
  totalRojoCat = 0;
  barra_verdeCat = 0;
  barra_rojoCat = 0;

  valorInicialGrillaCat = 0;
  totalVerdeGrillaCat = 0;
  totalRojoGrillaCat = 0;
  barra_verdeGrillaCat = 0;
  barra_rojoGrillaCat = 0;

  cienPorcientoVerdeCat = false;
  cienPorcientoRojoCat = false;

  // BARRA AREA
  valorInicialArea = 0;
  totalVerdeArea = 0;
  totalRojoArea = 0;
  barra_verdeArea = 0;
  barra_rojoArea = 0;

  valorInicialGrillaArea = 0;
  totalVerdeGrillaArea = 0;
  totalRojoGrillaArea = 0;
  barra_verdeGrillaArea = 0;
  barra_rojoGrillaArea = 0;

  cienPorcientoVerdeArea = false;
  cienPorcientoRojoArea = false;

  // BARRA LOCAL
  valorInicialLocal = 0;
  totalVerdeLocal = 0;
  totalRojoLocal = 0;
  barra_verdeLocal = 0;
  barra_rojoLocal = 0;

  valorInicialGrillaLocal = 0;
  totalVerdeGrillaLocal = 0;
  totalRojoGrillaLocal = 0;
  barra_verdeGrillaLocal = 0;
  barra_rojoGrillaLocal = 0;

  cienPorcientoVerdeLocal = false;
  cienPorcientoRojoLocal = false;



  cantCargo: number;
  realizada: number;
  porComenzar: number;
  fechaCierre: string;
  cantidadCargos: number = 0;
  cantidadRegistrosNotas: number = 0;
  cantidadRegistrosEvaluacions: number = 0;


  cantCategoria: number;
  realizadaCategoria: number;
  porComenzarCategoria: number;
  fechaCierreCategoria: string;

  cantArea: number;
  realizadaArea: number;
  porComenzarArea: number;
  fechaCierreArea: string;

  cantLocal: number;
  realizadaLocal: number;
  porComenzarLocal: number;
  fechaCierreLocal: string;

  cantidadCategoria: number = 0;

  filtroInicial: FiltroRequest = new FiltroRequest();
  filtro: FiltroRequest = new FiltroRequest();

  dataSourceNota: MatTableDataSource<any>;
  dataSourceDetalleEvaluaciones: MatTableDataSource<any>;
  dataSourceCargoTabla: MatTableDataSource<CargoDetalle>;
  dataSourceCategoriaTabla: MatTableDataSource<CategoriaDetalle>;
  dataSourceAreaTabla: MatTableDataSource<AreaDetalle>;
  dataSourceLocalTabla: MatTableDataSource<LocalDetalle>;


  txtBuscar: string;
  balotarioData: BalotarioRequest = new BalotarioRequest();

  nombreBalotario: string;

  constructor(
    private dialog: MatDialog,
    private balotarioService: BalotarioService,
    private formBuilder: FormBuilder,
    private itemService: ItemService,
    private excelService: ExporterService,
  ) {

    this.dataSourceNota = new MatTableDataSource([]);
    this.dataSourceDetalleEvaluaciones = new MatTableDataSource([]);
    this.dataSourceCargoTabla = new MatTableDataSource([]);
    this.dataSourceCategoriaTabla = new MatTableDataSource([]);
    this.dataSourceAreaTabla = new MatTableDataSource([]);
    this.dataSourceLocalTabla = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.dataSourceCargoTabla.sort = this.cargoSort;
    this.dataSourceCategoriaTabla.sort = this.categoriaSort;
    this.dataSourceAreaTabla.sort = this.areaSort;
    this.dataSourceLocalTabla.sort = this.localSort;
    this.dataSourceNota.sort = this.notaSort;
    this.dataSourceDetalleEvaluaciones.sort = this.detallEvalSort;
    this.filtroInicial.idYear = 1;
    this.columnaTableMonitoreo();
    this.columnaTableNotas();
    this.columnaTableDetalles();
    this.formularioFiltro();
    this.formularioNotas();
    this.formularioDetalle();
    this.cargarCombos();

    this.dataSourceCargoTabla.filterPredicate =
      (data: CargoDetalle, filter: string) => (data.descripcion.indexOf(filter) != -1);

    this.dataSourceCategoriaTabla.filterPredicate =
      (data: any, filter: string) => (data.nombreCompleto.indexOf(filter) != -1);

    this.dataSourceAreaTabla.filterPredicate =
      (data: any, filter: string) => (data.nombreCompleto.indexOf(filter) != -1);

    this.dataSourceLocalTabla.filterPredicate =
      (data: any, filter: string) => (data.nombreCompleto.indexOf(filter) != -1);

    this.dataSourceNota.filterPredicate =
      (data: any, filter: string) => (data.nombreCompleto.indexOf(filter) != -1);

    this.dataSourceDetalleEvaluaciones.filterPredicate =
      (data: any, filter: string) => (data.nombreCompleto.indexOf(filter) != -1);



  }

  cargarCombos() {
    this.itemService.listarYears().subscribe(data => {
      debugger;
      this.listYears = data.response;
      this.buscarBalotarioYear(this.listYears[0].cidNombre);
    });
  }

  buscarBalotarioYear(balotarioYear: number) {
    this.itemService.comboBalotarioYear(balotarioYear).subscribe(data => {
      this.listBalotarioCombo = data.response;
      console.log(this.listBalotarioCombo);
      // if (this.filtro.idYear == 1){
      debugger;
      this.filtroInicial.idBalotario = this.listBalotarioCombo[0].idCodigo;
      this.filtro.idBalotario = this.filtroInicial.idBalotario;
      this.nombreBalotario = this.listBalotarioCombo[0].cidNombre;
      this.tabCargoDetalleTabla();
      this.obtenerNotas();
        this.detalleEvaluaciones();
        this.tabCargoCabecera();
        this.tabCategoriaCabecera();
        this.tabCategoriaDetalleTabla();
        this.tabAreaCabecera();
        this.tabAreaDetalleTabla();
        this.tabLocalCabecera();
        this.tabLocalDetalleTabla();


      //this.obtenerBalotario(this.filtroInicial.idBalotario);
      //}
    });
  }

  async tabCargoCabecera() {
    debugger;
    let codBalotario = this.filtro.idBalotario;
    let idCategoria = (typeof this.filtro.idCategoria === 'undefined') || (typeof this.filtro.idCategoria[0] === 'undefined') ? 0 : this.filtro.idCategoria[0];
    let idArea = (typeof this.filtro.idArea === 'undefined')|| (typeof this.filtro.idArea[0] === 'undefined') ? 0 : this.filtro.idArea[0];
    let idLocal = (typeof this.filtro.idLocal === 'undefined') || (typeof this.filtro.idLocal[0] === 'undefined') ? 0 : this.filtro.idLocal[0];

    await this.balotarioService.tabCargoCabecera(codBalotario, idCategoria, idArea, idLocal)
      .then(
        (wsResponseTabCargoCabecera: any) => {

          if (wsResponseTabCargoCabecera.codResultado == 1) {

            this.tabCargoCabeceraData = wsResponseTabCargoCabecera.response;


            this.cantCargo = this.tabCargoCabeceraData[0].total;
            this.realizada = this.tabCargoCabeceraData[0].realizada;
            this.porComenzar = this.tabCargoCabeceraData[0].porComenzar;
            this.fechaCierre = this.tabCargoCabeceraData[0].fechaCierre;

            this.valorInicial = this.tabCargoCabeceraData[0].porcentajeVerde;
            this.totalVerde = this.tabCargoCabeceraData[0].porcentajeVerde;
            this.totalRojo = this.tabCargoCabeceraData[0].porcentajeRojo;
            this.pintarBarraCabeceraCargo();

          }
          else {
            this.mensaje2 = MENSAJES.ERROR_NOFUNCION;
            console.log(this.mensaje2)
          }
        },
        error => {

          this.mensaje2 = MENSAJES.ERROR_SERVICIO;
          console.log(this.mensaje2)
        }

      );

  }

  public cargarTablaCargoDetalle(): void {

    if (this.tabCargoDetalleData != null && this.tabCargoDetalleData.length > 0) {
      this.cantidadCargos = this.tabCargoDetalleData.length;
      this.tabCargoDetalleData.forEach((element) => {
 /*        element.totalRealizada = 1;
        element.totalBalotario = 1000; */
        debugger;
        let valorInicialGrilla: number = ((element.totalRealizada * 100) / element.totalBalotario);
        let totalVerdeGrilla: number = (element.totalRealizada * 100) / element.totalBalotario;
        let totalRojoGrilla: number = ((element.totalBalotario - element.totalRealizada) * 100) / element.totalBalotario;
        let barra_rojoGrilla: number;
        let barra_verdeGrilla: number;


        element.valorInicialGrilla = valorInicialGrilla;
        element.totalVerdeGrilla = totalVerdeGrilla;
        element.totalRojoGrilla = totalRojoGrilla;


     /*    if (element.totalRojoGrilla == 100) {
          element.barraCaso1 = false;
          element.barraCaso2 = true;
          element.barraCaso3 = false;
          valorInicialGrilla = 100;
          totalVerdeGrilla = 100;
          totalRojoGrilla = 0;

        }
        else if (element.totalVerdeGrilla == 0) {
          element.barraCaso1 = true;
          element.barraCaso2 = false;
          element.barraCaso3 = false;
          valorInicialGrilla = 0;
          totalVerdeGrilla = 0;
          totalRojoGrilla = 100;
        } */


        if (element.totalVerdeGrilla == 100) {
          element.barraCaso1 = true;
          element.barraCaso2 = false;
          element.barraCaso3 = false;
        }
        else if (element.totalRojoGrilla == 100) {

          element.barraCaso1 = false;
          element.barraCaso2 = true;
          element.barraCaso3 = false;

        }
        else if (element.totalRealizada != element.totalBalotario) {

          element.barraCaso1 = false;
          element.barraCaso2 = false;
          element.barraCaso3 = true;
        }


        if (valorInicialGrilla <= 100 && valorInicialGrilla > 90) {
          barra_rojoGrilla = 96;
          barra_verdeGrilla = valorInicialGrilla / 2;
        }
        else if (valorInicialGrilla <= 90 && valorInicialGrilla > 70) {
          barra_rojoGrilla = (100 - totalRojoGrilla / 1.8);
          barra_verdeGrilla = valorInicialGrilla / 2;
        }
        else if (valorInicialGrilla <= 70 && valorInicialGrilla > 50) {
          barra_rojoGrilla = (100 - totalRojoGrilla / 1.8);
          barra_verdeGrilla = valorInicialGrilla / 2;
        } else if (valorInicialGrilla <= 50 && valorInicialGrilla > 30) {
          barra_rojoGrilla = (100 - totalRojoGrilla / 2);
          barra_verdeGrilla = valorInicialGrilla / 2;
        } else if (valorInicialGrilla <= 30 && valorInicialGrilla > 10) {
          barra_rojoGrilla = (100 - totalRojoGrilla / 2);
          barra_verdeGrilla = valorInicialGrilla / 3;
        } else if (valorInicialGrilla <= 10 && valorInicialGrilla >= 0) {
          barra_rojoGrilla = (100 - totalRojoGrilla / 2);
          barra_verdeGrilla = valorInicialGrilla / 4;
        }

        element.barra_rojoGrilla = barra_rojoGrilla;
        element.barra_verdeGrilla = barra_verdeGrilla;
      });

      console.log(this.tabCargoDetalleData);
      this.dataSourceCargoTabla = new MatTableDataSource(this.tabCargoDetalleData);
      this.dataSourceCargoTabla.paginator = this.paginatorCargo;
      this.dataSourceCargoTabla.sort = this.cargoSort;
      console.log("tabla -> " + this.dataSourceCargoTabla);
    }
    else {
      this.cantidadCargos = 0;
      this.tabCargoDetalleData = [];
      this.dataSourceCargoTabla = new MatTableDataSource(this.tabCargoDetalleData);
      this.dataSourceCargoTabla.paginator = this.paginatorCargo;
      this.dataSourceCargoTabla.sort = this.cargoSort;
    }
  }

  async tabCargoDetalleTabla() {
    debugger;
    this.cantidadCargos = 0;
    this.tabCargoDetalleData = [];
    console.log(this.filtro)
    let codBalotario = this.filtro.idBalotario;
    let idCategoria = (typeof this.filtro.idCategoria === 'undefined') || (typeof this.filtro.idCategoria[0] === 'undefined') ? 0 : this.filtro.idCategoria[0];
    let idArea = (typeof this.filtro.idArea === 'undefined')|| (typeof this.filtro.idArea[0] === 'undefined') ? 0 : this.filtro.idArea[0];
    let idLocal = (typeof this.filtro.idLocal === 'undefined') || (typeof this.filtro.idLocal[0] === 'undefined') ? 0 : this.filtro.idLocal[0];

    await this.balotarioService.tabCargoDetalleTabla(codBalotario, idCategoria, idArea, idLocal)
      .then(
        (wsResponseTabCargoDetalle: any) => {
          if (wsResponseTabCargoDetalle.codResultado == 1) {
            this.tabCargoDetalleData = wsResponseTabCargoDetalle.response;
            console.log(this.tabCargoDetalleData);
            this.cargarTablaCargoDetalle();
          }
          else {
            this.mensaje2 = MENSAJES.ERROR_NOFUNCION;

            this.cargarTablaCargoDetalle();
          }
        },
        error => {

          this.mensaje2 = MENSAJES.ERROR_SERVICIO;

        }

      );

  }

  async tabCategoriaCabecera() {
    debugger;
    let codBalotario = this.filtro.idBalotario;
    let idCategoria = (typeof this.filtro.idCategoria === 'undefined') || (typeof this.filtro.idCategoria[0] === 'undefined') ? 0 : this.filtro.idCategoria[0];
    let idArea = (typeof this.filtro.idArea === 'undefined')|| (typeof this.filtro.idArea[0] === 'undefined') ? 0 : this.filtro.idArea[0];
    let idLocal = (typeof this.filtro.idLocal === 'undefined') || (typeof this.filtro.idLocal[0] === 'undefined') ? 0 : this.filtro.idLocal[0];

    await this.balotarioService.tabCategoriaCabecera(codBalotario, idCategoria, idArea, idLocal)
      .then(
        (wsResponseTabCategoriaCabecera: any) => {

          if (wsResponseTabCategoriaCabecera.codResultado == 1) {

            this.tabCategoriaCabeceraData = wsResponseTabCategoriaCabecera.response;

            this.cantCategoria = this.tabCategoriaCabeceraData[0].total;
            this.realizadaCategoria = this.tabCategoriaCabeceraData[0].realizada;
            this.porComenzarCategoria = this.tabCategoriaCabeceraData[0].porComenzar;
            this.fechaCierreCategoria = this.tabCategoriaCabeceraData[0].fechaCierre;

            this.valorInicialCat = this.tabCategoriaCabeceraData[0].porcentajeVerde;
            this.totalVerdeCat = this.tabCategoriaCabeceraData[0].porcentajeVerde;
            this.totalRojoCat = this.tabCategoriaCabeceraData[0].porcentajeRojo;
            this.pintarBarraCabeceraCategoria();

          }
          else {
            this.mensaje2 = MENSAJES.ERROR_NOFUNCION;
            console.log(this.mensaje2)
          }
        },
        error => {

          this.mensaje2 = MENSAJES.ERROR_SERVICIO;
          console.log(this.mensaje2)
        }

      );

  }

  async tabCategoriaDetalleTabla() {
    debugger;
    this.cantidadCategoria = 0;
    this.tabCategoriaDetalleData = [];
    console.log(this.filtro)
    let codBalotario = this.filtro.idBalotario;
    let idCategoria = (typeof this.filtro.idCategoria === 'undefined') || (typeof this.filtro.idCategoria[0] === 'undefined') ? 0 : this.filtro.idCategoria[0];
    let idArea = (typeof this.filtro.idArea === 'undefined')|| (typeof this.filtro.idArea[0] === 'undefined') ? 0 : this.filtro.idArea[0];
    let idLocal = (typeof this.filtro.idLocal === 'undefined') || (typeof this.filtro.idLocal[0] === 'undefined') ? 0 : this.filtro.idLocal[0];

    await this.balotarioService.tabCategoriaDetalleTabla(codBalotario, idCategoria, idArea, idLocal)
      .then(
        (wsResponseTabCategoriaDetalle: any) => {
          if (wsResponseTabCategoriaDetalle.codResultado == 1) {
            this.tabCategoriaDetalleData = wsResponseTabCategoriaDetalle.response;
            console.log(this.tabCategoriaDetalleData);
            this.cargarTablaCategoriaDetalle();
          }
          else {
            this.mensaje2 = MENSAJES.ERROR_NOFUNCION;

            this.cargarTablaCategoriaDetalle();
          }
        },
        error => {

          this.mensaje2 = MENSAJES.ERROR_SERVICIO;

        }

      );

  }

  public cargarTablaCategoriaDetalle(): void {

    if (this.tabCategoriaDetalleData != null && this.tabCategoriaDetalleData.length > 0) {
      this.cantCategoria = this.tabCategoriaDetalleData.length;
      this.tabCategoriaDetalleData.forEach((element) => {

        let valorInicialGrillaCat: number = ((element.totalRealizada * 100) / element.totalBalotario);
        let totalVerdeGrillaCat: number = (element.totalRealizada * 100) / element.totalBalotario;
        let totalRojoGrillaCat: number = ((element.totalBalotario - element.totalRealizada) * 100) / element.totalBalotario;
        let barra_rojoGrillaCat: number;
        let barra_verdeGrillaCat: number;


        element.valorInicialGrilla = Math.round(valorInicialGrillaCat);
        element.totalVerdeGrilla = Math.round(totalVerdeGrillaCat);
        element.totalRojoGrilla = Math.round(totalRojoGrillaCat);


        if (element.totalRojoGrilla == 100) {
          element.barraCaso1 = false;
          element.barraCaso2 = true;
          element.barraCaso3 = false;
          valorInicialGrillaCat = 100;
          totalVerdeGrillaCat = 100;
          totalRojoGrillaCat = 0;

        }
        else if (element.totalVerdeGrilla == 0) {
          element.barraCaso1 = true;
          element.barraCaso2 = false;
          element.barraCaso3 = false;
          valorInicialGrillaCat = 0;
          totalVerdeGrillaCat = 0;
          totalRojoGrillaCat = 100;
        }


        if (element.totalVerdeGrilla == 100) {
          element.barraCaso1 = true;
          element.barraCaso2 = false;
          element.barraCaso3 = false;
        }
        else if (element.totalRojoGrilla == 100) {

          element.barraCaso1 = false;
          element.barraCaso2 = true;
          element.barraCaso3 = false;

        }
        else if (element.totalRealizada != element.totalBalotario) {

          element.barraCaso1 = false;
          element.barraCaso2 = false;
          element.barraCaso3 = true;
        }

        if (valorInicialGrillaCat <= 100 && valorInicialGrillaCat > 90) {
          barra_rojoGrillaCat = 96;
          barra_verdeGrillaCat = valorInicialGrillaCat / 2;
        }
        else if (valorInicialGrillaCat <= 90 && valorInicialGrillaCat > 70) {
          barra_rojoGrillaCat = (100 - totalRojoGrillaCat / 1.8);
          barra_verdeGrillaCat = valorInicialGrillaCat / 2;
        }
        else if (valorInicialGrillaCat <= 70 && valorInicialGrillaCat > 50) {
          barra_rojoGrillaCat = (100 - totalRojoGrillaCat / 1.8);
          barra_verdeGrillaCat = valorInicialGrillaCat / 2;
        } else if (valorInicialGrillaCat <= 50 && valorInicialGrillaCat > 30) {
          barra_rojoGrillaCat = (100 - totalRojoGrillaCat / 2);
          barra_verdeGrillaCat = valorInicialGrillaCat / 2;
        } else if (valorInicialGrillaCat <= 30 && valorInicialGrillaCat > 10) {
          barra_rojoGrillaCat = (100 - totalRojoGrillaCat / 2);
          barra_verdeGrillaCat = valorInicialGrillaCat / 3;

        } else if (valorInicialGrillaCat <= 10 && valorInicialGrillaCat >= 0) {
          barra_rojoGrillaCat = (100 - totalRojoGrillaCat / 2);
          barra_verdeGrillaCat = valorInicialGrillaCat / 4;
        }

        else {
          barra_rojoGrillaCat = (95 - valorInicialGrillaCat / 2);
          barra_verdeGrillaCat = valorInicialGrillaCat / 2;
        }
        element.barra_rojoGrilla = barra_rojoGrillaCat;
        element.barra_verdeGrilla = barra_verdeGrillaCat;
      });

      console.log(this.tabCategoriaDetalleData);
      this.dataSourceCategoriaTabla = new MatTableDataSource(this.tabCategoriaDetalleData);
      this.dataSourceCategoriaTabla.paginator = this.paginatorCategoria;
      this.dataSourceCategoriaTabla.sort = this.categoriaSort;
      console.log("tabla -> " + this.dataSourceCategoriaTabla);
    }
    else {
      this.cantidadCargos = 0;
      this.tabCategoriaDetalleData = [];
      this.dataSourceCategoriaTabla = new MatTableDataSource(this.tabCategoriaDetalleData);
      this.dataSourceCategoriaTabla.paginator = this.paginatorCategoria;
      this.dataSourceCategoriaTabla.sort = this.categoriaSort;
    }
  }

  async tabAreaCabecera() {
    debugger;
    let codBalotario = this.filtro.idBalotario;
    let idCategoria = (typeof this.filtro.idCategoria === 'undefined') || (typeof this.filtro.idCategoria[0] === 'undefined') ? 0 : this.filtro.idCategoria[0];
    let idArea = (typeof this.filtro.idArea === 'undefined')|| (typeof this.filtro.idArea[0] === 'undefined') ? 0 : this.filtro.idArea[0];
    let idLocal = (typeof this.filtro.idLocal === 'undefined') || (typeof this.filtro.idLocal[0] === 'undefined') ? 0 : this.filtro.idLocal[0];

    await this.balotarioService.tabAreaCabecera(codBalotario, idCategoria, idArea, idLocal)
      .then(
        (wsResponseTabAreaCabecera: any) => {

          if (wsResponseTabAreaCabecera.codResultado == 1) {

            this.tabAreaCabeceraData = wsResponseTabAreaCabecera.response;

            this.cantArea = this.tabAreaCabeceraData[0].total;
            this.realizadaArea = this.tabAreaCabeceraData[0].realizada;
            this.porComenzarArea = this.tabAreaCabeceraData[0].porComenzar;
            this.fechaCierreArea = this.tabAreaCabeceraData[0].fechaCierre;

            this.valorInicialArea = this.tabAreaCabeceraData[0].porcentajeVerde;
            this.totalVerdeArea = this.tabAreaCabeceraData[0].porcentajeVerde;
            this.totalRojoArea = this.tabAreaCabeceraData[0].porcentajeRojo;
            this.pintarBarraCabeceraArea();

          }
          else {
            this.mensaje2 = MENSAJES.ERROR_NOFUNCION;
            console.log(this.mensaje2)
          }
        },
        error => {

          this.mensaje2 = MENSAJES.ERROR_SERVICIO;
          console.log(this.mensaje2)
        }

      );

  }

  async tabAreaDetalleTabla() {
    debugger;
    this.cantArea = 0;
    this.tabAreaDetalleData = [];
    console.log(this.filtro)
    let codBalotario = this.filtro.idBalotario;
    let idCategoria = (typeof this.filtro.idCategoria === 'undefined') || (typeof this.filtro.idCategoria[0] === 'undefined') ? 0 : this.filtro.idCategoria[0];
    let idArea = (typeof this.filtro.idArea === 'undefined')|| (typeof this.filtro.idArea[0] === 'undefined') ? 0 : this.filtro.idArea[0];
    let idLocal = (typeof this.filtro.idLocal === 'undefined') || (typeof this.filtro.idLocal[0] === 'undefined') ? 0 : this.filtro.idLocal[0];

    await this.balotarioService.tabAreaDetalleTabla(codBalotario, idCategoria, idArea, idLocal)
      .then(
        (wsResponseTabAreaDetalle: any) => {
          if (wsResponseTabAreaDetalle.codResultado == 1) {
            this.tabAreaDetalleData = wsResponseTabAreaDetalle.response;
            console.log(this.tabAreaDetalleData);
            this.cargarTablaAreaDetalle();
          }
          else {
            this.mensaje2 = MENSAJES.ERROR_NOFUNCION;

            this.cargarTablaAreaDetalle();
          }
        },
        error => {

          this.mensaje2 = MENSAJES.ERROR_SERVICIO;

        }

      );

  }

  public cargarTablaAreaDetalle(): void {
    if (this.tabAreaDetalleData != null && this.tabAreaDetalleData.length > 0) {
      this.cantArea = this.tabAreaDetalleData.length;
      this.tabAreaDetalleData.forEach((element) => {
        let valorInicialGrillaArea: number = ((element.totalRealizada * 100) / element.totalBalotario);
        let totalVerdeGrillaArea: number = (element.totalRealizada * 100) / element.totalBalotario;
        let totalRojoGrillaArea: number = ((element.totalBalotario - element.totalRealizada) * 100) / element.totalBalotario;
        let barra_rojoGrillaArea: number;
        let barra_verdeGrillaArea: number;

        element.valorInicialGrilla = Math.round(valorInicialGrillaArea);
        element.totalVerdeGrilla = Math.round(totalVerdeGrillaArea);
        element.totalRojoGrilla = Math.round(totalRojoGrillaArea);


        if (element.totalRojoGrilla == 100) {
          element.barraCaso1 = false;
          element.barraCaso2 = true;
          element.barraCaso3 = false;
          valorInicialGrillaArea = 100;
          totalVerdeGrillaArea = 100;
          totalRojoGrillaArea = 0;

        }
        else if (element.totalVerdeGrilla == 0) {
          element.barraCaso1 = true;
          element.barraCaso2 = false;
          element.barraCaso3 = false;
          valorInicialGrillaArea = 0;
          totalVerdeGrillaArea = 0;
          totalRojoGrillaArea = 100;
        }


        if (element.totalVerdeGrilla == 100) {
          element.barraCaso1 = true;
          element.barraCaso2 = false;
          element.barraCaso3 = false;
        }
        else if (element.totalRojoGrilla == 100) {

          element.barraCaso1 = false;
          element.barraCaso2 = true;
          element.barraCaso3 = false;

        }
        else if (element.totalRealizada != element.totalBalotario) {

          element.barraCaso1 = false;
          element.barraCaso2 = false;
          element.barraCaso3 = true;
        }

        if (valorInicialGrillaArea <= 100 && valorInicialGrillaArea > 90) {
          barra_rojoGrillaArea = 96;
          barra_verdeGrillaArea = valorInicialGrillaArea / 2;
        }
        else if (valorInicialGrillaArea <= 90 && valorInicialGrillaArea > 70) {
          barra_rojoGrillaArea = (100 - totalRojoGrillaArea / 1.8);
          barra_verdeGrillaArea = valorInicialGrillaArea / 2;
        }
        else if (valorInicialGrillaArea <= 70 && valorInicialGrillaArea > 50) {
          barra_rojoGrillaArea = (100 - totalRojoGrillaArea / 1.8);
          barra_verdeGrillaArea = valorInicialGrillaArea / 2;
        } else if (valorInicialGrillaArea <= 50 && valorInicialGrillaArea > 30) {
          barra_rojoGrillaArea = (100 - totalRojoGrillaArea / 2);
          barra_verdeGrillaArea = valorInicialGrillaArea / 2;
        } else if (valorInicialGrillaArea <= 30 && valorInicialGrillaArea > 10) {
          barra_rojoGrillaArea = (100 - totalRojoGrillaArea / 2);
          barra_verdeGrillaArea = valorInicialGrillaArea / 3;

        } else if (valorInicialGrillaArea <= 10 && valorInicialGrillaArea >= 0) {
          barra_rojoGrillaArea = (100 - totalRojoGrillaArea / 2);
          barra_verdeGrillaArea = valorInicialGrillaArea / 4;
        }

        else {
          barra_rojoGrillaArea = (95 - valorInicialGrillaArea / 2);
          barra_verdeGrillaArea = this.valorInicialGrillaArea / 2;
        }
        element.barra_rojoGrilla = barra_rojoGrillaArea;
        element.barra_verdeGrilla = barra_verdeGrillaArea;
      });

      console.log(this.tabAreaDetalleData);
      this.dataSourceAreaTabla = new MatTableDataSource(this.tabAreaDetalleData);
      this.dataSourceAreaTabla.paginator = this.paginatorArea;
      this.dataSourceAreaTabla.sort = this.areaSort;
      console.log("tabla -> " + this.dataSourceAreaTabla);
    }
    else {
      this.cantArea = 0;
      this.tabAreaDetalleData = [];
      this.dataSourceAreaTabla = new MatTableDataSource(this.tabAreaDetalleData);
      this.dataSourceAreaTabla.paginator = this.paginatorArea;
      this.dataSourceAreaTabla.sort = this.areaSort;
    }
  }

  async tabLocalCabecera() {
    debugger;
    let codBalotario = this.filtro.idBalotario;
    let idCategoria = (typeof this.filtro.idCategoria === 'undefined') || (typeof this.filtro.idCategoria[0] === 'undefined') ? 0 : this.filtro.idCategoria[0];
    let idArea = (typeof this.filtro.idArea === 'undefined')|| (typeof this.filtro.idArea[0] === 'undefined') ? 0 : this.filtro.idArea[0];
    let idLocal = (typeof this.filtro.idLocal === 'undefined') || (typeof this.filtro.idLocal[0] === 'undefined') ? 0 : this.filtro.idLocal[0];

    await this.balotarioService.tabLocalCabecera(codBalotario, idCategoria, idArea, idLocal)
      .then(
        (wsResponseTabLocalCabecera: any) => {

          if (wsResponseTabLocalCabecera.codResultado == 1) {

            this.tabLocalCabeceraData = wsResponseTabLocalCabecera.response;

            this.cantLocal = this.tabLocalCabeceraData[0].total;
            this.realizadaLocal = this.tabLocalCabeceraData[0].realizada;
            this.porComenzarLocal = this.tabLocalCabeceraData[0].porComenzar;
            this.fechaCierreLocal = this.tabLocalCabeceraData[0].fechaCierre;

            this.valorInicialLocal = this.tabLocalCabeceraData[0].porcentajeVerde;
            this.totalVerdeLocal = this.tabLocalCabeceraData[0].porcentajeVerde;
            this.totalRojoLocal = this.tabLocalCabeceraData[0].porcentajeRojo;
            this.pintarBarraCabeceraLocal();

          }
          else {
            this.mensaje2 = MENSAJES.ERROR_NOFUNCION;
            console.log(this.mensaje2)
          }
        },
        error => {

          this.mensaje2 = MENSAJES.ERROR_SERVICIO;
          console.log(this.mensaje2)
        }

      );

  }

  async tabLocalDetalleTabla() {
    debugger;
    this.cantLocal = 0;
    this.tabLocalDetalleData = [];
    console.log(this.filtro)
    let codBalotario = this.filtro.idBalotario;
    let idCategoria = (typeof this.filtro.idCategoria === 'undefined') || (typeof this.filtro.idCategoria[0] === 'undefined') ? 0 : this.filtro.idCategoria[0];
    let idArea = (typeof this.filtro.idArea === 'undefined')|| (typeof this.filtro.idArea[0] === 'undefined') ? 0 : this.filtro.idArea[0];
    let idLocal = (typeof this.filtro.idLocal === 'undefined') || (typeof this.filtro.idLocal[0] === 'undefined') ? 0 : this.filtro.idLocal[0];

    await this.balotarioService.tabLocalDetalleTabla(codBalotario, idCategoria, idArea, idLocal)
      .then(
        (wsResponseTabLocalDetalle: any) => {
          if (wsResponseTabLocalDetalle.codResultado == 1) {
            this.tabLocalDetalleData = wsResponseTabLocalDetalle.response;
            console.log(this.tabLocalDetalleData);
            this.cargarTablaLocalDetalle();
          }
          else {
            this.mensaje2 = MENSAJES.ERROR_NOFUNCION;

            this.cargarTablaLocalDetalle();
          }
        },
        error => {

          this.mensaje2 = MENSAJES.ERROR_SERVICIO;

        }

      );

  }

  public cargarTablaLocalDetalle(): void {

    if (this.tabLocalDetalleData != null && this.tabLocalDetalleData.length > 0) {
      this.cantLocal = this.tabLocalDetalleData.length;
      this.tabLocalDetalleData.forEach((element) => {

        let valorInicialGrillaLocal: number = ((element.totalRealizada * 100) / element.totalBalotario);
        let totalVerdeGrillaLocal: number = (element.totalRealizada * 100) / element.totalBalotario;
        let totalRojoGrillaLocal: number = ((element.totalBalotario - element.totalRealizada) * 100) / element.totalBalotario;
        let barra_rojoGrillaLocal: number;
        let barra_verdeGrillaLocal: number;


        element.valorInicialGrilla = Math.round(valorInicialGrillaLocal);
        element.totalVerdeGrilla = Math.round(totalVerdeGrillaLocal);
        element.totalRojoGrilla = Math.round(totalRojoGrillaLocal);


        if (element.totalRojoGrilla == 100) {
          element.barraCaso1 = false;
          element.barraCaso2 = true;
          element.barraCaso3 = false;
          valorInicialGrillaLocal = 100;
          totalVerdeGrillaLocal = 100;
          totalRojoGrillaLocal = 0;

        }
        else if (element.totalVerdeGrilla == 0) {
          element.barraCaso1 = true;
          element.barraCaso2 = false;
          element.barraCaso3 = false;
          valorInicialGrillaLocal = 0;
          totalVerdeGrillaLocal = 0;
          totalRojoGrillaLocal = 100;
        }


        if (element.totalVerdeGrilla == 100) {
          element.barraCaso1 = true;
          element.barraCaso2 = false;
          element.barraCaso3 = false;
        }
        else if (element.totalRojoGrilla == 100) {

          element.barraCaso1 = false;
          element.barraCaso2 = true;
          element.barraCaso3 = false;

        }
        else if (element.totalRealizada != element.totalBalotario) {

          element.barraCaso1 = false;
          element.barraCaso2 = false;
          element.barraCaso3 = true;
        }

        if (valorInicialGrillaLocal <= 100 && valorInicialGrillaLocal > 90) {
          barra_rojoGrillaLocal = 96;
          barra_verdeGrillaLocal = valorInicialGrillaLocal / 2;
        }
        else if (valorInicialGrillaLocal <= 90 && valorInicialGrillaLocal > 70) {
          barra_rojoGrillaLocal = (100 - totalRojoGrillaLocal / 1.8);
          barra_verdeGrillaLocal = valorInicialGrillaLocal / 2;
        }
        else if (valorInicialGrillaLocal <= 70 && valorInicialGrillaLocal > 50) {
          barra_rojoGrillaLocal = (100 - totalRojoGrillaLocal / 1.8);
          barra_verdeGrillaLocal = valorInicialGrillaLocal / 2;
        } else if (valorInicialGrillaLocal <= 50 && valorInicialGrillaLocal > 30) {
          barra_rojoGrillaLocal = (100 - totalRojoGrillaLocal / 2);
          barra_verdeGrillaLocal = valorInicialGrillaLocal / 2;
        } else if (valorInicialGrillaLocal <= 30 && valorInicialGrillaLocal > 10) {
          barra_rojoGrillaLocal = (100 - totalRojoGrillaLocal / 2);
          barra_verdeGrillaLocal = valorInicialGrillaLocal / 3;

        } else if (valorInicialGrillaLocal <= 10 && valorInicialGrillaLocal >= 0) {
          barra_rojoGrillaLocal = (100 - totalRojoGrillaLocal / 2);
          barra_verdeGrillaLocal = valorInicialGrillaLocal / 4;
        }

        else {
          barra_rojoGrillaLocal = (95 - valorInicialGrillaLocal / 2);
          barra_verdeGrillaLocal = this.valorInicialGrillaLocal / 2;
        }
        element.barra_rojoGrilla = barra_rojoGrillaLocal;
        element.barra_verdeGrilla = barra_verdeGrillaLocal;
      });

      console.log(this.tabLocalDetalleData);
      this.dataSourceLocalTabla = new MatTableDataSource(this.tabLocalDetalleData);
      this.dataSourceLocalTabla.paginator = this.paginatorLocal;
      this.dataSourceLocalTabla.sort = this.localSort;
      console.log("tabla -> " + this.dataSourceLocalTabla);
    }
    else {
      this.cantLocal = 0;
      this.tabLocalDetalleData = [];
      this.dataSourceLocalTabla = new MatTableDataSource(this.tabLocalDetalleData);
      this.dataSourceLocalTabla.paginator = this.paginatorLocal;
      this.dataSourceLocalTabla.sort = this.localSort;
    }

  }

  pintarBarraCabeceraCargo() {
    /*  this.valorInicial = 20;
      this.totalVerde = 20;
      this.totalRojo = 80;*/

    if (this.totalRojo == 100) {
      this.cienPorcientoVerde = false;
    } else {
      this.cienPorcientoVerde = true;
    }

    if (this.totalVerde == 100) {
      this.cienPorcientoRojo = false;
    } else {
      this.cienPorcientoRojo = true;
    }

    if (this.valorInicial <= 100 && this.valorInicial > 97) {
      this.barra_rojo = 95;
      this.barra_verde = this.valorInicial / 2;
    }
    else if (this.valorInicial <= 97 && this.valorInicial > 95) {
      this.barra_rojo = 94;
      this.barra_verde = this.valorInicial / 2;
    }
    else if (this.valorInicial <= 95 && this.valorInicial > 90) {
      this.barra_rojo = 92;
      this.barra_verde = this.valorInicial / 2;
    }
    else if (this.valorInicial <= 90 && this.valorInicial > 80) {
      this.barra_rojo = 88;
      this.barra_verde = this.valorInicial / 2;
    }
    else if (this.valorInicial <= 80 && this.valorInicial > 70) {
      this.barra_rojo = 84;
      this.barra_verde = this.valorInicial / 2;
    }
    else if (this.valorInicial <= 70 && this.valorInicial > 50) {
      this.barra_rojo = (100 - this.totalRojo / 1.8);
      this.barra_verde = this.valorInicial / 2;
    }
    else if (this.valorInicial <= 50 && this.valorInicial >= 30) {
      this.barra_rojo = (100 - this.totalRojo / 2);
      this.barra_verde = this.valorInicial / 2;
    } else if (this.valorInicial <= 30 && this.valorInicial > 10) {
      this.barra_rojo = (100 - this.totalRojo / 2);
      this.barra_verde = this.valorInicial / 3;

    } else if (this.valorInicial <= 10 && this.valorInicial >= 0) {
      this.barra_rojo = (100 - this.totalRojo / 2);
      this.barra_verde = this.valorInicial / 4;
    }
    else {
      this.barra_rojo = (95 - this.valorInicial / 2);
      this.barra_verde = this.valorInicial / 2;
    }
  }

  pintarBarraCabeceraCategoria() {
    //this.valorInicialCat = 20;
    //this.totalVerdeCat = 20;
    //this.totalRojoCat = 80;

    if (this.totalRojoCat == 100) {
      this.cienPorcientoVerdeCat = false;
    } else {
      this.cienPorcientoVerdeCat = true;
    }

    if (this.totalVerdeCat == 100) {
      this.cienPorcientoRojoCat = false;
    } else {
      this.cienPorcientoRojoCat = true;
    }

    if (this.valorInicialCat <= 100 && this.valorInicialCat > 97) {
      this.barra_rojoCat = 95;
      this.barra_verdeCat = this.valorInicialCat / 2;
    }
    else if (this.valorInicialCat <= 97 && this.valorInicialCat > 95) {
      this.barra_rojoCat = 94;
      this.barra_verdeCat = this.valorInicialCat / 2;
    }
    else if (this.valorInicialCat <= 95 && this.valorInicialCat > 90) {
      this.barra_rojoCat = 92;
      this.barra_verdeCat = this.valorInicialCat / 2;
    }
    else if (this.valorInicialCat <= 90 && this.valorInicialCat > 80) {
      this.barra_rojoCat = 88;
      this.barra_verdeCat = this.valorInicialCat / 2;
    }
    else if (this.valorInicialCat <= 80 && this.valorInicialCat > 70) {
      this.barra_rojoCat = 84;
      this.barra_verdeCat = this.valorInicialCat / 2;
    }
    else if (this.valorInicialCat <= 70 && this.valorInicialCat > 50) {
      this.barra_rojoCat = (100 - this.totalRojoCat / 1.8);
      this.barra_verdeCat = this.valorInicialCat / 2;
    }
    else if (this.valorInicialCat <= 50 && this.valorInicial >= 30) {
      this.barra_rojoCat = (100 - this.totalRojoCat / 2);
      this.barra_verdeCat = this.valorInicialCat / 2;
    } else if (this.valorInicialCat <= 30 && this.valorInicialCat > 10) {
      this.barra_rojoCat = (100 - this.totalRojoCat / 2);
      this.barra_verdeCat = this.valorInicialCat / 3;

    } else if (this.valorInicialCat <= 10 && this.valorInicialCat >= 0) {
      this.barra_rojoCat = (100 - this.totalRojoCat / 2);
      this.barra_verdeCat = this.valorInicialCat / 4;
    }
    else {
      this.barra_rojoCat = (95 - this.valorInicialCat / 2);
      this.barra_verdeCat = this.valorInicialCat / 2;
    }
  }

  pintarBarraCabeceraArea() {
    //this.valorInicialCat = 20;
    //this.totalVerdeCat = 20;
    //this.totalRojoCat = 80;

    if (this.totalRojoArea == 100) {
      this.cienPorcientoVerdeArea = false;
    } else {
      this.cienPorcientoVerdeArea = true;
    }

    if (this.totalVerdeArea == 100) {
      this.cienPorcientoRojoArea = false;
    } else {
      this.cienPorcientoRojoArea = true;
    }

    if (this.valorInicialArea <= 100 && this.valorInicialArea > 97) {
      this.barra_rojoArea = 95;
      this.barra_verdeArea = this.valorInicialArea / 2;
    }
    else if (this.valorInicialArea <= 97 && this.valorInicialArea > 95) {
      this.barra_rojoArea = 94;
      this.barra_verdeArea = this.valorInicialArea / 2;
    }
    else if (this.valorInicialArea <= 95 && this.valorInicialArea > 90) {
      this.barra_rojoArea = 92;
      this.barra_verdeArea = this.valorInicialArea / 2;
    }
    else if (this.valorInicialArea <= 90 && this.valorInicialArea > 80) {
      this.barra_rojoArea = 88;
      this.barra_verdeArea = this.valorInicialArea / 2;
    }
    else if (this.valorInicialArea <= 80 && this.valorInicialArea > 70) {
      this.barra_rojoArea = 84;
      this.barra_verdeArea = this.valorInicialArea / 2;
    }
    else if (this.valorInicialArea <= 70 && this.valorInicialArea > 50) {
      this.barra_rojoArea = (100 - this.totalRojoArea / 1.8);
      this.barra_verdeArea = this.valorInicialArea / 2;
    }
    else if (this.valorInicialArea <= 50 && this.valorInicialArea >= 30) {
      this.barra_rojoArea = (100 - this.totalRojoArea / 2);
      this.barra_verdeArea = this.valorInicialArea / 2;
    } else if (this.valorInicialArea <= 30 && this.valorInicialArea > 10) {
      this.barra_rojoArea = (100 - this.totalRojoArea / 2);
      this.barra_verdeArea = this.valorInicialArea / 3;

    } else if (this.valorInicialArea <= 10 && this.valorInicialArea >= 0) {
      this.barra_rojoArea = (100 - this.totalRojoArea / 2);
      this.barra_verdeArea = this.valorInicialArea / 4;
    }
    else {
      this.barra_rojoArea = (95 - this.totalRojoArea / 2);
      this.barra_verdeArea = this.valorInicialArea / 2;
    }
  }

  pintarBarraCabeceraLocal() {
    //this.valorInicialLocal = 20;
    //this.totalVerdeLocal = 20;
    //this.totalRojoLocal = 80;

    if (this.totalRojoLocal == 100) {
      this.cienPorcientoVerdeLocal = false;
    } else {
      this.cienPorcientoVerdeLocal = true;
    }

    if (this.totalVerdeLocal == 100) {
      this.cienPorcientoRojoLocal = false;
    } else {
      this.cienPorcientoRojoLocal = true;
    }

    if (this.valorInicialLocal <= 100 && this.valorInicialLocal > 97) {
      this.barra_rojoLocal = 95;
      this.barra_verdeLocal = this.valorInicialLocal / 2;
    }
    else if (this.valorInicialLocal <= 97 && this.valorInicialLocal > 95) {
      this.barra_rojoLocal = 94;
      this.barra_verdeLocal = this.valorInicialLocal / 2;
    }
    else if (this.valorInicialLocal <= 95 && this.valorInicialLocal > 90) {
      this.barra_rojoLocal = 92;
      this.barra_verdeLocal = this.valorInicialLocal / 2;
    }
    else if (this.valorInicialLocal <= 90 && this.valorInicialLocal > 80) {
      this.barra_rojoLocal = 88;
      this.barra_verdeLocal = this.valorInicialLocal / 2;
    }
    else if (this.valorInicialLocal <= 80 && this.valorInicialLocal > 70) {
      this.barra_rojoLocal = 84;
      this.barra_verdeLocal = this.valorInicialLocal / 2;
    }
    else if (this.valorInicialLocal <= 70 && this.valorInicialLocal > 50) {
      this.barra_rojoLocal = (100 - this.totalRojoLocal / 1.8);
      this.barra_verdeLocal = this.valorInicialLocal / 2;
    }
    else if (this.valorInicialLocal <= 50 && this.valorInicial >= 30) {
      this.barra_rojoLocal = (100 - this.totalRojoLocal / 2);
      this.barra_verdeLocal = this.valorInicialLocal / 2;
    } else if (this.valorInicialLocal <= 30 && this.valorInicialLocal > 10) {
      this.barra_rojoLocal = (100 - this.totalRojoLocal / 2);
      this.barra_verdeLocal = this.valorInicialLocal / 3;

    } else if (this.valorInicialLocal <= 10 && this.valorInicialLocal >= 0) {
      this.barra_rojoLocal = (100 - this.totalRojoLocal / 2);
      this.barra_verdeLocal = this.valorInicialLocal / 4;
    }
    else {
      this.barra_rojoLocal = (95 - this.valorInicialLocal / 2);
      this.barra_verdeLocal = this.valorInicialLocal / 2;
    }
  }

  columnaTableMonitoreo(): void {
    this.columnasMonitoreo = ['descripcion', 'totalBalotario', 'avance'];
  }

  columnaTableNotas(): void {
    this.columnasNotas = ['tipoDocumento', 'identificacion', 'nombreCompleto', 'intentos', 'notas', 'desCargo', 'desCategoria', 'desArea', 'desLocal'];
  }

  columnaTableDetalles(): void {
    this.columnasDetalles = ['tipoDocumento','codTrabajador', 'numDocumento', 'nombreCompleto', 'fechaNacimiento','edad','edadRangos','intentoValido','fechaIngreso','tsAnio','tsMes','tsDias','tsRango','genero','fechaBalotario','descripcionBalotario','desCargo', 'desCategoria', 'desArea', 'desLocal', 'intentos', 'tema', 'pregunta', 'respuesta', 'fiCorrecta'];
  }

  formularioFiltro() {
    this.filtrosForm = this.formBuilder.group({
      buscarFrm: ['']
    });
  }

  formularioNotas() {
    this.notasForm = this.formBuilder.group({
      buscarUsuarioNotasFrm: ['']
    });
  }


  formularioDetalle() {
    this.detalleForm = this.formBuilder.group({
      buscarDetalleFrm: ['']
    });
  }

  categoria: string = '';
  filtrarResultados() {
    console.log("Filtroinicial : "+this.filtro);
    debugger
    const dialogReg: MatDialogRef<FiltrarResultadosComponent> = this.dialog.open(FiltrarResultadosComponent, {
      disableClose: true,
      panelClass: 'dialog-no-padding',
      data: {
        filtro: this.filtro
      }
    });
    dialogReg.afterClosed().subscribe((valor: FiltroRequest) => {
      console.log("Valor al seleccionar:"+valor)
      if (valor) {
        debugger;
        this.filtro = valor;
        console.log(" filtro en bandeja al  seleccionar"+this.filtro);
        //this.obtenerBalotario(this.filtro.idBalotario);
        if (typeof this.filtro.descripcionBalotario !== 'undefined')
          this.nombreBalotario = this.filtro.descripcionBalotario;
        this.tabCargoCabecera();
        this.tabCargoDetalleTabla();
        this.tabCategoriaCabecera();
        this.tabCategoriaDetalleTabla();
        this.tabAreaCabecera();
        this.tabAreaDetalleTabla();
        this.tabLocalCabecera();
        this.tabLocalDetalleTabla();
        this.obtenerNotas();
        this.detalleEvaluaciones();
      }
    });
  }



  mensaje: string;
  async obtenerNotas() {
    this.cantidadRegistrosNotas = 0;

    let codBalotario = this.filtro.idBalotario;
    let idCategoria = (typeof this.filtro.idCategoria === 'undefined') || (typeof this.filtro.idCategoria[0] === 'undefined') ? 0 : this.filtro.idCategoria[0];
    let idArea = (typeof this.filtro.idArea === 'undefined')|| (typeof this.filtro.idArea[0] === 'undefined') ? 0 : this.filtro.idArea[0];
    let idLocal = (typeof this.filtro.idLocal === 'undefined') || (typeof this.filtro.idLocal[0] === 'undefined') ? 0 : this.filtro.idLocal[0];

    this.tablaNotasResponse = [];
    await this.balotarioService.obtenerNotas(codBalotario, idCategoria, idArea, idLocal)
      .then(
        (wsResponseProyecto: any) => {

          if (wsResponseProyecto.codResultado == 1) {
            this.tablaNotasResponse = (wsResponseProyecto.response != null) ? wsResponseProyecto.response : [];

            console.log(wsResponseProyecto)
            console.log(this.tablaNotasResponse)

            this.cargarTablaNotas();

          }
          else {
            this.mensaje = MENSAJES.ERROR_NOFUNCION;
            this.cargarTablaNotas();
            // this.openDialogMensaje(null,  wsResponseProyecto.msgResultado, true, false, wsResponseProyecto.codResultado);

          }

        },
        error => {

          this.mensaje = MENSAJES.ERROR_SERVICIO;
          /*   this.openDialogMensaje(this.mensaje, MENSAJES.PREFACTIBILIDAD.TITLE, true, false, null);
            console.error(error);
            this.disableBuscar = false; */
        }

      );
    this.cargarTablaNotas();
  }

  public cargarTablaNotas(): void {
    if (this.tablaNotasResponse != null && this.tablaNotasResponse.length > 0) {
      this.cantidadRegistrosNotas = this.tablaNotasResponse.length;
      this.dataSourceNota = new MatTableDataSource(this.tablaNotasResponse);
      this.dataSourceNota.paginator = this.paginatorNotas;
      this.dataSourceNota.sort = this.notaSort;
      console.log("tabla -> " + this.dataSourceNota);
    }
    else {
      debugger;
      this.cantidadRegistrosNotas = 0;
      this.tablaNotasResponse = [];
      this.dataSourceNota = new MatTableDataSource(this.tablaNotasResponse);
      this.dataSourceNota.paginator = this.paginatorNotas;
      this.dataSourceNota.sort = this.notaSort;
    }
  }

  mensaje2: string;
  async detalleEvaluaciones() {
    this.cantidadRegistrosEvaluacions = 0;
    let codBalotario = this.filtro.idBalotario;
    let idCategoria = (typeof this.filtro.idCategoria === 'undefined') || (typeof this.filtro.idCategoria[0] === 'undefined') ? 0 : this.filtro.idCategoria[0];
    let idArea = (typeof this.filtro.idArea === 'undefined')|| (typeof this.filtro.idArea[0] === 'undefined') ? 0 : this.filtro.idArea[0];
    let idLocal = (typeof this.filtro.idLocal === 'undefined') || (typeof this.filtro.idLocal[0] === 'undefined') ? 0 : this.filtro.idLocal[0];

    this.tablaDetalleEvaluacionesResponse = [];

    await this.balotarioService.indicadorDetalle(codBalotario, idCategoria, idArea, idLocal)
      .then(
        (wsResponseProyecto: any) => {

          if (wsResponseProyecto.codResultado == 1) {
            this.tablaDetalleEvaluacionesResponse = (wsResponseProyecto.response != null) ? wsResponseProyecto.response : [];

            console.log(wsResponseProyecto)
            console.log(this.tablaDetalleEvaluacionesResponse)

            this.cargartablaDetalleEvaluaciones();

          }
          else {
            this.mensaje2 = MENSAJES.ERROR_NOFUNCION;
            this.cargartablaDetalleEvaluaciones();
            // this.openDialogMensaje(null,  wsResponseProyecto.msgResultado, true, false, wsResponseProyecto.codResultado);

          }

        },
        error => {

          this.mensaje2 = MENSAJES.ERROR_SERVICIO;
          /*   this.openDialogMensaje(this.mensaje, MENSAJES.PREFACTIBILIDAD.TITLE, true, false, null);
            console.error(error);
            this.disableBuscar = false; */
        }

      );

    console.log(this.tablaNotasResponse)
    this.cargartablaDetalleEvaluaciones();
  }

  public cargartablaDetalleEvaluaciones(): void {
    if (this.tablaDetalleEvaluacionesResponse != null && this.tablaDetalleEvaluacionesResponse.length > 0) {
      this.cantidadRegistrosEvaluacions = this.tablaDetalleEvaluacionesResponse.length;
      this.dataSourceDetalleEvaluaciones = new MatTableDataSource(this.tablaDetalleEvaluacionesResponse);
      this.dataSourceDetalleEvaluaciones.paginator = this.paginatorDetalleEvaluaciones;
      this.dataSourceDetalleEvaluaciones.sort = this.detallEvalSort;
      console.log("tabla -> " + this.dataSourceDetalleEvaluaciones);
    }
    else {
      debugger;
      this.cantidadRegistrosEvaluacions = 0;
      this.dataSourceDetalleEvaluaciones = new MatTableDataSource(this.tablaDetalleEvaluacionesResponse);
      this.dataSourceDetalleEvaluaciones.paginator = this.paginatorDetalleEvaluaciones;
      this.dataSourceDetalleEvaluaciones.sort = this.detallEvalSort;
    }
  }

  exportNotasXLXS(): void {
    this.excelService.exportAsExcelFile(this.dataSourceNota.filteredData, 'notas');
  }

  exportarDetalleEvaluaciones(): void {
    this.excelService.exportAsExcelFile(this.dataSourceDetalleEvaluaciones.filteredData, 'evaluaciones');
  }

  applyFilterTabCargo(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSourceCargoTabla.filter = filterValue;
  }

  applyFilterTabCategoria(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSourceCategoriaTabla.filter = filterValue;
  }

  applyFilterTabArea(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSourceAreaTabla.filter = filterValue;
  }

  applyFilterTabLocal(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSourceLocalTabla.filter = filterValue;
  }

  applyFilterTabNotas(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSourceNota.filter = filterValue;
  }
  applyFilterTabDetalleEvaluaciones(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSourceDetalleEvaluaciones.filter = filterValue;
  }



}



export interface ReporteCargo {
  cargo: string;
  cantUsuario: number;
  avance: number;
}

export interface CargoDetalle {
  codigo: number;
  descripcion: string;
  totalBalotario: number;
  totalRealizada: number;
  valorInicialGrilla: number;
  totalVerdeGrilla: number;
  totalRojoGrilla: number;
  barra_rojoGrilla: number;
  barra_verdeGrilla: number;
  barraCaso1: boolean;
  barraCaso2: boolean;
  barraCaso3: boolean;
}

export interface CategoriaDetalle {
  codigo: number;
  descripcion: string;
  totalBalotario: number;
  totalRealizada: number;
  valorInicialGrilla: number;
  totalVerdeGrilla: number;
  totalRojoGrilla: number;
  barra_rojoGrilla: number;
  barra_verdeGrilla: number;
  barraCaso1: boolean;
  barraCaso2: boolean;
  barraCaso3: boolean;
}

export interface AreaDetalle {
  codigo: number;
  descripcion: string;
  totalBalotario: number;
  totalRealizada: number;
  valorInicialGrilla: number;
  totalVerdeGrilla: number;
  totalRojoGrilla: number;
  barra_rojoGrilla: number;
  barra_verdeGrilla: number;
  barraCaso1: boolean;
  barraCaso2: boolean;
  barraCaso3: boolean;
}

export interface LocalDetalle {
  codigo: number;
  descripcion: string;
  totalBalotario: number;
  totalRealizada: number;
  valorInicialGrilla: number;
  totalVerdeGrilla: number;
  totalRojoGrilla: number;
  barra_rojoGrilla: number;
  barra_verdeGrilla: number;
  barraCaso1: boolean;
  barraCaso2: boolean;
  barraCaso3: boolean;
}

const ELEMENT_DATA: ReporteCargo[] = [
  { cargo: 'OPERADOR MULTIFUNCIONAL', cantUsuario: 9, avance: 50 },
  { cargo: 'PERECIBLES MULTIFUNCIONAL', cantUsuario: 1, avance: 20 },
  { cargo: 'ENCARGADO DE PERECIBLES', cantUsuario: 4, avance: 80 },
  { cargo: 'JEFE DEL CENTRO PRODUCTIVO', cantUsuario: 5, avance: 60 },

];
