import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BalotarioService } from '../../../service/balotario.service';

declare const startCountDown: any;

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  @Output() muestraInicio = new EventEmitter();

  habilitarbtnContinuar:boolean;

  textoDescripcion: string;
  textoMensaje: string;
  codBalotario: number;

  constructor(private balotarioService: BalotarioService) { }

  ngOnInit() {
   
    this.habilitarbtnContinuar = false;
    this.validarBalotario();
  }

  validarBalotario(){
    this.balotarioService.validarBalotarioUsuario().subscribe(
      (wsResponse: any) => {
        debugger;
        console.log(wsResponse);
        if (wsResponse.codResultado == 1 || wsResponse.codResultado == 2) {
          this.textoDescripcion = wsResponse.response.nomDescripcion;
          this.textoMensaje = wsResponse.response.nomMensaje;
          this.codBalotario = wsResponse.response.codBalotario;
          localStorage.setItem('cod_bal', this.codBalotario.toString());
          this.habilitarbtnContinuar = false;
        } else { 
          if(wsResponse.response!=null){
            this.textoDescripcion = wsResponse.response.nomDescripcion;
            this.textoMensaje = wsResponse.msgResultado;
            this.habilitarbtnContinuar = true;
          }else{
            this.textoMensaje = wsResponse.msgResultado;
            this.habilitarbtnContinuar = true;
          }
       
        }
      },
      error => {
        console.error(error);
      }
    );
  }

  iniciar() {
    console.log("INICIO - COMPONENT iniciar ");
    //startCountDown();
    localStorage.setItem('init', (true).toString());
    this.muestraInicio.emit(true);
  }

}
