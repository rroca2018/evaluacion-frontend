import { Component, OnInit, ElementRef, ViewChild, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { NgxSpinnerService } from 'ngx-spinner';
import { InfoMessageComponent } from '@shared/components/info-message/info-message.component';
import { MENSAJES } from 'app/common';
import { PersonaRequest } from '../../../../dto/request/PersonaRequest';
import { ItemService } from '../../../../service/item.service';
import { PersonaService } from '../../../../service/persona.service';
import * as _moment from 'moment';
import { filter } from 'rxjs/operators';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { WsResponse } from '../../../../dto/WsResponse';
import { VerificarRequest } from '../../../../dto/request/verificarRequest';


@Component({
  selector: 'app-registrar-actualizar',
  templateUrl: './registrar-actualizar.component.html',
  styleUrls: ['./registrar-actualizar.component.scss']
})
export class RegistrarActualizarComponent implements OnInit {
  // ************* tabs
  laboral: boolean;
  selectedIndexTab: string;
  // ************* data
  personaForm: FormGroup;

  listTipoDocumento: any[];
  listCategoria: any[];
  listCargo: any[];
  listRol: any[];
  listArea: any[];
  listLocal: any[];
  listDepartamento: any[];
  listProvincia: any[];
  listDistrito: any[];
  listEstado: any[];

  activo: boolean;

  @ViewChild('myInput')
  myInputVariable: ElementRef;

  selectedFoto: boolean;
  fileUploadFoto: File;
  archivoFoto: string;
  fileUploadResFoto = { status: '', message: '', messageAux: '' };

  selectedContrato: boolean;
  fileUploadContrato: File;
  archivoContrato: string;
  fileUploadResContrato = { status: '', message: '', messageAux: '' };

  mensaje: any;
  persona: PersonaRequest = new PersonaRequest();

  enabledProvincia: boolean = true;
  enabledDistrito: boolean = true;
  existencia: boolean;

  dialogRefMessage: MatDialogRef<any>;

  textoButton: string;
  tituloModalUsuario: string;

  estado_modificar: boolean;

  constructor(
    private spinner: NgxSpinnerService,
    public dialogRef: MatDialogRef<RegistrarActualizarComponent>,
    private formBuilder: FormBuilder,
    private itemService: ItemService,
    private personaService: PersonaService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA)
    public datos: Datapersona
  ) {
   
  }

  ngOnInit() {
    debugger;
    this.cargarCombos();

    if (this.datos.dataPersona) {
      this.tituloModalUsuario = "Modificar Persona";
      this.textoButton = "Actualizar";
      this.persona = this.datos.dataPersona.response;
      debugger;
      console.log(this.persona)
      this.activo = true;
      this.buscarArea(this.persona.idCategoria);
      this.buscarlocal(this.persona.idCategoria, this.persona.idArea)
      this.estado_modificar = true;
      /*this.personaForm.get('fechaNacimiento').setValue(this.retornaFecha(this.persona.fechaNacimiento));
      this.personaForm.get('fechaInicioContrato').setValue(this.retornaFecha(this.persona.fechaInicioContrato));
      this.personaForm.get('fechaTerminoContrato').setValue(this.retornaFecha(this.persona.fechaTerminoContrato));*/
    } else {
      this.activo = false;
      this.tituloModalUsuario = "Nueva Persona";
      this.textoButton = "Grabar";
      this.persona = new PersonaRequest();
      this.persona.sueldoBasico = 0;
      //this.persona.fecNacimiento = "05";
      this.estado_modificar = false;
      this.persona.idRol = 2;

    //this.llenarDataFormularioPrueba();
    }

    this.formulario();
  }

  llenarDataFormularioPrueba(){
    
    this.persona.usuario = "xxx";
    this.persona.clave =  "xxxxxxxxxxxx";
    this.persona.codTrabajador = "xxx";
    this.persona.primerNombre = "xxx";
    this.persona.segundoNombre = "xxx";
    this.persona.apePaterno = "xxx";
    this.persona.apeMaterno = "xxx";
    this.persona.correoElectronico = "xxx@gmail.com";
    this.persona.sueldoBasico = 0;
    this.persona.idTipoDocumento = 1;
    this.persona.numDocumento = "123456789";
    this.persona.idCargo = 1;
    this.persona.idCategoria = 1;
    this.persona.idArea = 1;
    this.persona.idLocal = 1;
    this.persona.idRol = 1;
  }

  retornaFecha(fecha: string) {
    debugger;
    return new Date(`${fecha}T23:00`);
  }


  //  ************* DATA
  formulario() {
    this.personaForm = this.formBuilder.group({
      codigoFrm: [''],
      usuarioFrm: [{value: '', disabled: this.activo}],
      claveFrm: [''],
      codigoTrabajadorFrm: [''],
      primerNombreFrm: [''],
      segundoNombreFrm: [''],
      apellidoPaternoFrm: [''],
      apellidoMaternoFrm: [''],
      correoFrm: [''],
      sexoFrm: [''],
      fechaNacimientoFrm: [''],
      fechaIngresoFrm: [''],
      sueldoFrm: [''],
      tipoDocFrm: [''],
      numeroDocFrm: [''],
      cargoFrm: [''],
      categoriaFrm: [''],
      areaFrm: [''],
      localFrm: [''],
      rolFrm: [''],
      estadoFrm: ['']
    });


  }

  cargarCombos() {
    this.itemService.listadoTipoDocumento().subscribe(data => {
      this.listTipoDocumento = data.response
    });
    this.itemService.listadoCategoria().subscribe(data => {
      this.listCategoria = data.response
    });
    this.itemService.listadoCargo().subscribe(data => {
      this.listCargo = data.response
    });
    this.itemService.listadoRol().subscribe(data => {
      this.listRol = data.response
    });
    this.itemService.listadoEstado().subscribe(data => {
      this.listEstado = data.response
    });

    

  }

  buscarArea(idCategoria: number) {
    debugger;
    this.listLocal = [];
    this.itemService.listadoArea(idCategoria).subscribe(data => {
      this.listArea = data.response
      if (data.response.length > 0) {
        this.listArea = data.response
      } else {
        this.listArea = [];
        if(typeof this.persona.idArea !== 'undefined'){
          this.persona.idArea = 0;
        }
      }
    });
  }

  buscarlocal(idCategoria: number, idArea: number) {
    debugger;
    this.itemService.listadoLocal(idCategoria, idArea).subscribe(data => {
      if (data.response.length > 0) {
        this.listLocal = data.response
      } else {
        this.listLocal = [];
        if(typeof this.persona.idLocal !== 'undefined'){
          this.persona.idLocal = 0;
        }
      }
    });
  }



  seleccionaFoto(event) {
    this.selectedFoto = true;
    this.fileUploadFoto = event.target.files[0];
    this.archivoFoto = event.target.files[0].name;
  }

  seleccionaContrato(event) {
    this.selectedContrato = true;
    this.fileUploadContrato = event.target.files[0];
    this.archivoContrato = event.target.files[0].name;
  }




  validarFormulario(): boolean {
    debugger;
    let valido: boolean = true;
    if (typeof this.persona.usuario === 'undefined' || this.persona.usuario == "") {
      valido = false;
    } else if (typeof this.persona.clave === 'undefined' || this.persona.clave == "") {
      valido = false;
    } else if (typeof this.persona.codTrabajador === 'undefined' || this.persona.codTrabajador == "") {
      valido = false;
    } else if (typeof this.persona.primerNombre === 'undefined' || this.persona.primerNombre == "") {
      valido = false;
    } else if (typeof this.persona.apePaterno === 'undefined' || this.persona.apePaterno == "") {
      valido = false;
    } else if (typeof this.persona.apeMaterno === 'undefined' || this.persona.apeMaterno == "") {
      valido = false;
    } else if (typeof this.persona.correoElectronico === 'undefined' || this.persona.correoElectronico == "") {
      valido = false;
    } else if (typeof this.persona.sexo === 'undefined' || this.persona.sexo == "") {
      valido = false;
    } else if (typeof this.persona.fecNacimiento === 'undefined' || this.persona.fecNacimiento == "") {
      valido = false;
    } else if (typeof this.persona.fecIngreso === 'undefined' || this.persona.fecIngreso == "") {
      valido = false;
    } else if (typeof this.persona.idTipoDocumento === 'undefined' || this.persona.idTipoDocumento == 0) {
      valido = false;
    } else if (typeof this.persona.numDocumento === 'undefined') {
      valido = false;
    } else if (typeof this.persona.idCargo === 'undefined' || this.persona.idCargo == 0) {
      valido = false;
    } else if (typeof this.persona.idCategoria === 'undefined' || this.persona.idCategoria == 0) {
      valido = false;
    } else if (typeof this.persona.idArea === 'undefined' || this.persona.idArea == 0) {
      valido = false;
    } else if (typeof this.persona.idLocal === 'undefined' || this.persona.idLocal == 0) {
      valido = false;
    } else if (typeof this.persona.idRol === 'undefined' || this.persona.idRol == 0) {
      valido = false;
    }
    return valido;
  }

  validarClave() : boolean{
    let valido: boolean = true;
    let primerBlanco = /^ /;
    let ultimoBlanco = / $/;
    let variosBlancos = /[ ]+/g;
    let texto;
    let textoFinal;
    texto = this.persona.clave.replace (variosBlancos," ");
    texto = this.persona.clave.replace (primerBlanco,"");
    texto = this.persona.clave.replace (ultimoBlanco,"");
    //textoFinal = texto.split (texto, " ");
    if (texto.length < 8) {
      valido = false;
    }
    return valido;
  }

  validarCorreo(): boolean {
    debugger;
    let valido: boolean;
    let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    if (emailRegex.test(this.persona.correoElectronico)) {
      valido = true;
    } else {
      valido = false;
    }

    return valido;
  }

  validarFechaNacimiento = () => {
    
    let valFecNacMay: boolean = true;
    let valFecNacMin: boolean = true;
    let valFecIngreso: boolean = true;
    let fecNacimiento = new Date(this.persona.fecNacimiento).getTime();
    let fecIngreso = new Date(this.persona.fecIngreso).getTime();
    let fecMinima = new Date("01/01/1920").getTime();
    let fechaActual = new Date();
    if(fecNacimiento >= fecIngreso){
      valFecNacMay = false;
    }else if(fecNacimiento <= fecMinima){
      valFecNacMin = false;
    }else if(fecIngreso > fechaActual.setDate(fechaActual.getDate() + 365)){
      valFecIngreso = false;
    }

    return { valFecNacMay, valFecNacMin, valFecIngreso };
  }

  validarSueldoMinimo(): boolean {
    let valido: boolean = true;

    if(this.persona.sueldoBasico < 0){
      valido = false;
    }

    return valido;
  }




  rpta: any[] = [];
  verificar: VerificarRequest;
  async guardarPersona() {
   
    const { valFecNacMay, valFecNacMin, valFecIngreso } = this.validarFechaNacimiento();
    
    if (this.validarFormulario() == false) {
      this.openDialogMensaje(MENSAJES.PERSONA.CAMPOS_OBLIGATORIOS, true, false, null);
    } else if(this.validarClave() == false){
      this.openDialogMensaje(MENSAJES.PERSONA.CLAVE_MINIMO_OCHO_CARACTERES, true, false, null);
    } else if(this.validarCorreo() == false){
      this.openDialogMensaje(MENSAJES.PERSONA.CORREO_FORMATO_INCORRECTO, true, false, null);
    }else if( valFecNacMay == false){
      this.openDialogMensaje(MENSAJES.PERSONA.FECHA_NACIMIENTO_MAYOR_IGUAL_FECHA_INGRESO, true, false, null);
    }else if( valFecNacMin == false){
      this.openDialogMensaje(MENSAJES.PERSONA.FECHA_NACIMIENTO_MENOR_FECHA_MINIMA, true, false, null);
    }else if( valFecIngreso == false){
      this.openDialogMensaje(MENSAJES.PERSONA.FECHA_INGRESO_MAYOR_FECHA_ACTUAL, true, false, null);
    }else if(this.validarSueldoMinimo() == false){
      this.openDialogMensaje(MENSAJES.PERSONA.SUELDO_BASICO_CERO, true, false, null);
    }
    
    
    else {

    this.verificar = new VerificarRequest();
    
    let validoUsuario : boolean = true;
    let validoCodTrabajador: boolean = true;
    let validoNumDocumento: boolean = true;


    if(this.persona.flagEditar !=1){
      await this.personaService.verificarUsuario(this.persona.usuario).then(res =>{
        if(res.codResultado == 1){
         this.openDialogMensaje(MENSAJES.PERSONA.USUARIO_EXISTE, true, false, null);
         validoUsuario = false;
        }
      })
      if(validoUsuario){
        this.verificar.codTrabajador = this.persona.codTrabajador;
        await this.personaService.verificarCodTrabajador(this.verificar).then(res =>{
          if(res.codResultado == 1){
           this.openDialogMensaje(MENSAJES.PERSONA.COD_TRABAJADOR_EXISTE, true, false, null);
           validoCodTrabajador = false;
          }
        })
       }
       if(validoUsuario && validoCodTrabajador){
        this.verificar.numDocumento = this.persona.numDocumento;
        await this.personaService.verificarNumDocumento(this.verificar).then(res =>{
          if(res.codResultado == 1){
           this.openDialogMensaje(MENSAJES.PERSONA.NUM_DOCUMENTO_EXISTE, true, false, null);
           validoNumDocumento = false;
          }
        })
       }

       if(validoUsuario && validoCodTrabajador && validoNumDocumento){
       this.openDialogMensajeConfirm(MENSAJES.PERSONA.GUARDAR_PERSONA, true);

        this.dialogRefMessage.afterClosed()
          .pipe(filter(verdadero => !!verdadero))
          .subscribe(async() => {   
            this.persona.fecNacimiento = _moment(this.personaForm.get('fechaNacimientoFrm').value).format('YYYY/MM/DD');
            this.persona.fecIngreso = _moment(this.personaForm.get('fechaIngresoFrm').value).format('YYYY/MM/DD');

            await this.personaService.crearPersona(this.persona).then((wsResponse : any)=> {
              debugger;
              console.log(wsResponse);  
             },  error => {
                       
               this.mensaje = MENSAJES.ERROR_SERVICIO;
               console.error(this.mensaje); 
             }      
         ) ;
            
           
            this.dialogRef.close(true);
            this.snackBar.open("La persona ha sido registrado correctamente!");
          });

      
        }




    }else{
    
      let  verificarRequest = new VerificarRequest();
      verificarRequest.codUsuario = this.persona.codUsuario;
      verificarRequest.codTrabajador = this.persona.codTrabajador;

        await this.personaService.verificarCodTrabajador(verificarRequest).then(res =>{
          if(res.codResultado == 1){
           this.openDialogMensaje(MENSAJES.PERSONA.COD_TRABAJADOR_EXISTE, true, false, null);
           validoCodTrabajador = false;
          }
        })
       
       if(validoCodTrabajador){
        verificarRequest.codUsuario = this.persona.codUsuario;
        verificarRequest.numDocumento = this.persona.numDocumento;
        await this.personaService.verificarNumDocumento(verificarRequest).then(res =>{
          if(res.codResultado == 1){
           this.openDialogMensaje(MENSAJES.PERSONA.NUM_DOCUMENTO_EXISTE, true, false, null);
           validoNumDocumento = false;
          }
        })
       }

       if(validoCodTrabajador && validoNumDocumento){
        this.openDialogMensajeConfirm(MENSAJES.PERSONA.MODIFICAR_PERSONA, true);
        debugger;
        this.dialogRefMessage.afterClosed()
          .pipe(filter(verdadero => !!verdadero))
          .subscribe(async() => {   
            debugger;
            this.persona.fecNacimiento = _moment(this.personaForm.get('fechaNacimientoFrm').value).format('YYYY/MM/DD');
            this.persona.fecIngreso = _moment(this.personaForm.get('fechaIngresoFrm').value).format('YYYY/MM/DD');
    
            await this.personaService.actualizarPersona(this.persona).then((wsResponse : any)=> {
              debugger;
              console.log(wsResponse);  
             },  error => {
                       
               this.mensaje = MENSAJES.ERROR_SERVICIO;
               console.error(this.mensaje);  
             }      
         ) ;

          
            this.dialogRef.close(true);
            this.snackBar.open("La persona ha sido actualizada correctamente!");
          });
         }

    }
    

  }

  }


  cerrarFormulario() {
    if(this.persona.flagEditar !=1){
    this.openDialogMensajeConfirm(MENSAJES.PERSONA.BOTON_SALIR_GUARDAR, true);

    this.dialogRefMessage.afterClosed()
      .pipe(filter(verdadero => !!verdadero))
      .subscribe(() => {
        this.dialogRef.close(true);

      });
    }else{
      this.openDialogMensajeConfirm(MENSAJES.PERSONA.BOTON_SALIR_MODIFICAR, true);

    this.dialogRefMessage.afterClosed()
      .pipe(filter(verdadero => !!verdadero))
      .subscribe(() => {
        this.dialogRef.close(true);

      });
    }
  }


  public openDialogMensajeConfirm(message: string, confirmacion: boolean): void {

    this.dialogRefMessage = this.dialog.open(InfoMessageComponent, {
      //width: '400px',
      disableClose: true,
      data: { message: message, confirmacion: confirmacion }
    });
  }



  public openDialogMensaje(message: string, alerta: boolean, confirmacion: boolean, valor: any
  ): void {
    const dialogRef = this.dialog.open(InfoMessageComponent, {
      disableClose: true,
      data: {
        message: message,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe((ok: number) => {
      if (ok == 0) {

      }
    });
  }

  async  xxx(){
      this.dialogRefMessage.afterClosed()
    .pipe(filter(verdadero => !!verdadero))
    .toPromise().then(async () => {   
      this.persona.fecNacimiento = _moment(this.personaForm.get('fechaNacimientoFrm').value).format('YYYY/MM/DD');
      this.persona.fecIngreso = _moment(this.personaForm.get('fechaIngresoFrm').value).format('YYYY/MM/DD');

      await this.personaService.crearPersona(this.persona).then((wsResponse : any)=> {
        console.log(wsResponse);  
       },  error => {
                 
         this.mensaje = MENSAJES.ERROR_SERVICIO;
    
       }
       
   ) ;
      
            
          
     
      this.dialogRef.close(true);
      this.snackBar.open("La persona ha sido registrado correctamente!");
    });


  }

}

interface Datapersona {
  dataPersona?: any
}

