import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { BalotarioService } from '../../service/balotario.service';
import { Chart } from 'chart.js';
import { WsResponse } from '../../dto/WsResponse';

@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.component.html',
  styleUrls: ['./resultado.component.scss']
})
export class ResultadoComponent implements OnInit {

  filtrosForm: FormGroup;
  listComboDominio: any[];

  // CHART
  Estado = [];
  Porcentaje = [];
  chart = [];

  constructor(
    private balotarioService: BalotarioService
  ) { }

  ngOnInit() {
    this.obtenerChart();
    this.crearFiltrosForm();
    this.cargarCombos();
  }

  crearFiltrosForm() {
    this.filtrosForm = new FormGroup({
      comboDominioFrmCtrl: new FormControl(null),
    });
  }

  get comboDominioFrmCtrl() { return this.filtrosForm.get('comboDominioFrmCtrl'); }

  cargarCombos() {
    this.balotarioService.obtenerComboDominio().subscribe(data => {
      this.listComboDominio = data.response
    });
  }

  obtenerChart() {
    this.balotarioService.listado().subscribe((WsResponse: WsResponse) => {
      WsResponse.response.forEach(x => {
        this.Estado.push(x.estado);
        this.Porcentaje.push(x.porcentaje);
      });



      this.chart = new Chart('canvas', {
        type: 'doughnut',
        data: {
          labels: this.Estado,
          datasets: [
            {
              data: this.Porcentaje,
              backgroundColor: [
                "#3cb371",
                "#D7DF01",
                "#DF0101"
              ],
              fill: true
            }
          ]
        },
        options: {
          title: {
            display: true,
            position: "bottom",
            text: 'Nivel Obtenido'
          },
          legend: {
            display: false,
          },
          tooltips: {
            enabled: false
          },
          cutoutPercentage: 80,
        },
      
      });
    });
  }

}
