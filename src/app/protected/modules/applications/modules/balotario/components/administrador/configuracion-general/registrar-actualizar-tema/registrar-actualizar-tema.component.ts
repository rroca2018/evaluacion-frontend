import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { InfoMessageComponent } from '@shared/components/info-message/info-message.component';
import { MENSAJES } from 'app/common';
import { filter } from 'rxjs/operators';
import { Tema } from '../../../../dto/request/Tema';
import { ConfiguracionService } from '../../../../service/configuracion.service';

@Component({
  selector: 'app-registrar-actualizar-tema',
  templateUrl: './registrar-actualizar-tema.component.html',
  styleUrls: ['./registrar-actualizar-tema.component.scss']
})
export class RegistrarActualizarTemaComponent implements OnInit {
  temaForm: FormGroup;
  textoButton: String;
  tituloModalCargo: String;
  tema: Tema = new Tema();
  dialogRefMessage: MatDialogRef<any>;
  mensaje: any;
  mostrarEstado: boolean = false;


  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<RegistrarActualizarTemaComponent>,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private configuracionService: ConfiguracionService,
    @Inject(MAT_DIALOG_DATA)
    public datos: DataTema,
  ) { }

  ngOnInit() {
    debugger;
    console.log(this.datos.dataTema)
    if (this.datos.dataTema) {
    this.tituloModalCargo = "Modificar tema";
    this.textoButton = "Actualizar";
    this.tema = this.datos.dataTema.response;
    this.mostrarEstado = true;  
    }
    else{
      this.tituloModalCargo = "Agregar tema";
      this.textoButton = "Grabar";
    }
  
    this.formulario();
  }


  formulario() {
    this.temaForm = this.formBuilder.group({
      descripcionTemaFrm: [''],  
      estadoFrm: [''],  
    });
  }
  
  grabarTema(){
    if (this.validarFormulario() == false) {
      this.openDialogMensaje(MENSAJES.TEMA.CAMPOS_OBLIGATORIOS, true, false, null);
    }
    else if (this.tema.flagEditar != 1) {
      this.openDialogMensajeConfirm(MENSAJES.TEMA.GUARDAR_TEMA, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(async() => {   
       debugger;
          await this.configuracionService.grabarTema(this.tema).then((wsResponse : any)=> {
            debugger;
            console.log(wsResponse);  
           },  error => {
                     
             this.mensaje = MENSAJES.ERROR_SERVICIO;
             console.error(this.mensaje); 
           }      
       ) ;
   
          this.dialogRef.close(true);
          this.snackBar.open("El tema ha sido registrado correctamente!");
        });
    } 
    else {
            debugger;       
      this.openDialogMensajeConfirm(MENSAJES.TEMA.MODIFICAR_TEMA, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(async() => {   
       debugger;
          await this.configuracionService.grabarTema(this.tema).then((wsResponse : any)=> {
            debugger;
            console.log(wsResponse);  
           },  error => {
                     
             this.mensaje = MENSAJES.ERROR_SERVICIO;
             console.error(this.mensaje); 
           }      
       ) ;
   
          this.dialogRef.close(true);
          this.snackBar.open("El tema ha sido registrado correctamente!");
        });
    }    
 

  }

  validarFormulario(): boolean {
    debugger;
    let valido: boolean = true;
    if (typeof this.tema.descripcionTema === 'undefined' || this.tema.descripcionTema  == "") {
      valido = false;
    }
    return valido;
  }
  public openDialogMensajeConfirm(message: string, confirmacion: boolean): void {

    this.dialogRefMessage = this.dialog.open(InfoMessageComponent, {
      //width: '400px',
      disableClose: true,
      data: { message: message, confirmacion: confirmacion }
    });
  }



  public openDialogMensaje(message: string, alerta: boolean, confirmacion: boolean, valor: any
  ): void {
    const dialogRef = this.dialog.open(InfoMessageComponent, {
      disableClose: true,
      data: {
        message: message,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe((ok: number) => {
      if (ok == 0) {

      }
    });
  }

  cerrarFormulario() {
    if (this.tema.flagEditar != 1) {
      this.openDialogMensajeConfirm(MENSAJES.TEMA.BOTON_SALIR_GUARDAR, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(() => {
          this.dialogRef.close(true);

        });
    } else {
      this.openDialogMensajeConfirm(MENSAJES.TEMA.BOTON_SALIR_MODIFICAR, true);

      this.dialogRefMessage.afterClosed()
        .pipe(filter(verdadero => !!verdadero))
        .subscribe(() => {
          this.dialogRef.close(true);

        });
    }
  }

}

interface DataTema {
  dataTema?: any
}
