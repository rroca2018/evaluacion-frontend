import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BalotarioRoutingModule } from './balotario-routing.module';
import { SharedModule } from '@shared/shared.module';
import { AuthModule } from '@shared/auth/auth.module';
import { LoadingModule } from '@shared/loading/loading.module';
import { CountdownModule } from 'ngx-countdown';
import { ReporteComponent } from './components/reporte/reporte.component';
import { MatTabsModule } from '@angular/material';
import { AreaComponent } from './components/reporte/area/area.component';
import { EvaluadorComponent } from './components/reporte/evaluador/evaluador.component';
import { ColaboradorComponent } from './components/reporte/colaborador/colaborador.component';
import { AutoevaluacionComponent } from './components/reporte/autoevaluacion/autoevaluacion.component';
import { BalotarioService } from './service/balotario.service';
import { ResultadoComponent } from './components/resultado/resultado.component';
import { RegistrarActualizarComponent } from './components/administrador/usuario/registrar-actualizar/registrar-actualizar.component';
import { UsuarioComponent } from './components/administrador/usuario/usuario.component';
import { ItemService } from './service/item.service';
import { PersonaService } from './service/persona.service';
import { PlantillaExcelComponent } from './components/administrador/usuario/plantilla-excel/plantilla-excel.component';
import { CargaMasivaComponent } from './components/administrador/usuario/plantilla-excel/carga-masiva/carga-masiva.component';
import { ExcelService } from './service/excel.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ConfiguracionBalotariosComponent } from './components/administrador/configuracion-balotarios/configuracion-balotarios.component';
import { VerPreguntasComponent } from './components/administrador/configuracion-balotarios/ver-preguntas/ver-preguntas.component';
import { RegistrarActualizarBalotariosComponent } from './components/administrador/configuracion-balotarios/registrar-actualizar-balotarios/registrar-actualizar-balotarios.component';
import { VisualizacionResultadosComponent } from './components/administrador/visualizacion-resultados/visualizacion-resultados.component';
import { FiltrarResultadosComponent } from './components/administrador/visualizacion-resultados/filtrar-resultados/filtrar-resultados.component';

import {
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatTableModule
} from '@angular/material'
import { ExporterService } from './service/exporter.service';
import { PreguntasRespuestasComponent } from './components/administrador/preguntas-respuestas/preguntas-respuestas.component';
import { PreguntasRespuestasService } from './service/preguntasRespuestas.service';
import { RegistrarActualizarPreguntasComponent } from './components/administrador/preguntas-respuestas/registrar-actualizar-preguntas/registrar-actualizar-preguntas.component';
import { PlantillaExcelPreguntasComponent } from './components/administrador/preguntas-respuestas/plantilla-excel-preguntas/plantilla-excel-preguntas.component';
import { CargaMasivaPreguntasComponent } from './components/administrador/preguntas-respuestas/plantilla-excel-preguntas/carga-masiva-preguntas/carga-masiva-preguntas.component';
import { ConfiguracionGeneralComponent } from './components/administrador/configuracion-general/configuracion-general.component';
import { ConfiguracionService } from './service/configuracion.service';
import { RegistrarActualizarCategoriaComponent } from './components/administrador/configuracion-general/registrar-actualizar-categoria/registrar-actualizar-categoria.component';
import { RegistrarActualizarAreaComponent } from './components/administrador/configuracion-general/registrar-actualizar-area/registrar-actualizar-area.component';
import { RegistrarActualizarLocalComponent } from './components/administrador/configuracion-general/registrar-actualizar-local/registrar-actualizar-local.component';
import { RegistrarActualizarCargoComponent } from './components/administrador/configuracion-general/registrar-actualizar-cargo/registrar-actualizar-cargo.component';
import { RegistrarActualizarTemaComponent } from './components/administrador/configuracion-general/registrar-actualizar-tema/registrar-actualizar-tema.component';

@NgModule({
  declarations: [
    ReporteComponent,
    AreaComponent,
    EvaluadorComponent,
    ColaboradorComponent,
    AutoevaluacionComponent,
    ResultadoComponent,
    UsuarioComponent,
    RegistrarActualizarComponent,
    PlantillaExcelComponent,
    CargaMasivaComponent,
    ConfiguracionBalotariosComponent,
    VerPreguntasComponent,
    RegistrarActualizarBalotariosComponent,
    VisualizacionResultadosComponent,
    FiltrarResultadosComponent,
    PreguntasRespuestasComponent,
    RegistrarActualizarPreguntasComponent,
    PlantillaExcelPreguntasComponent,
    CargaMasivaPreguntasComponent,
    ConfiguracionGeneralComponent,
    RegistrarActualizarCategoriaComponent,
    RegistrarActualizarAreaComponent,
    RegistrarActualizarLocalComponent,
    RegistrarActualizarCargoComponent,
    RegistrarActualizarTemaComponent
  ],
  entryComponents: [
    RegistrarActualizarComponent,
    PlantillaExcelComponent,
    CargaMasivaComponent,
    ConfiguracionBalotariosComponent,
    VerPreguntasComponent,
    RegistrarActualizarBalotariosComponent,
    VisualizacionResultadosComponent,
    FiltrarResultadosComponent,
    RegistrarActualizarPreguntasComponent,
    PlantillaExcelPreguntasComponent,
    CargaMasivaPreguntasComponent,
    RegistrarActualizarCategoriaComponent,
    RegistrarActualizarAreaComponent,
    RegistrarActualizarLocalComponent,
    RegistrarActualizarCargoComponent,
    RegistrarActualizarTemaComponent
  ],
  providers: [
    BalotarioService,
    ItemService,
    PersonaService,
    ExcelService,
    ExporterService,
    PreguntasRespuestasService,
    ConfiguracionService
  ],
  imports: [
    SharedModule,
    CommonModule,
    BalotarioRoutingModule,
    CountdownModule,
    AuthModule.forRoot(),
    LoadingModule.forRoot(),
    MatTabsModule,
    NgxSpinnerModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatTableModule
  ]
})
export class BalotarioModule { }
